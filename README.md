
# MDS Motion_Streaming

MDS B3 - Motion Streaming API calls

## Run the Back-End

```bash
    $ cd ./site/back/src
    $ node index.js
```

## Run the Front-End
```bash
    $ cd ./site/front
    $ yarn start
```

<!--- If we have only one group/collection, then no need for the "ungrouped" heading -->

## Endpoints

- [MDS Motion_Streaming](#mds-motion_streaming)
  - [Run the Back-End](#run-the-back-end)
  - [Run the Front-End](#run-the-front-end)
  - [Endpoints](#endpoints)
  - [Actor](#actor)
    - [1. localhost:8080/api/actor/list](#1-localhost8080apiactorlist)
    - [2. localhost:8080/api/actor/show](#2-localhost8080apiactorshow)
    - [3. localhost:8080/api/actor/add](#3-localhost8080apiactoradd)
    - [4. localhost:8080/api/actor/update](#4-localhost8080apiactorupdate)
    - [5. localhost:8080/api/actor/delete](#5-localhost8080apiactordelete)
  - [Classification](#classification)
    - [1. localhost:8080/api/classification/list](#1-localhost8080apiclassificationlist)
    - [2. localhost:8080/api/classification/show](#2-localhost8080apiclassificationshow)
    - [3. localhost:8080/api/classification/add](#3-localhost8080apiclassificationadd)
    - [4. localhost:8080/api/classification/update](#4-localhost8080apiclassificationupdate)
    - [5. localhost:8080/api/classification/delete](#5-localhost8080apiclassificationdelete)
  - [Category](#category)
    - [1. localhost:8080/api/category/list](#1-localhost8080apicategorylist)
    - [2. localhost:8080/api/category/show](#2-localhost8080apicategoryshow)
    - [3. localhost:8080/api/category/add](#3-localhost8080apicategoryadd)
    - [4. localhost:8080/api/category/update](#4-localhost8080apicategoryupdate)
    - [5. localhost:8080/api/category/delete](#5-localhost8080apicategorydelete)
  - [Marketting](#marketting)
    - [1. localhost:8080/api/marketting/list](#1-localhost8080apimarkettinglist)
    - [2. localhost:8080/api/marketting/show](#2-localhost8080apimarkettingshow)
    - [3. localhost:8080/api/marketting/add](#3-localhost8080apimarkettingadd)
    - [4. localhost:8080/api/marketting/list](#4-localhost8080apimarkettinglist)
    - [5. localhost:8080/api/marketting/delete](#5-localhost8080apimarkettingdelete)
  - [Member](#member)
    - [1. localhost:8080/api/member/list](#1-localhost8080apimemberlist)
    - [2. localhost:8080/api/member/show](#2-localhost8080apimembershow)
    - [3. localhost:8080/api/member/add](#3-localhost8080apimemberadd)
    - [4. localhost:8080/api/member/update](#4-localhost8080apimemberupdate)
    - [5. localhost:8080/api/member/delete](#5-localhost8080apimemberdelete)
  - [Movie](#movie)
    - [1. localhost:8080/api/movie/list](#1-localhost8080apimovielist)
    - [2. localhost:8080/api/movie/show](#2-localhost8080apimovieshow)
    - [3. localhost:8080/api/movie/views](#3-localhost8080apimovieviews)
    - [4. localhost:8080/api/movie/category](#4-localhost8080apimoviecategory)
    - [5. localhost:8080/api/movie/ranking](#5-localhost8080apimovieranking)
    - [6. localhost:8080/api/movie/search](#6-localhost8080apimoviesearch)
    - [7. localhost:8080/api/movie/add](#7-localhost8080apimovieadd)
    - [8. localhost:8080/api/movie/update](#8-localhost8080apimovieupdate)
    - [9. localhost:8080/api/movie/updateviews](#9-localhost8080apimovieupdateviews)
    - [10. localhost:8080/api/movie/updateranking](#10-localhost8080apimovieupdateranking)
    - [11. localhost:8080/api/movie/delete](#11-localhost8080apimoviedelete)
  - [Pricing](#pricing)
    - [1. localhost:8080/api/pricing/list](#1-localhost8080apipricinglist)
    - [2. localhost:8080/api/pricing/show](#2-localhost8080apipricingshow)
    - [3. localhost:8080/api/pricing/add](#3-localhost8080apipricingadd)
    - [4. localhost:8080/api/pricing/update](#4-localhost8080apipricingupdate)
    - [5. localhost:8080/api/pricing/delete](#5-localhost8080apipricingdelete)
  - [MovieActor](#movieactor)
    - [1. localhost:8080/api/movieactor/list](#1-localhost8080apimovieactorlist)
    - [2. localhost:8080/api/movieactor/showmovies](#2-localhost8080apimovieactorshowmovies)
    - [3. localhost:8080/api/movieactor/showactors](#3-localhost8080apimovieactorshowactors)
    - [4. localhost:8080/api/movieactor/add](#4-localhost8080apimovieactoradd)
    - [5. localhost:8080/api/movieactor/delete](#5-localhost8080apimovieactordelete)
  - [Favorite](#favorite)
    - [1. localhost:8080/api/favorite/list](#1-localhost8080apifavoritelist)
    - [2. localhost:8080/api/favorite/show](#2-localhost8080apifavoriteshow)
    - [3. localhost:8080/api/favorite/add](#3-localhost8080apifavoriteadd)
    - [4. localhost:8080/api/favorite/delete](#4-localhost8080apifavoritedelete)
  - [Statistics](#statistics)
    - [1. localhost:8080/api/statistics/showmostwatched](#1-localhost8080apistatisticsshowmostwatched)
    - [2. localhost:8080/api/statistics/showlatest](#2-localhost8080apistatisticsshowlatest)
    - [3. localhost:8080/api/statistics/showmarkettings](#3-localhost8080apistatisticsshowmarkettings)

--------



## Actor

Routes for actors



### 1. localhost:8080/api/actor/list


Get the list of actors


***Endpoint:***

```bash
Method: GET
Type: 
URL: localhost:8080/api/actor/list
```



### 2. localhost:8080/api/actor/show


Get an actor from ID


***Endpoint:***

```bash
Method: GET
Type: RAW
URL: localhost:8080/api/actor/show/51
```



### 3. localhost:8080/api/actor/add


Add an actor


***Endpoint:***

```bash
Method: POST
Type: RAW
URL: localhost:8080/api/actor/add
```



***Body:***

```js        
{
    "firstname": "Axel",
    "lastname": "Pion",
    "picture": ""
}
```



### 4. localhost:8080/api/actor/update


Update an actor


***Endpoint:***

```bash
Method: PATCH
Type: RAW
URL: localhost:8080/api/actor/update
```



***Body:***

```js        
{
    "id": 52,
    "firstname": "Rémy",
    "lastname": "Rubis",
    "picture": ""
}
```



### 5. localhost:8080/api/actor/delete


Delete an actor from ID


***Endpoint:***

```bash
Method: DELETE
Type: RAW
URL: localhost:8080/api/actor/delete/52
```



## Classification

Routes for classification



### 1. localhost:8080/api/classification/list


Get the list of classifications


***Endpoint:***

```bash
Method: GET
Type: 
URL: localhost:8080/api/classification/list
```



### 2. localhost:8080/api/classification/show


Get a classification from ID


***Endpoint:***

```bash
Method: GET
Type: 
URL: localhost:8080/api/classification/show/6
```



### 3. localhost:8080/api/classification/add


Post a classification


***Endpoint:***

```bash
Method: POST
Type: RAW
URL: localhost:8080/api/classification/add
```



***Body:***

```js        
{
    "title": "BDSM"
}
```



### 4. localhost:8080/api/classification/update


Update a classification


***Endpoint:***

```bash
Method: PATCH
Type: RAW
URL: localhost:8080/api/classification/update
```



***Body:***

```js        
{
    "id": 7,
    "title": "Granny"
}
```



### 5. localhost:8080/api/classification/delete


Delete classification from ID


***Endpoint:***

```bash
Method: DELETE
Type: RAW
URL: localhost:8080/api/classification/delete/7
```



## Category

Routes for category



### 1. localhost:8080/api/category/list


Get the list of categories


***Endpoint:***

```bash
Method: GET
Type: 
URL: localhost:8080/api/category/list
```



### 2. localhost:8080/api/category/show


Get a category from ID


***Endpoint:***

```bash
Method: GET
Type: 
URL: localhost:8080/api/category/show/8
```



### 3. localhost:8080/api/category/add


Add a category


***Endpoint:***

```bash
Method: POST
Type: RAW
URL: localhost:8080/api/marketting/add
```



***Body:***

```js        
{
    "title": "addCategory",
    "picture": ""
}
```



### 4. localhost:8080/api/category/update


Update a category


***Endpoint:***

```bash
Method: PATCH
Type: RAW
URL: localhost:8080/api/category/update
```



***Body:***

```js        
{
    "id": 9,
    "title": "Adulte",
    "picture": ""
}
```



### 5. localhost:8080/api/category/delete


Delete a category from ID


***Endpoint:***

```bash
Method: DELETE
Type: RAW
URL: localhost:8080/api/category/delete/9
```



## Marketting

Routes for marketting



### 1. localhost:8080/api/marketting/list


Get the list of markettings


***Endpoint:***

```bash
Method: GET
Type: RAW
URL: localhost:8080/api/marketting/list
```



### 2. localhost:8080/api/marketting/show


Get a marketting from ID


***Endpoint:***

```bash
Method: GET
Type: 
URL: localhost:8080/api/marketting/show/1
```



### 3. localhost:8080/api/marketting/add


Add a marketting


***Endpoint:***

```bash
Method: POST
Type: RAW
URL: localhost:8080/api/marketting/add
```



### 4. localhost:8080/api/marketting/list


Update a marketting


***Endpoint:***

```bash
Method: PATCH
Type: RAW
URL: localhost:8080/api/marketting/update
```



***Body:***

```js        
{
    "id": 11,
    "title": "MarkettingUpdate",
    "content": "Ceci est une update marketting",
    "link": "",
    "picture": "",
    "video": ""
}
```



### 5. localhost:8080/api/marketting/delete


Delete a marketting from ID


***Endpoint:***

```bash
Method: DELETE
Type: RAW
URL: localhost:8080/api/marketting/delete/11
```



## Member

Routes for member



### 1. localhost:8080/api/member/list


Get the list of members


***Endpoint:***

```bash
Method: GET
Type: RAW
URL: localhost:8080/api/member/list
```



### 2. localhost:8080/api/member/show


Get a member from ID


***Endpoint:***

```bash
Method: GET
Type: RAW
URL: localhost:8080/api/member/show/1
```



### 3. localhost:8080/api/member/add


Add a member


***Endpoint:***

```bash
Method: POST
Type: RAW
URL: localhost:8080/api/member/add
```



***Body:***

```js        
{
    "email": "pionaxel@gmail.com",
    "password": "",
    "firstname": "Axel",
    "lastname": "Pion",
    "address": "5 rue des adresses fictives",
    "zipcode": "95666",
    "city": "Ville fantôme",
    "picture": "",
    "pricing": 3
}
```



### 4. localhost:8080/api/member/update


Update a member


***Endpoint:***

```bash
Method: PATCH
Type: RAW
URL: localhost:8080/api/member/update
```



***Body:***

```js        
{
    "id": 1,
    "email": "pionaxel@gmail.com",
    "password": "ABC",
    "firstname": "Axel",
    "lastname": "Pion",
    "address": "5 rue des adresses fictives",
    "zipcode": "95666",
    "city": "Ville fantôme",
    "picture": "",
    "pricing": 3
}
```



### 5. localhost:8080/api/member/delete


Delete a member from ID


***Endpoint:***

```bash
Method: DELETE
Type: RAW
URL: localhost:8080/api/member/delete/1
```



## Movie

Routes for movie



### 1. localhost:8080/api/movie/list


Get the list of movies


***Endpoint:***

```bash
Method: GET
Type: RAW
URL: localhost:8080/api/movie/list
```



### 2. localhost:8080/api/movie/show


Get a movie from ID


***Endpoint:***

```bash
Method: GET
Type: RAW
URL: localhost:8080/api/movie/show/6
```



### 3. localhost:8080/api/movie/views


Get a movie's views from ID


***Endpoint:***

```bash
Method: GET
Type: RAW
URL: localhost:8080/api/movie/views/6
```



### 4. localhost:8080/api/movie/category


Get a list of movies from category ID


***Endpoint:***

```bash
Method: GET
Type: 
URL: localhost:8080/api/movie/category/1
```



### 5. localhost:8080/api/movie/ranking


Get a list of movie from ranking


***Endpoint:***

```bash
Method: GET
Type: RAW
URL: localhost:8080/api/movie/ranking/6
```



### 6. localhost:8080/api/movie/search


Search for a movie from title or category


***Endpoint:***

```bash
Method: GET
Type: 
URL: localhost:8080/api/movie/search/movie 6
```



### 7. localhost:8080/api/movie/add


Add a movie


***Endpoint:***

```bash
Method: POST
Type: RAW
URL: localhost:8080/api/movie/add
```



***Body:***

```js        
{
    "title": "MovieTest",
    "description": "MovieTest description",
    "image": "",
    "date": "2022-05-01",
    "length": 120,
    "video": "",
    "category": 1,
    "classification": 3
}
```



### 8. localhost:8080/api/movie/update


Update a movie


***Endpoint:***

```bash
Method: PATCH
Type: RAW
URL: localhost:8080/api/movie/update
```



***Body:***

```js        
{
    "id": 101,
    "title": "MovieTest",
    "description": "MovieTest description",
    "image": "",
    "date": "2022-05-01",
    "length": 120,
    "video": "",
    "category": 1,
    "classification": 3
}
```



### 9. localhost:8080/api/movie/updateviews


Update a movie's views


***Endpoint:***

```bash
Method: PATCH
Type: RAW
URL: localhost:8080/api/movie/updateviews/6
```



### 10. localhost:8080/api/movie/updateranking


Update a movie's rank


***Endpoint:***

```bash
Method: PATCH
Type: RAW
URL: localhost:8080/api/movie/updateranking
```



***Body:***

```js        
{
    "id": 6,
    "ranking": 3
}
```



### 11. localhost:8080/api/movie/delete


Delete a movie from ID


***Endpoint:***

```bash
Method: DELETE
Type: RAW
URL: localhost:8080/api/movie/delete/6
```



## Pricing

Routes for pricing



### 1. localhost:8080/api/pricing/list


Get list of pricings


***Endpoint:***

```bash
Method: GET
Type: RAW
URL: localhost:8080/api/pricing/list
```



### 2. localhost:8080/api/pricing/show


Get a pricing from ID


***Endpoint:***

```bash
Method: GET
Type: RAW
URL: localhost:8080/api/pricing/show/1
```



### 3. localhost:8080/api/pricing/add


Add a pricing


***Endpoint:***

```bash
Method: POST
Type: RAW
URL: localhost:8080/api/pricing/add
```



***Body:***

```js        
{
    "title": "Pouet",
    "price": 1000
}
```



### 4. localhost:8080/api/pricing/update


Update a pricing


***Endpoint:***

```bash
Method: PATCH
Type: RAW
URL: localhost:8080/api/pricing/update
```



***Body:***

```js        
{
    "id": 4,
    "title": "Pouet",
    "price": 50000
}
```



### 5. localhost:8080/api/pricing/delete


Delete a pricing from ID


***Endpoint:***

```bash
Method: DELETE
Type: RAW
URL: localhost:8080/api/pricing/delete/4
```



## MovieActor

Routes for movie_actor



### 1. localhost:8080/api/movieactor/list


Get the list of movie_actors


***Endpoint:***

```bash
Method: GET
Type: RAW
URL: localhost:8080/api/movieactor/list
```



### 2. localhost:8080/api/movieactor/showmovies


Get movies from actor's ID


***Endpoint:***

```bash
Method: GET
Type: RAW
URL: localhost:8080/api/movieactor/showmovies/10
```



### 3. localhost:8080/api/movieactor/showactors


Get actors from movie's ID


***Endpoint:***

```bash
Method: GET
Type: RAW
URL: localhost:8080/api/movieactor/showactors/10
```



### 4. localhost:8080/api/movieactor/add


Add a relation between actor and movie


***Endpoint:***

```bash
Method: POST
Type: RAW
URL: localhost:8080/api/movieactor/add
```



***Body:***

```js        
{
    "movie": 10,
    "actor": 20
}
```



### 5. localhost:8080/api/movieactor/delete


Delete a relation between actor and movie


***Endpoint:***

```bash
Method: DELETE
Type: RAW
URL: localhost:8080/api/movieactor/delete
```



***Body:***

```js        
{
    "movie": 10,
    "actor": 20
}
```



## Favorite

Routes for favorite



### 1. localhost:8080/api/favorite/list


Get the list of favorites


***Endpoint:***

```bash
Method: GET
Type: RAW
URL: localhost:8080/api/favorite/list
```



### 2. localhost:8080/api/favorite/show


Get a favorite from member ID


***Endpoint:***

```bash
Method: GET
Type: RAW
URL: localhost:8080/api/favorite/show/1
```



### 3. localhost:8080/api/favorite/add


Update a favorite


***Endpoint:***

```bash
Method: POST
Type: RAW
URL: localhost:8080/api/favorite/add
```



***Body:***

```js        
{
    "member": 1,
    "movie": 100
}
```



### 4. localhost:8080/api/favorite/delete


Delete a movie from favorite using member ID and movie ID


***Endpoint:***

```bash
Method: DELETE
Type: RAW
URL: localhost:8080/api/favorite/delete
```



***Body:***

```js        
{
    "member": 1,
    "movie": 100
}
```



## Statistics

Routes for statistics



### 1. localhost:8080/api/statistics/showmostwatched


Get a list of the 5 most watched movies


***Endpoint:***

```bash
Method: GET
Type: RAW
URL: localhost:8080/api/statistics/showmostwatched
```



### 2. localhost:8080/api/statistics/showlatest


Get a list of the last 10 movies


***Endpoint:***

```bash
Method: GET
Type: RAW
URL: localhost:8080/api/statistics/showlatest
```



### 3. localhost:8080/api/statistics/showmarkettings


Get a list of 3 random markettings


***Endpoint:***

```bash
Method: GET
Type: RAW
URL: localhost:8080/api/statistics/showmarkettings
```

---
[Back to top](#mds-motion_streaming)