import '../css/footer.scss';

const Footer = class {
  constructor() {
    this.el = document.querySelector('#footer');
  }

  render = () => (`
    <div class="separator"></div>
    <div class="footer-logo">
      <a class="footer-brand" href="/">
        <h1 class="motion-streaming">MOTION STREAMING</h1>
      </a>
    </div>
    <div class="footer-links">
      <div class="left-links">
        <a class="feedback">
          <p class="p1">Feedback</p>
        </a>
        <span class="v-separator"></span>
        <a class="help">
          <p class="p1">Help</p>
        </a>
        <span class="v-separator"></span>
        <a class="faq">
          <p class="p1">FAQ</p>
        </a>
      </div>
      <div class="right-links">
        <p class="subtitle-v2-semi">FOLLOW ON</p>
        <a class="facebook">
          <i class="fa-brands fa-facebook"></i>
        </a>
        <span class="v-separator"></span>
        <a class="instagram">
          <i class="fa-brands fa-instagram"></i>
        </a>
      </div>
    </div>
    <div class="copyrights">
      <p class="p1">©2022 - All rights reserved</p>
    </div>
  `);

  run = () => { this.el.innerHTML = this.render(); };
};

export default Footer;
