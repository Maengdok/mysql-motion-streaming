const SignIn = class {
  constructor() {
    this.el = document.querySelector('#body');
  }

  render = () => (`
    <h2>SignIn</h2>
    <div class="signin">
      <form>
        <label for="firstname">Prénom</label>
        <input type="text" id="firstname" required />
        <label for"lastname">Nom</label>
        <input type="text" id="lastname" required />
        <label for="email">Email</label>
        <input type="email" id="email" required />
        <label for="password">Mot de passe</label>
        <input type="password" id="password" required />
        <label for="confirm-password">Confirmer mot de passe</label>
        <input type="password" id="confirm-password" required />
        <label for="address">Adresse</label>
        <input type="text" id="address" required />
        <label for="zipcode">Code postal</label>
        <input type="text" id="zipcode" required />
        <label for="city">Ville</label>
        <input type="text" id="city" required />
      </form>
    </div>
  `);

  run = () => { this.el.innerHTML = this.render(); };
};

export default SignIn;
