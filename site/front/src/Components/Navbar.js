import axios from 'axios';
import '../css/navbar.scss';

const Navbar = class {
  constructor() {
    this.navbar = document.querySelector('#navbar');
  }

  search = (title) => {
    const result = document.querySelector('#result');
    axios.get(`http://127.0.0.1:8080/api/movie/search/${title}`, {
      host: '127.0.0.1:8080',
      headers: {
        Accept: '*/*',
        'content-type': 'application/json',
        'x-frame-options': ''
      }
    })
      .then((res) => {
        res.data.forEach(
          (item) => { result.innerHTML += this.renderResult(item); }
        );
        result.className = 'show';
      })
      .catch((err) => { throw new Error(err); });
  };

  renderResult = (data) => {
    const {
      id,
      title,
      image,
      date,
      length,
      category
    } = data;
    const formatDate = date.split('T')[0];
    const reg = /^[12][0-9]{3}/;
    const regexp = new RegExp(reg, 'g');
    const year = regexp.exec(formatDate);

    return `
      <a href="movie?id=${id}">
        <img src=${image}/>
        <div class="result-body">
          <p class="p1">${title}</p>
          <p class="p1">${category} • ${year} • ${length} min</p>
        </div>
      </a>
    `;
  };

  renderNavbar = () => (`
      <div class="nav-element">
        <div class="nav-logo">
          <a class="nav-brand" href="/">
            <h1 class="motion-streaming">MOTION STREAMING</h1>
          </a>
        </div>
        <div class="nav-links">
          <ul class="nav-links-elements">
            <li class="nav-home">
              <a class="home" href="/">Accueil</a>
            </li>
            <li class="nav-movies">
              <a class="movies" href="movies">Films</a>
            </li>
            <li class="nav-action">
              <a class="action" href="category?id=3">Action</a>
            </li>
            <li class="nav-drama">
              <a class="drama" href="category?id=6">Drame</a>
            </li>
            <li class="nav-comedy">
              <a class="comedy" href="category?id=7">Comédie</a>
            </li>
          </ul>
        </div>
        <div class="nav-search">
          <input id="search" type="text"></input>
          <div id="result" class="hidden">
          </div>
        </div>
        <div class="nav-sign">
          <a href="signin">
            <button class="button-v1">Sign in</button>
          </a>
        </div>
      </div>
    `);

  run = () => {
    this.navbar.innerHTML = this.renderNavbar();

    const search = document.querySelector('#search');
    search.addEventListener('keypress', (e) => {
      if (e.key === 'Enter') {
        this.search(search.value);
      }
    });

    const resultDiv = document.querySelector('#result');
    search.addEventListener('keypress', (e) => {
      if (e.key === 'Escape') {
        resultDiv.className = 'hidden';
      }
    });

    document.addEventListener('click', (e) => {
      if (!resultDiv.contains(e.target)) {
        resultDiv.className = 'hidden';
      }
    });

    // const resultA = document.querySelector('#result a');
    // resultA.addEventListener('click', () => {
    //   resultDiv.className = 'hidden';
    // });
  };
};

export default Navbar;
