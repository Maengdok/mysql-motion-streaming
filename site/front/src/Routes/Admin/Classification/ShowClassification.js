const ShowClassification = class {
  constructor() {
    this.el = document.querySelector('#body');
  }

  render = () => ('<h2>Show Classification</h2>');

  run = () => { this.el.innerHTML = this.render(); };
};

export default ShowClassification;
