const AddClassification = class {
  constructor() {
    this.el = document.querySelector('#body');
  }

  render = () => ('<h2>Add Classification</h2>');

  run = () => { this.el.innerHTML = this.render(); };
};

export default AddClassification;
