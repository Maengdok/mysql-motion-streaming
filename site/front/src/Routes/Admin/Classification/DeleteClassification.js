const DeleteClassification = class {
  constructor() {
    this.el = document.querySelector('#body');
  }

  render = () => ('<h2>Delete Classification</h2>');

  run = () => { this.el.innerHTML = this.render(); };
};

export default DeleteClassification;
