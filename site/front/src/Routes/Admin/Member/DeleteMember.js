const DeleteMember = class {
  constructor() {
    this.el = document.querySelector('#body');
  }

  render = () => ('<h2>Delete Member</h2>');

  run = () => { this.el.innerHTML = this.render(); };
};

export default DeleteMember;
