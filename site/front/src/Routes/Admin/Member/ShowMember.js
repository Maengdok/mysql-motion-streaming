const ShowMember = class {
  constructor() {
    this.el = document.querySelector('#body');
  }

  render = () => ('<h2>Show Member</h2>');

  run = () => { this.el.innerHTML = this.render(); };
};

export default ShowMember;
