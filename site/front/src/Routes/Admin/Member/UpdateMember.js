const UpdateMember = class {
  constructor() {
    this.el = document.querySelector('#body');
  }

  render = () => ('<h2>Update Member</h2>');

  run = () => { this.el.innerHTML = this.render(); };
};

export default UpdateMember;
