const AddMovie = class {
  constructor() {
    this.el = document.querySelector('#body');
  }

  render = () => ('<h2>Add Movie</h2>');

  run = () => { this.el.innerHTML = this.render(); };
};

export default AddMovie;
