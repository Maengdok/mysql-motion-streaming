const DeleteMovie = class {
  constructor() {
    this.el = document.querySelector('#body');
  }

  render = () => ('<h2>Delete Movie</h2>');

  run = () => { this.el.innerHTML = this.render(); };
};

export default DeleteMovie;
