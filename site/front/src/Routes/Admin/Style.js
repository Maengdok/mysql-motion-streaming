const Style = class {
  constructor() {
    this.el = document.querySelector('#body');
  }

  render = () => (`
    <h1>Header 1</h1>
    <h2>Header 2</h2>
    <h3>Header 3</h3>
    <h4>Header 4</h4>
    <h5 class="subtitle">Subtitle</h5>
    <h6 class="subtitle-v2-reg">Subtitle 2 Regular</h6>
    <h6 class="subtitle-v2-med">Subtitle 2 Medium</h6>
    <h6 class="subtitle-v2-semi">Subtitle 2 SemiBold</h6>
    <button class="button-v1">Button text</button>
    <button class="button-v2">Button text 
      <i class="fa-light fa-arrow-right"></i>
    </button>
    <p class="p1">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam faucibus commodo ex. Donec et libero condimentum, egestas libero non, posuere nisl.</p>
    <p class="p2">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam faucibus commodo ex. Donec et libero condimentum, egestas libero non, posuere nisl.</p>
    <p class="p3">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam faucibus commodo ex. Donec et libero condimentum, egestas libero non, posuere nisl.</p>
    <i class="fa-solid fa-chevron-right"></i>
    `
  );

  run = () => { this.el.innerHTML = this.render(); };
};

export default Style;
