const ShowPricing = class {
  constructor() {
    this.el = document.querySelector('#body');
  }

  render = () => ('<h2>Show Pricing</h2>');

  run = () => { this.el.innerHTML = this.render(); };
};

export default ShowPricing;
