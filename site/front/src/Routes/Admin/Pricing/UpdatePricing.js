const UpdatePricing = class {
  constructor() {
    this.el = document.querySelector('#body');
  }

  render = () => ('<h2>Update Pricing</h2>');

  run = () => { this.el.innerHTML = this.render(); };
};

export default UpdatePricing;
