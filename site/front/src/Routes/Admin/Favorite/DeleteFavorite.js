const DeleteFavorite = class {
  constructor() {
    this.el = document.querySelector('#body');
  }

  render = () => ('<h2>Delete Favorite</h2>');

  run = () => { this.el.innerHTML = this.render(); };
};

export default DeleteFavorite;
