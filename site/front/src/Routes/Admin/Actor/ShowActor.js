const ShowActor = class {
  constructor() {
    this.el = document.querySelector('#body');
  }

  render = () => ('<h2>Show Actor</h2>');

  run = () => { this.el.innerHTML = this.render(); };
};

export default ShowActor;
