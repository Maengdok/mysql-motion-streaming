const DeleteActor = class {
  constructor() {
    this.el = document.querySelector('#body');
  }

  render = () => ('<h2>Delete Actor</h2>');

  run = () => { this.el.innerHTML = this.render(); };
};

export default DeleteActor;
