const ShowCategory = class {
  constructor() {
    this.el = document.querySelector('#body');
  }

  render = () => ('<h2>Show Category</h2>');

  run = () => { this.el.innerHTML = this.render(); };
};

export default ShowCategory;
