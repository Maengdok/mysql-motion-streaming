const DeleteCategory = class {
  constructor() {
    this.el = document.querySelector('#body');
  }

  render = () => ('<h2>Delete Category</h2>');

  run = () => { this.el.innerHTML = this.render(); };
};

export default DeleteCategory;
