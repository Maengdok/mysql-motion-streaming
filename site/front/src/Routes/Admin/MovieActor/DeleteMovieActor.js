const DeleteMovieActor = class {
  constructor() {
    this.el = document.querySelector('#body');
  }

  render = () => ('<h2>Delete MovieActor</h2>');

  run = () => { this.el.innerHTML = this.render(); };
};

export default DeleteMovieActor;
