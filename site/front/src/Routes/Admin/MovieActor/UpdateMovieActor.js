const UpdateMovieActor = class {
  constructor() {
    this.el = document.querySelector('#body');
  }

  render = () => ('<h2>Update MovieActor</h2>');

  run = () => { this.el.innerHTML = this.render(); };
};

export default UpdateMovieActor;
