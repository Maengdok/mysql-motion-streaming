import axios from 'axios';
import '../css/category.scss';

const Category = class {
  constructor() {
    this.el = document.querySelector('#body');
    this.id = 0;
  }

  getCategory = (category) => {
    const categoryTitle = document.querySelector('#title');
    axios.get(`http://127.0.0.1:8080/api/category/show/${category}`, {
      host: '127.0.0.1:8080',
      headers: {
        Accept: '*/*',
        'content-type': 'application/json',
        'x-frame-options': ''
      }
    })
      .then((res) => { categoryTitle.innerHTML = res.data.title; })
      .catch((err) => { throw new Error(err); });
  };

  getMovies = (id) => {
    const category = document.querySelector('#category');
    axios.get(`http://127.0.0.1:8080/api/movie/category/${id}`, {
      host: '127.0.0.1:8080',
      headers: {
        Accept: '*/*',
        'content-type': 'application/json'
      }
    })
      .then((res) => {
        res.data.forEach(
          (item) => { category.innerHTML += this.renderMovies(item); }
        );
      })
      .catch((err) => { throw new Error(err); });
  };

  renderMovies = (data) => {
    const {
      idMovie,
      title,
      imageMin,
      date,
      length
    } = data;
    const formatDate = date.split('T')[0];
    const reg = /^[12][0-9]{3}/;
    const regexp = new RegExp(reg, 'g');
    const year = regexp.exec(formatDate);

    return `
      <div class="movie${idMovie}">
        <a href="movie?id=${idMovie}"> 
          <div class="movie-body">
            <p>${title}</p>
            <p>${year} - ${length} min</p>
          </div>
          <img src="${imageMin}" />
        </a>
      </div>
    `;
  };

  render = () => (`
    <h2 id="title">Category</h2>
    <div id="category">
    </div>
  `);

  run = () => {
    const id = window.location.search.split('?id=')[1];
    this.id = id;
    this.el.innerHTML = this.render();
    this.getMovies(this.id);
    this.getCategory(this.id);
  };
};

export default Category;
