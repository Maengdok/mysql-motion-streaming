import axios from 'axios';
import '../css/movies.scss';

const Movies = class {
  constructor() {
    this.el = document.querySelector('#body');
  }

  getCategories = () => {
    const categories = document.querySelector('#categories');
    axios.get('http://127.0.0.1:8080/api/category/list', {
      host: '127.0.0.1:8080',
      headers: {
        Accept: '*/*',
        'content-type': 'application/json'
      }
    })
      .then((res) => {
        res.data.forEach(
          (item) => { categories.innerHTML += this.renderCategories(item); }
        );
      })
      .catch((err) => { throw new Error(err); });
  };

  getMovies = () => {
    const movies = document.querySelector('#movie-list');
    axios.get('http://127.0.0.1:8080/api/movie/list', {
      host: '127.0.0.1:8080',
      headers: {
        Accept: '*/*',
        'content-type': 'application/json'
      }
    })
      .then((res) => {
        res.data.forEach(
          (item) => { movies.innerHTML += this.renderMovies(item); }
        );
      })
      .catch((err) => { throw new Error(err); });
  };

  renderCategories = (data) => {
    const { idCategory, title, picture } = data;

    return `
      <li class="category${idCategory}">
        <a href="category?id=${idCategory}">
          <p class="subtitle-v2-reg">${title}</p>
          <img src="${picture}" />
        </a>
      </li>`;
  };

  renderMovies = (data) => {
    const {
      idMovie,
      title,
      imageMin,
      date,
      length
    } = data;
    const formatDate = date.split('T')[0];
    const reg = /^[12][0-9]{3}/;
    const regexp = new RegExp(reg, 'g');
    const year = regexp.exec(formatDate);

    return `
      <div class="movie${idMovie}">
        <a href="movie?id=${idMovie}"> 
          <div class="movie-body">
            <p>${title}</p>
            <p>${year} - ${length} min</p>
          </div>
          <img src="${imageMin}" />
        </a>
      </div>
    `;
  };

  render = () => (`
    <h2 class="movies-title">Tous les films</h2>
    <div id="categories">
    </div>
    <div id="movie-list">
    </div>`
  );

  run = () => {
    this.el.innerHTML = this.render();
    this.getMovies();
    this.getCategories();
  };
};

export default Movies;
