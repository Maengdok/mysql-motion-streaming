import axios from 'axios';
import '../css/dashboard.scss';

const Dashboard = class {
  constructor() {
    this.el = document.querySelector('#body');
    this.counter = 0;
    this.idMovie = 0;
  }

  previousMovie = () => {
    const slides = document.querySelectorAll('.most-watched-details');
    const size = slides[0].clientWidth;

    slides[this.counter].style.transform = `translateX(${(+size * this.counter)}px)`;
    slides[this.counter].style.transition = 'transform 0.8s ease-in-out';
    if (this.counter <= 0) return;
    this.counter -= 1;
    slides[this.counter].style.transform = `translateX(${(+size * this.counter)}px)`;
  };

  nextMovie = () => {
    const slides = document.querySelectorAll('.most-watched-details');
    const size = slides[0].clientWidth;

    slides[this.counter].style.transform = `translateX(${(-size * this.counter)}px)`;
    if (this.counter >= slides.length - 1) return;
    slides[this.counter + 1].style.transition = 'transform 0.8s ease-in-out';
    this.counter += 1;
    slides[this.counter].style.transform = `translateX(${(-size * this.counter)}px)`;
  };

  getCategories = () => {
    const categories = document.querySelector('#categories');
    axios.get('http://127.0.0.1:8080/api/category/list', {
      host: '127.0.0.1:8080',
      headers: {
        Accept: '*/*',
        'content-type': 'application/json'
      }
    })
      .then((res) => {
        res.data.forEach(
          (item) => { categories.innerHTML += this.renderCategories(item); }
        );
      })
      .catch((err) => { throw new Error(err); });
  };

  get3RandomAdvertises = () => {
    const advertises = document.querySelector('#ad');
    axios.get('http://127.0.0.1:8080/api/statistics/showads', {
      host: '127.0.0.1:8080',
      headers: {
        Accept: '*/*',
        'content-type': 'application/json'
      }
    })
      .then((res) => {
        res.data.forEach(
          (item) => { advertises.innerHTML += this.renderAdvertises(item); }
        );
      })
      .catch((err) => { throw new Error(err); });
  };

  get10LatestMovies = () => {
    const latestMovies = document.querySelector('#latest-movies');
    axios.get('http://127.0.0.1:8080/api/statistics/showlatest', {
      host: '127.0.0.1:8080',
      headers: {
        Accept: '*/*',
        'content-type': 'application/json'
      }
    })
      .then((res) => {
        res.data.forEach(
          (item) => { latestMovies.innerHTML += this.renderLatestMovies(item); }
        );
      })
      .catch((err) => { throw new Error(err); });
  };

  get5MostWatchedMovies = () => {
    const mostWatched = document.querySelector('#most-watched');
    axios.get('http://127.0.0.1:8080/api/statistics/showmostwatched', {
      host: '127.0.0.1:8080',
      headers: {
        Accept: '*/*',
        'content-type': 'application/json'
      }
    })
      .then((res) => {
        res.data.forEach(
          (item) => {
            mostWatched.innerHTML += this.renderMostWatched(item);
          }
        );
      })
      .catch((err) => { throw new Error(err); });
  };

  renderCategories = (data) => {
    const { idCategory, title, picture } = data;

    return `
      <li class="category${idCategory}">
        <a href="category?id=${idCategory}">
          <p class="subtitle-v2-reg">${title}</p>
          <img src="${picture}" />
        </a>
      </li>`;
  };

  renderAdvertises = (data) => {
    const {
      title,
      content,
      link,
      picture,
      video
    } = data;
    let toShow = null;

    if (picture && !video) toShow = `<img src="${picture}" />'`;
    else if (!picture && video) {
      toShow = `<iframe width="400" height="250" src="${video}"frameborder="0" allowfullscreen></iframe>`;
    } else if (!picture && !video) toShow = '<img src="https://miro.medium.com/max/1400/1*Mo_uCTtjeMik8HXAHLYdVQ.jpeg" />';

    return `
      <div class="random-ad">
        <a href="${link}">
          <p class="subtitle">${title}</p>
          <p class="p2">${content}</p>
          ${toShow}
        </a>
      </div>
    `;
  };

  renderLatestMovies = (data) => {
    const {
      idMovie,
      date,
      description,
      imagePortrait,
      length,
      title,
      category
    } = data;
    const formatDate = date.split('T')[0];
    const reg = /^[12][0-9]{3}/;
    const regexp = new RegExp(reg, 'g');
    const year = regexp.exec(formatDate);

    return `
      <div class="latest-details" id="movie${idMovie}">
      <a class="latest-details-body" href="movie?id=${idMovie}">
        <p class="subtitle">${title}</p>
        <p class="p1">${category} • ${year} • ${length} min</p>
        <p class="p2">${description}</p>
      </a>
      <img src="${imagePortrait}" />
      </div>
    `;
  };

  renderMostWatched = (data) => {
    const {
      idMovie,
      date,
      description,
      imageMax,
      length,
      title,
      category,
      classification
    } = data;
    const formatDate = date.split('T')[0];
    const reg = /^[12][0-9]{3}/;
    const regexp = new RegExp(reg, 'g');
    const year = regexp.exec(formatDate);

    return `
      <div class="most-watched-details">
        <div class="most-watched-body">
          <div class="details-body">
            <h3>${title}</h3>
            <p class="subtitle-v2-semi"><span class="category-title">${category}</span> • <span class="classification-title">${classification}</span> • ${year[0]} • ${length} min</p>
            <p class="p2">${description}</p>
            <div class="buttons">
              <a href="movie?id=${idMovie}">
                <button class="play-button">
                  <div class="circle">
                    <i class="fa-solid fa-play"></i>
                  </div>
                  <p class="p2">Regarder</p>
                </button>
              </a>
              <button class="watchlist-button">
                <i class="fa-solid fa-plus"></i>
                Watchlist
              </button>
            </div>
          </div>
        </div>
        <img src="${imageMax}" />
      </div>
    `;
  };

  render = () => (`
    <div class="carousel">
      <div id="most-watched">
      </div>
      <button class="prev-next" id="prev"><i class="fa-solid fa-chevron-left"></i></button>
      <button class="prev-next" id="next"><i class="fa-solid fa-chevron-right"></i></button>
    </div>
    <div id="categories">
    </div>
    <h3 class="latest-movies-h3">Derniers films</h3>
    <div id="latest-movies">
    </div>
    <h3 class="ad-h3">Publicités</h3>
    <ul id="ad">
    </ul>
  `);

  waitForElm(selector) {
    return new Promise((resolve) => {
      if (document.querySelector(selector)) {
        resolve(document.querySelector(selector));
      }

      const observer = new MutationObserver(() => {
        if (document.querySelector(selector)) {
          resolve(document.querySelector(selector));
          observer.disconnect();
        }
        return 1;
      });

      observer.observe(document.body, {
        childList: true,
        subtree: true
      });
    });
  }

  run = () => {
    this.el.innerHTML = this.render();

    this.get10LatestMovies();
    this.get5MostWatchedMovies();
    this.get3RandomAdvertises();
    this.getCategories();

    const prevBtn = document.querySelector('#prev');
    const nextBtn = document.querySelector('#next');
    prevBtn.addEventListener('click', () => this.previousMovie());
    nextBtn.addEventListener('click', () => this.nextMovie());
  };
};

export default Dashboard;
