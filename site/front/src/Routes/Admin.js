const Admin = class {
  constructor() {
    this.el = document.querySelector('#body');
  }

  render = () => ('<h2>Admin</h2>');

  run = () => { this.el.innerHTML = this.render(); };
};

export default Admin;
