import '../css/movie.scss';

import axios from 'axios';

const Movie = class {
  constructor() {
    this.el = document.querySelector('#body');
    this.id = 0;
  }

  getMoviesCategory = (category) => {
    const related = document.querySelector('.related-section');
    axios.get(`http://127.0.0.1:8080/api/movie/category/${category}`, {
      host: '127.0.0.1:8080',
      headers: {
        Accept: '*/*',
        'content-type': 'application/json'
      }
    })
      .then((res) => {
        res.data.forEach(
          (item) => { related.innerHTML += this.renderRelated(item); }
        );
      })
      .catch((err) => { throw new Error(err); });
  };

  get6ForYouMovies = (category) => {
    const forYou = document.querySelector('.for-you-section');
    axios.get(`http://127.0.0.1:8080/api/movie/foryou/${category}`, {
      host: '127.0.0.1:8080',
      headers: {
        Accept: '*/*',
        'content-type': 'application/json'
      }
    })
      .then((res) => {
        res.data.forEach(
          (item) => { forYou.innerHTML += this.renderForYou(item); }
        );
      })
      .catch((err) => { throw new Error(err); });
  };

  getMovie = (id) => {
    const movie = document.querySelector('.movie');
    axios.get(`http://127.0.0.1:8080/api/movie/show/${id}`, {
      host: '127.0.0.1:8080',
      headers: {
        Accept: '*/*',
        'content-type': 'application/json'
      }
    })
      .then((res) => {
        this.get6ForYouMovies(res.data.idCategory);
        this.getMoviesCategory(res.data.idCategory);
        movie.innerHTML += this.renderMovie(res.data);
      })
      .catch((err) => { throw new Error(err); });
  };

  renderRelated = (data) => {
    const {
      idMovie,
      title,
      imageMin
    } = data;

    return `
      <div class="related-movie${idMovie}">
        <a href="movie?id=${idMovie}">
          <p class="p1">${title}</p>
          <img src="${imageMin}" />
        </a>
      </div>
    `;
  };

  renderForYou = (data) => {
    const {
      idMovie,
      title,
      imagePortrait
    } = data;

    return `
      <div class="for-you-movie${idMovie}">
        <a href="movie?id=${idMovie}">
          <p class="p1">${title}</p>
          <img src="${imagePortrait}" />
        </a>
      </div>
    `;
  };

  renderMovie = (data) => {
    const {
      title,
      imageMax,
      description,
      date,
      length,
      ranking,
      vote,
      view,
      video,
      category,
      classification
    } = data;
    const formatDate = date.split('T')[0];
    const reg = /^[12][0-9]{3}/;
    const regexp = new RegExp(reg, 'g');
    const year = regexp.exec(formatDate);

    return `
      <div class="movie-section">
        <a href="${video}">
          <div class="circle">
            <i class="fa-solid fa-play"></i>
          </div>
          <img src="${imageMax}" />
        </a>
        <div class="details">
          <div class="title-buttons">
            <div class="title-date-category">
              <h4>${title}</h4>
              <p class="subtitle">${year} • ${length} • ${ranking} / 5 • ${vote} votes • ${view} vues</p>
              <p class="p3"><span class="category-title">${category}</span> - <span class="classification-title">${classification}</span></p>
            </div>
            <div class="buttons">
              <button class="buttons-watchlist-button">
                <i class="fa-solid fa-plus"></i>
                Watchlist
              </button>
              <button class="share-button">
                <i class="fa-solid fa-share"></i>
                Partager
              </button>
              <button class="download-button">
                <i class="fa-solid fa-download"></i>
                Télécharger
              </button>
            </div>
          </div>
          <div class="description">
            <p class="subtitle">Description</p>
            <p class="p3">${description}</p>
          </div>
        </div>
      </div>
    `;
  };

  render = () => (`
    <div id="movie">
      <div class="movie-for-you">
        <div class="movie">
        </div>
        <div class="for-you">
          <h5>Pour vous</h5>
          <div class="for-you-section">
          </div>
        </div>
      </div>
      <div class="related">
        <h5>Vidéos similaires</h5>
        <div class="related-section">
        </div>
      </div>
    </div>
  `);

  run = () => {
    const id = window.location.search.split('?id=')[1];
    this.id = id;
    this.el.innerHTML = this.render();
    this.getMovie(this.id);
  };
};

export default Movie;
