import axios from 'axios';
import './css/index.scss';
import Navbar from './Components/Navbar.js';
import Footer from './Components/Footer.js';
import Error404 from './Routes/Error404.js';
import Dashboard from './Routes/Dashboard.js';
import Movie from './Routes/Movie.js';
import Category from './Routes/Category.js';
import Admin from './Routes/Admin.js';
import Style from './Routes/Admin/Style.js';
import Movies from './Routes/Movies.js';
import SignIn from './Components/SignIn.js';
import AddActor from './Routes/Admin/Actor/AddActor.js';
import DeleteActor from './Routes/Admin/Actor/DeleteActor.js';
import ListActors from './Routes/Admin/Actor/ListActors.js';
import ShowActor from './Routes/Admin/Actor/ShowActor.js';
import UpdateActor from './Routes/Admin/Actor/UpdateActor.js';
import AddCategory from './Routes/Admin/Category/AddCategory.js';
import DeleteCategory from './Routes/Admin/Category/DeleteCategory.js';
import ListCategories from './Routes/Admin/Category/ListCategories.js';
import ShowCategory from './Routes/Admin/Category/ShowCategory.js';
import UpdateCategory from './Routes/Admin/Category/UpdateCategory.js';
import AddClassification from './Routes/Admin/Classification/AddClassification.js';
import DeleteClassification from './Routes/Admin/Classification/DeleteClassification.js';
import ListClassifications from './Routes/Admin/Classification/ListClassifications.js';
import ShowClassification from './Routes/Admin/Classification/ShowClassification.js';
import UpdateClassification from './Routes/Admin/Classification/UpdateClassification.js';
import AddFavorite from './Routes/Admin/Favorite/AddFavorite.js';
import DeleteFavorite from './Routes/Admin/Favorite/DeleteFavorite.js';
import ListFavorites from './Routes/Admin/Favorite/ListFavorites.js';
import ShowFavorite from './Routes/Admin/Favorite/ShowFavorite.js';
import UpdateFavorite from './Routes/Admin/Favorite/UpdateFavorite.js';
import AddMarketting from './Routes/Admin/Marketting/AddMarketting.js';
import DeleteMarketting from './Routes/Admin/Marketting/DeleteMarketting.js';
import ListMarkettings from './Routes/Admin/Marketting/ListMarkettings.js';
import ShowMarketting from './Routes/Admin/Marketting/ShowMarketting.js';
import UpdateMarketting from './Routes/Admin/Marketting/UpdateMarketting.js';
import AddMember from './Routes/Admin/Member/AddMember.js';
import DeleteMember from './Routes/Admin/Member/DeleteMember.js';
import ListMembers from './Routes/Admin/Member/ListMembers.js';
import ShowMember from './Routes/Admin/Member/ShowMember.js';
import UpdateMember from './Routes/Admin/Member/UpdateMember.js';
import AddMemberMovie from './Routes/Admin/MemberMovie/AddMemberMovie.js';
import DeleteMemberMovie from './Routes/Admin/MemberMovie/DeleteMemberMovie.js';
import ListMemberMovies from './Routes/Admin/MemberMovie/ListMemberMovies.js';
import ShowMemberMovie from './Routes/Admin/MemberMovie/ShowMemberMovie.js';
import UpdateMemberMovie from './Routes/Admin/MemberMovie/UpdateMemberMovie.js';
import AddMovie from './Routes/Admin/Movie/AddMovie.js';
import DeleteMovie from './Routes/Admin/Movie/DeleteMovie.js';
import ListMovies from './Routes/Admin/Movie/ListMovies.js';
import ShowMovie from './Routes/Admin/Movie/ShowMovie.js';
import UpdateMovie from './Routes/Admin/Movie/UpdateMovie.js';
import AddMovieActor from './Routes/Admin/MovieActor/AddMovieActor.js';
import DeleteMovieActor from './Routes/Admin/MovieActor/DeleteMovieActor.js';
import ListMovieActors from './Routes/Admin/MovieActor/ListMovieActors.js';
import ShowMovieActor from './Routes/Admin/MovieActor/ShowMovieActor.js';
import UpdateMovieActor from './Routes/Admin/MovieActor/UpdateMovieActor.js';
import AddPricing from './Routes/Admin/Pricing/AddPricing.js';
import DeletePricing from './Routes/Admin/Pricing/DeletePricing.js';
import ListPricings from './Routes/Admin/Pricing/ListPricings.js';
import ShowPricing from './Routes/Admin/Pricing/ShowPricing.js';
import UpdatePricing from './Routes/Admin/Pricing/UpdatePricing.js';

const Routes = class Routing {
  getPath = () => {
    const arrayPath = window.location.pathname.split('/');
    arrayPath.shift();

    return arrayPath.join('/');
  };

  run = () => {
    const navbar = new Navbar();
    const footer = new Footer();
    const error404 = new Error404();
    const dashboard = new Dashboard();
    const movie = new Movie();
    const signIn = new SignIn();
    const category = new Category();
    const admin = new Admin();
    const style = new Style();
    const movies = new Movies();
    const addActor = new AddActor();
    const deleteActor = new DeleteActor();
    const listActors = new ListActors();
    const showActor = new ShowActor();
    const updateActor = new UpdateActor();
    const addCategory = new AddCategory();
    const deleteCategory = new DeleteCategory();
    const listCategories = new ListCategories();
    const showCategory = new ShowCategory();
    const updateCategory = new UpdateCategory();
    const addClassification = new AddClassification();
    const deleteClassification = new DeleteClassification();
    const listClassifications = new ListClassifications();
    const showClassification = new ShowClassification();
    const updateClassification = new UpdateClassification();
    const addFavorite = new AddFavorite();
    const deleteFavorite = new DeleteFavorite();
    const listFavorites = new ListFavorites();
    const showFavorite = new ShowFavorite();
    const updateFavorite = new UpdateFavorite();
    const addMarketting = new AddMarketting();
    const deleteMarketting = new DeleteMarketting();
    const listMarkettings = new ListMarkettings();
    const showMarketting = new ShowMarketting();
    const updateMarketting = new UpdateMarketting();
    const addMember = new AddMember();
    const deleteMember = new DeleteMember();
    const listMembers = new ListMembers();
    const showMember = new ShowMember();
    const updateMember = new UpdateMember();
    const addMemberMovie = new AddMemberMovie();
    const deleteMemberMovie = new DeleteMemberMovie();
    const listMemberMovies = new ListMemberMovies();
    const showMemberMovie = new ShowMemberMovie();
    const updateMemberMovie = new UpdateMemberMovie();
    const addMovie = new AddMovie();
    const deleteMovie = new DeleteMovie();
    const listMovies = new ListMovies();
    const showMovie = new ShowMovie();
    const updateMovie = new UpdateMovie();
    const addMovieActor = new AddMovieActor();
    const deleteMovieActor = new DeleteMovieActor();
    const listMovieActors = new ListMovieActors();
    const showMovieActor = new ShowMovieActor();
    const updateMovieActor = new UpdateMovieActor();
    const addPricing = new AddPricing();
    const deletePricing = new DeletePricing();
    const listPricings = new ListPricings();
    const showPricing = new ShowPricing();
    const updatePricing = new UpdatePricing();

    navbar.run();
    footer.run();

    switch (this.getPath()) {
      case '':
        dashboard.run();
        break;
      case 'movie':
        movie.run();
        break;
      case 'admin':
        admin.run();
        break;
      case 'style':
        style.run();
        break;
      case 'movies':
        movies.run();
        break;
      case 'signin':
        signIn.run();
        break;
      case 'category':
        category.run();
        break;
      // #region crud actor
      case 'admin-actor-add':
        addActor.run();
        break;
      case 'admin-actor-delete':
        deleteActor.run();
        break;
      case 'admin-actor':
        listActors.run();
        break;
      case 'admin-actor-show':
        showActor.run();
        break;
      case 'admin-actor-update':
        updateActor.run();
        break;
      // #endregion
      // #region crud category
      case 'admin-category-add':
        addCategory.run();
        break;
      case 'admin-category-delete':
        deleteCategory.run();
        break;
      case 'admin-category':
        listCategories.run();
        break;
      case 'admin-category-show':
        showCategory.run();
        break;
      case 'admin-category-update':
        updateCategory.run();
        break;
      // #endregion
      // #region crud classification
      case 'admin-classification-add':
        addClassification.run();
        break;
      case 'admin-classification-delete':
        deleteClassification.run();
        break;
      case 'admin-classification':
        listClassifications.run();
        break;
      case 'admin-classification-show':
        showClassification.run();
        break;
      case 'admin-classification-update':
        updateClassification.run();
        break;
      // #endregion
      // #region crud favorite
      case 'admin-favorite-add':
        addFavorite.run();
        break;
      case 'admin-favorite-delete':
        deleteFavorite.run();
        break;
      case 'admin-favorite':
        listFavorites.run();
        break;
      case 'admin-favorite-show':
        showFavorite.run();
        break;
      case 'admin-favorite-update':
        updateFavorite.run();
        break;
      // #endregion
      // #region crud marketting
      case 'admin-marketting-add':
        addMarketting.run();
        break;
      case 'admin-marketting-delete':
        deleteMarketting.run();
        break;
      case 'admin-marketting':
        listMarkettings.run();
        break;
      case 'admin-marketting-show':
        showMarketting.run();
        break;
      case 'admin-marketting-update':
        updateMarketting.run();
        break;
      // #endregion
      // #region crud member
      case 'admin-member-add':
        addMember.run();
        break;
      case 'admin-member-delete':
        deleteMember.run();
        break;
      case 'admin-member':
        listMembers.run();
        break;
      case 'admin-member-show':
        showMember.run();
        break;
      case 'admin-member-update':
        updateMember.run();
        break;
      // #endregion
      // #region crud membermovie
      case 'admin-membermovie-add':
        addMemberMovie.run();
        break;
      case 'admin-membermovie-delete':
        deleteMemberMovie.run();
        break;
      case 'admin-membermovie':
        listMemberMovies.run();
        break;
      case 'admin-membermovie-show':
        showMemberMovie.run();
        break;
      case 'admin-membermovie-update':
        updateMemberMovie.run();
        break;
      // #endregion
      // #region crud movie
      case 'admin-movie-add':
        addMovie.run();
        break;
      case 'admin-movie-delete':
        deleteMovie.run();
        break;
      case 'admin-movie':
        listMovies.run();
        break;
      case 'admin-movie-show':
        showMovie.run();
        break;
      case 'admin-movie-update':
        updateMovie.run();
        break;
      // #endregion
      // #region crud movieactor
      case 'admin-movieactor-add':
        addMovieActor.run();
        break;
      case 'admin-movieactor-delete':
        deleteMovieActor.run();
        break;
      case 'admin-movieactor':
        listMovieActors.run();
        break;
      case 'admin-movieactor-show':
        showMovieActor.run();
        break;
      case 'admin-movieactor-update':
        updateMovieActor.run();
        break;
      // #endregion
      // #region crud pricing
      case 'admin-pricing-add':
        addPricing.run();
        break;
      case 'admin-pricing-delete':
        deletePricing.run();
        break;
      case 'admin-pricing':
        listPricings.run();
        break;
      case 'admin-pricing-show':
        showPricing.run();
        break;
      case 'admin-pricing-update':
        updatePricing.run();
        break;
      // #endregion
      case 'admin-database-create':
        axios.get('http://127.0.0.1:8080/api/database/create', {
          host: '127.0.0.1:8080',
          headers: {
            Accept: '*/*',
            'content-type': 'application/json'
          }
        })
          .catch((err) => { throw new Error(err); });
        break;
      case 'admin-database-populate':
        axios.get('http://127.0.0.1:8080/api/database/populate', {
          host: '127.0.0.1:8080',
          headers: {
            Accept: '*/*',
            'content-type': 'application/json'
          }
        })
          .catch((err) => { throw new Error(err); });
        break;
      default:
        error404.run();
    }
  };
};

const routes = new Routes();

routes.run();
