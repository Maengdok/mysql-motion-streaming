const PricingNormalizer = class {
  listNormalizer = (data) => {
    const list = [];
    for (let i = 0; i <= data.length - 1; i += 1) {
      list[i] = {
        id_pricing: data[i].id_pricing,
        title: data[i].title,
        price: data[i].price
      };
    }

    return list;
  };

  showNormalizer = (data) => {
    const pricing = {
      id_pricing: data[0].id_pricing,
      title: data[0].title,
      price: data[0].price
    };

    return pricing;
  };
};

export default PricingNormalizer;
