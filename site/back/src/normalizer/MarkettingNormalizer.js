const MarkettingNormalizer = class {
  listNormalizer = (data) => {
    const list = [];
    for (let i = 0; i <= data.length - 1; i += 1) {
      list[i] = {
        id_marketting: data[i].id_marketting,
        title: data[i].title,
        content: data[i].content,
        link: data[i].link,
        picture: data[i].picture,
        video: data[i].video
      };
    }

    return list;
  };

  showNormalizer = (data) => {
    const marketting = {
      id_marketting: data[0].id_marketting,
      title: data[0].title,
      content: data[0].content,
      link: data[0].link,
      picture: data[0].picture,
      video: data[0].video
    };

    return marketting;
  };
};

export default MarkettingNormalizer;
