const FavoriteNormalizer = class {
  listNormalizer = (data) => {
    const list = [];
    for (let i = 0; i <= data.length - 1; i += 1) {
      list[i] = {
        member: data[i].id_member,
        movie: data[i].id_movie
      };
    }

    return list;
  };

  showNormalizer = (data) => {
    const favorites = [];
    for (let i = 0; i <= data.length - 1; i += 1) {
      favorites[i] = {
        id_member: data[0].id_member,
        id_movie: data[i].id_movie,
        title: data[i].title,
        description: data[i].description,
        image: data[i].image,
        length: data[i].length,
        ranking: data[i].ranking,
        vote: data[i].vote,
        view: data[i].view,
        video: data[i].video,
        category: data[i].category,
        classification: data[i].classification
      };
    }

    return favorites;
  };
};

export default FavoriteNormalizer;
