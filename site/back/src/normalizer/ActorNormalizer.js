const ActorNormalizer = class {
  listNormalizer = (data) => {
    const list = [];
    for (let i = 0; i <= data.length - 1; i += 1) {
      list[i] = {
        id_actor: data[i].id_actor,
        firstname: data[i].firstname,
        lastname: data[i].lastname,
        picture: data[i].picture
      };
    }

    return list;
  };

  showNormalizer = (data) => {
    const actor = {
      id_actor: data[0].id_actor,
      firstname: data[0].firstname,
      lastname: data[0].lastname,
      picture: data[0].picture
    };

    return actor;
  };
};

export default ActorNormalizer;
