const StatisticsNormalizer = class {
  showMoviesNormalizer = (data) => {
    const list = [];
    for (let i = 0; i <= data.length - 1; i += 1) {
      list[i] = {
        idMovie: data[i].id_movie,
        title: data[i].title,
        description: data[i].description,
        imageMax: data[i].image_max,
        imageMin: data[i].image_min,
        imagePortrait: data[i].image_portrait,
        date: data[i].date,
        length: data[i].length,
        ranking: data[i].ranking,
        vote: data[i].vote,
        view: data[i].view,
        video: data[i].video,
        category: data[i].category_title,
        classification: data[i].classification_title
      };
    }

    return list;
  };

  showAdverstisesNormalizer = (data) => {
    const list = [];
    for (let i = 0; i <= data.length - 1; i += 1) {
      list[i] = {
        idMarketting: data[i].id_marketting,
        title: data[i].title,
        content: data[i].content,
        link: data[i].link,
        picture: data[i].picture,
        video: data[i].video
      };
    }

    return list;
  };

  // TODO : REWORK MOVIE TO ADD THE NUMBER OF TIME A MEMBER HAS SEEN THE MOVIE
  // showWatchedMoviesNormalizer = (data) => {

  // };

  showCountFavoritesNormalizer = (data) => {
    const { count } = data[0].count;

    return count;
  };
};

export default StatisticsNormalizer;
