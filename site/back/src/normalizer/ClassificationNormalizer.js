const ClassificationNormalizer = class {
  listNormalizer = (data) => {
    const list = [];
    for (let i = 0; i <= data.length - 1; i += 1) {
      list[i] = {
        id_classification: data[i].id_classification,
        title: data[i].title
      };
    }

    return list;
  };

  showNormalizer = (data) => {
    const classification = {
      id_classification: data[0].id_classification,
      title: data[0].title
    };

    return classification;
  };
};

export default ClassificationNormalizer;
