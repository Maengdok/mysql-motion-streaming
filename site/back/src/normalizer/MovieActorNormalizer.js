const MovieActorNormalizer = class {
  listNormalizer = (data) => {
    const list = [];
    for (let i = 0; i <= data.length - 1; i += 1) {
      list[i] = {
        movie: data[i].id_movie,
        actor: data[i].id_actor
      };
    }

    return list;
  };

  showActorsFromMovieNormalizer = (data) => {
    const actors = [];
    for (let i = 0; i <= data.length - 1; i += 1) {
      actors[i] = {
        id_movie: data[i].id_movie,
        id_actor: data[i].id_actor,
        firstname: data[i].firstname,
        lastname: data[i].lastname,
        picture: data[i].picture
      };
    }

    return actors;
  };

  showMoviesFromActorNormalizer = (data) => {
    const movies = [];
    for (let i = 0; i <= data.length - 1; i += 1) {
      movies[i] = {
        id_actor: data[0].id_actor,
        id_movie: data[i].id_movie,
        title: data[i].title,
        description: data[i].description,
        image: data[i].image,
        length: data[i].length,
        ranking: data[i].ranking,
        vote: data[i].vote,
        view: data[i].view,
        video: data[i].video,
        category: data[i].category,
        classification: data[i].classification
      };
    }

    return movies;
  };
};

export default MovieActorNormalizer;
