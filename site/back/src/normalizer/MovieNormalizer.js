const MovieNormalizer = class {
  listNormalizer = (data) => {
    const list = [];
    for (let i = 0; i <= data.length - 1; i += 1) {
      list[i] = {
        idMovie: data[i].id_movie,
        title: data[i].title,
        description: data[i].description,
        imageMax: data[i].image_max,
        imageMin: data[i].image_min,
        imagePortrait: data[i].image_portrait,
        date: data[i].date,
        length: data[i].length,
        ranking: data[i].ranking,
        vote: data[i].vote,
        view: data[i].view,
        video: data[i].video,
        createdAt: data[i].createdAt,
        updatedAt: data[i].updatedAt,
        idCategory: data[i].id_category,
        category: data[i].category_title,
        idClassification: data[i].id_classification,
        classification: data[i].classification_title
      };
    }

    return list;
  };

  showNormalizer = (data) => {
    const movie = {
      idMovie: data[0].id_movie,
      title: data[0].title,
      description: data[0].description,
      imageMax: data[0].image_max,
      imageMin: data[0].image_min,
      imagePortrait: data[0].image_portrait,
      date: data[0].date,
      length: data[0].length,
      ranking: data[0].ranking,
      vote: data[0].vote,
      view: data[0].view,
      video: data[0].video,
      createdAt: data[0].createdAt,
      updatedAt: data[0].updatedAt,
      idCategory: data[0].id_category,
      category: data[0].category_title,
      idClassification: data[0].id_classification,
      classification: data[0].classification_title
    };

    return movie;
  };

  viewsNormalizer = (data) => {
    const views = {
      idMovie: data[0].id_movie,
      view: data[0].view
    };

    return views;
  };

  rankingNormalizer = (data) => {
    const rank = {
      idMovie: data[0].id_movie,
      ranking: data[0].ranking,
      vote: data[0].vote
    };

    return rank;
  };

  searchNormalizer = (data) => {
    const list = [];
    for (let i = 0; i <= data.length - 1; i += 1) {
      list[i] = {
        id: data[i].id_movie,
        title: data[i].title,
        image: data[i].image_min,
        date: data[i].date,
        length: data[i].length,
        idCategory: data[i].id_category,
        category: data[i].category_title
      };
    }

    return list;
  };
};

export default MovieNormalizer;
