const CategoryNormalizer = class {
  listNormalizer = (data) => {
    const list = [];
    for (let i = 0; i <= data.length - 1; i += 1) {
      list[i] = {
        idCategory: data[i].id_category,
        title: data[i].title,
        picture: data[i].picture
      };
    }

    return list;
  };

  showNormalizer = (data) => {
    const category = {
      idCategory: data[0].id_category,
      title: data[0].title,
      picture: data[0].picture
    };

    return category;
  };
};

export default CategoryNormalizer;
