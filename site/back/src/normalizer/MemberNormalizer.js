const MemberNormalizer = class {
  listNormalizer = (data) => {
    const list = [];
    for (let i = 0; i <= data.length - 1; i += 1) {
      list[i] = {
        id_member: data[i].id_member,
        email: data[i].email,
        firstname: data[i].firstname,
        lastname: data[i].lastname,
        address: data[i].address,
        zipcode: data[i].zipcode,
        city: data[i].city,
        picture: data[i].picture,
        createdAt: data[i].createdAt,
        updatedAt: data[i].updatedAt,
        pricing: data[i].pricing
      };
    }

    return list;
  };

  showNormalizer = (data) => {
    const member = {
      id_member: data[0].id_member,
      email: data[0].email,
      firstname: data[0].firstname,
      lastname: data[0].lastname,
      address: data[0].address,
      zipcode: data[0].zipcode,
      city: data[0].city,
      picture: data[0].picture,
      createdAt: data[0].createdAt,
      updatedAt: data[0].updatedAt,
      pricing: data[0].pricing
    };

    return member;
  };
};

export default MemberNormalizer;
