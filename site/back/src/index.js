import * as mysql from 'mysql';
import dotenv from 'dotenv';
import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import DatabaseQueries from './database/DatabaseQueries.js';
import DatabasePopulate from './database/DatabasePopulate.js';
import ActorController from './controller/ActorController.js';
import ClassificationController from './controller/ClassificationController.js';
import CategoryController from './controller/CategoryController.js';
import MarkettingController from './controller/MarkettingController.js';
import MemberController from './controller/MemberController.js';
import MovieController from './controller/MovieController.js';
import PricingController from './controller/PricingController.js';
import MovieActorController from './controller/MovieActorController.js';
import FavoriteController from './controller/FavoriteController.js';
import StatisticsController from './controller/StatisticsController.js';

// TODO : ROUTES DatabaseQueries, DatabasePopulate / LogIn + SignIn Controller
dotenv.config();
const db = mysql.createConnection({
  host: process.env.HOST,
  user: process.env.USR,
  password: process.env.PSSWRD
});
const app = express();
const jsonParser = bodyParser.json();
const databaseQueries = new DatabaseQueries(db);
const databasePopulate = new DatabasePopulate(db);
const usedDb = databaseQueries.dbUse();
const actorController = new ActorController(db, usedDb);
const classificationController = new ClassificationController(db, usedDb);
const categoryController = new CategoryController(db, usedDb);
const markettingController = new MarkettingController(db, usedDb);
const memberController = new MemberController(db, usedDb);
const movieController = new MovieController(db, usedDb);
const pricingController = new PricingController(db, usedDb);
const movieActorController = new MovieActorController(db, usedDb);
const favoriteController = new FavoriteController(db, usedDb);
const statisticsController = new StatisticsController(db, usedDb);

app.use(cors());

app.get('/api/database/create', (_req, res) => {
  databaseQueries.run(() => res.status(200).json());
});

app.get('/api/database/populate', (_req, res) => {
  databasePopulate.run(() => res.status(200).json());
});

// #region /api/actor/
app.get('/api/actor/list', (_req, res) => {
  console.log(res);
  actorController.list((result) => res.status(200).json(result));
});

app.get('/api/actor/show/:id', (req, res) => {
  const id = parseInt(req.params.id, 10);
  actorController.show(id, (result) => res.status(200).json(result));
});

app.post('/api/actor/add', jsonParser, (req, res) => {
  const actor = req.body;
  actorController.add(
    actor.firstname,
    actor.lastname,
    actor.picture,
    (result) => res.status(200).json(result)
  );
});

app.patch('/api/actor/update', jsonParser, (req, res) => {
  const actor = req.body;
  actorController.update(
    actor.id,
    actor.firstname,
    actor.lastname,
    actor.picture,
    (result) => res.status(200).json(result)
  );
});

app.delete('/api/actor/delete/:id', (req, res) => {
  const id = parseInt(req.params.id, 10);
  actorController.delete(id, (result) => res.status(200).json(result));
});
// #endregion

// #region /api/classification/
app.get('/api/classification/list', (_req, res) => {
  classificationController.list((result) => res.status(200).json(result));
});

app.get('/api/classification/show/:id', (req, res) => {
  const id = parseInt(req.params.id, 10);
  classificationController.show(id, (result) => res.status(200).json(result));
});

app.post('/api/classification/add', jsonParser, (req, res) => {
  const classification = req.body;
  classificationController.add(
    classification.title,
    (result) => res.status(200).json(result)
  );
});

app.patch('/api/classification/update', jsonParser, (req, res) => {
  const classification = req.body;
  classificationController.update(
    classification.id,
    classification.title,
    (result) => res.status(200).json(result)
  );
});

app.delete('/api/classification/delete/:id', (req, res) => {
  const id = parseInt(req.params.id, 10);
  classificationController.delete(id, (result) => res.status(200).json(result));
});
// #endregion

// #region /api/genre/
app.get('/api/category/list', (_req, res) => {
  categoryController.list((result) => res.status(200).json(result));
});

app.get('/api/category/show/:id', (req, res) => {
  const id = parseInt(req.params.id, 10);
  categoryController.show(id, (result) => res.status(200).json(result));
});

app.post('/api/category/add', jsonParser, (req, res) => {
  const category = req.body;
  categoryController.add(
    category.title,
    category.picture,
    (result) => res.status(200).json(result)
  );
});

app.patch('/api/category/update', jsonParser, (req, res) => {
  const category = req.body;
  categoryController.update(
    category.id,
    category.title,
    category.picture,
    (result) => res.status(200).json(result)
  );
});

app.delete('/api/category/delete/:id', (req, res) => {
  const id = parseInt(req.params.id, 10);
  categoryController.delete(id, (result) => res.status(200).json(result));
});
// #endregion

// #region /api/marketting/
app.get('/api/marketting/list', (_req, res) => {
  markettingController.list((result) => res.status(200).json(result));
});

app.get('/api/marketting/show/:id', (req, res) => {
  const id = parseInt(req.params.id, 10);
  markettingController.show(id, (result) => res.status(200).json(result));
});

app.post('/api/marketting/add', jsonParser, (req, res) => {
  const marketting = req.body;
  markettingController.add(
    marketting.title,
    marketting.content,
    marketting.link,
    marketting.picture,
    marketting.video,
    (result) => res.status(200).json(result)
  );
});

app.patch('/api/marketting/update', jsonParser, (req, res) => {
  const marketting = req.body;
  markettingController.update(
    marketting.id,
    marketting.title,
    marketting.content,
    marketting.link,
    marketting.picture,
    marketting.video,
    (result) => res.status(200).json(result)
  );
});

app.delete('/api/marketting/delete/:id', (req, res) => {
  const id = parseInt(req.params.id, 10);
  markettingController.delete(id, (result) => res.status(200).json(result));
});
// #endregion

// #region /api/member/
app.get('/api/member/list', (_req, res) => {
  memberController.list((result) => res.status(200).json(result));
});

app.get('/api/member/show/:id', (req, res) => {
  const id = parseInt(req.params.id, 10);
  memberController.show(id, (result) => res.status(200).json(result));
});

app.post('/api/member/add', jsonParser, (req, res) => {
  const member = req.body;
  memberController.add(
    member.email,
    member.password,
    member.firstname,
    member.lastname,
    member.address,
    member.zipcode,
    member.city,
    member.picture,
    member.pricing,
    (result) => res.status(200).json(result)
  );
});

app.patch('/api/member/update', jsonParser, (req, res) => {
  const member = req.body;
  memberController.update(
    member.id,
    member.email,
    member.password,
    member.firstname,
    member.lastname,
    member.address,
    member.zipcode,
    member.city,
    member.picture,
    member.pricing,
    (result) => res.status(200).json(result)
  );
});

app.delete('/api/member/delete/:id', (req, res) => {
  const id = parseInt(req.params.id, 10);
  memberController.delete(id, (result) => res.status(200).json(result));
});
// #endregion

// #region /api/movie/
app.get('/api/movie/list', (_req, res) => {
  movieController.list((result) => res.status(200).json(result));
});

app.get('/api/movie/show/:id', (req, res) => {
  const id = parseInt(req.params.id, 10);
  movieController.show(id, (result) => res.status(200).json(result));
});

app.get('/api/movie/category/:id', (req, res) => {
  const id = parseInt(req.params.id, 10);
  movieController.category(id, (result) => res.status(200).json(result));
});

app.post('/api/movie/add', jsonParser, (req, res) => {
  const movie = req.body;
  movieController.add(
    movie.title,
    movie.description,
    movie.imageMax,
    movie.imageMin,
    movie.imagePortrait,
    movie.date,
    movie.length,
    movie.video,
    movie.category,
    movie.classification,
    (result) => res.status(200).json(result)
  );
});

app.patch('/api/movie/update', jsonParser, (req, res) => {
  const movie = req.body;
  movieController.update(
    movie.id,
    movie.title,
    movie.description,
    movie.imageMax,
    movie.imageMin,
    movie.imagePortrait,
    movie.date,
    movie.length,
    movie.video,
    movie.category,
    movie.classification,
    (result) => res.status(200).json(result)
  );
});

app.delete('/api/movie/delete/:id', (req, res) => {
  const id = parseInt(req.params.id, 10);
  movieController.delete(id, (result) => res.status(200).json(result));
});

app.get('/api/movie/views/:id', (req, res) => {
  const id = parseInt(req.params.id, 10);
  movieController.showViews(id, (result) => res.status(200).json(result));
});

app.patch('/api/movie/updateviews/:id', (req, res) => {
  const id = parseInt(req.params.id, 10);
  movieController.updateViews(id, (result) => res.status(200).json(result));
});

app.get('/api/movie/ranking/:id', (req, res) => {
  const id = parseInt(req.params.id, 10);
  movieController.showRanking(id, (result) => res.status(200).json(result));
});

app.patch('/api/movie/updateranking', jsonParser, (req, res) => {
  const rank = req.body;
  movieController.updateRanking(rank.id, rank.ranking, (result) => res.status(200).json(result));
});

app.get('/api/movie/foryou/:id', (req, res) => {
  const id = parseInt(req.params.id, 10);
  movieController.show6MoviesFromCategoryAndRanking(id, (result) => res.status(200).json(result));
});

app.get('/api/movie/search/:title', (req, res) => {
  const { title } = req.params;
  movieController.search(title, (result) => res.status(200).json(result));
});
// #endregion

// #region /api/pricing/
app.get('/api/pricing/list', (_req, res) => {
  pricingController.list((result) => res.status(200).json(result));
});

app.get('/api/pricing/show/:id', (req, res) => {
  const id = parseInt(req.params.id, 10);
  pricingController.show(id, (result) => res.status(200).json(result));
});

app.post('/api/pricing/add', jsonParser, (req, res) => {
  const pricing = req.body;
  pricingController.add(
    pricing.title,
    pricing.price,
    (result) => res.status(200).json(result)
  );
});

app.patch('/api/pricing/update', jsonParser, (req, res) => {
  const pricing = req.body;
  pricingController.update(
    pricing.id,
    pricing.title,
    pricing.price,
    (result) => res.status(200).json(result)
  );
});

app.delete('/api/pricing/delete/:id', (req, res) => {
  const id = parseInt(req.params.id, 10);
  pricingController.delete(id, (result) => res.status(200).json(result));
});
// #endregion

// #region /api/movieactor/
app.get('/api/movieactor/list', (_req, res) => {
  movieActorController.list((result) => res.status(200).json(result));
});

app.get('/api/movieactor/showactors/:id', (req, res) => {
  const id = parseInt(req.params.id, 10);
  movieActorController.showActorsFromMovie(id, (result) => res.status(200).json(result));
});

app.get('/api/movieactor/showmovies/:id', jsonParser, (req, res) => {
  const id = parseInt(req.params.id, 10);
  movieActorController.showMoviesFromActor(id, (result) => res.status(200).json(result));
});

app.post('/api/movieactor/add', jsonParser, (req, res) => {
  const movieActor = req.body;
  movieActorController.add(
    movieActor.movie,
    movieActor.actor,
    (result) => res.status(200).json(result)
  );
});

app.delete('/api/movieactor/delete', jsonParser, (req, res) => {
  const movieActor = req.body;
  movieActorController.delete(
    movieActor.movie,
    movieActor.actor,
    (result) => res.status(200).json(result)
  );
});
// #endregion

// #region /api/favorite/
app.get('/api/favorite/list', (_req, res) => {
  favoriteController.list((result) => res.status(200).json(result));
});

app.get('/api/favorite/show/:id', (req, res) => {
  const id = parseInt(req.params.id, 10);
  favoriteController.show(id, (result) => res.status(200).json(result));
});

app.post('/api/favorite/add', jsonParser, (req, res) => {
  const favorite = req.body;
  favoriteController.add(favorite.member, favorite.movie, (result) => res.status(200).json(result));
});

app.delete('/api/favorite/delete', jsonParser, (req, res) => {
  const favorite = req.body;
  favoriteController.delete(
    favorite.member,
    favorite.movie,
    (result) => res.status(200).json(result)
  );
});
// #endregion

// #region /api/statistics/
app.get('/api/statistics/showmostwatched', (_req, res) => {
  statisticsController.show5MostWatchedMovies((result) => res.status(200).json(result));
});

app.get('/api/statistics/showlatest', (_req, res) => {
  statisticsController.show10LatestMovies((result) => res.status(200).json(result));
});

app.get('/api/statistics/showads', (_req, res) => {
  statisticsController.show3RandomAdvertises((result) => res.status(200).json(result));
});

app.get('/api/statistics/showcount/:id', jsonParser, (req, res) => {
  const id = parseInt(req.params.id, 10);
  statisticsController.showCountFavorites(id, (result) => res.status(200).json(result));
});
// #endregion
app.listen(8080, () => {});
