const ClassificationQueries = class {
  constructor(db) {
    this.db = db;
  }

  // #region CRUD CLASSIFICATION
  dbQuerySelectClassificatons = (callback) => {
    this.db.query('SELECT * FROM classification ORDER BY title ASC', (err, result) => ((result) ? callback(result) : callback(err)));
  };

  dbQuerySelectClassification = (id, callback) => {
    this.db.query(`SELECT * FROM classification WHERE id_classification = ${id}`, (err, result) => ((result) ? callback(result) : callback(err)));
  };

  dbQueryAddClassification = (title, callback) => {
    this.db.query(`INSERT INTO classification (title) VALUES ("${title}")`, (err, result) => ((result) ? callback('Classification added successfully !') : callback(err)));
  };

  dbQueryUpdateClassification = (id, title, callback) => {
    this.db.query(`UPDATE classification SET title = "${title}" WHERE id_classification = ${id}`, (err, result) => ((result) ? callback('Classification updated successfully !') : callback(err)));
  };

  dbQueryDeleteClassification = (id, callback) => {
    this.db.query(`DELETE FROM classification WHERE id_classification = ${id}`, (err, result) => ((result) ? callback('Classification deleted successfully !') : callback(err)));
  };
  // #endregion
};

export default ClassificationQueries;
