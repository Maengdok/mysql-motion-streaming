const MemberQueries = class {
  constructor(db) {
    this.db = db;
  }

  // #region CRUD MEMBER
  dbQuerySelectMembers = (callback) => {
    this.db.query('SELECT email, firstname, lastname, id_pricing FROM member ORDER BY firstname, lastname ASC', (err, result) => ((result) ? callback(result) : callback(err)));
  };

  dbQuerySelectMember = (id, callback) => {
    this.db.query(`SELECT
      email,
      firstname,
      lastname,
      address,
      zipcode,
      city,
      picture,
      created_at,
      id_pricing
      FROM member WHERE id_member = ${id}`, (err, result) => ((result) ? callback(result) : callback(err)));
  };

  dbQueryAddMember = (
    email,
    password,
    firstname,
    lastname,
    address,
    zipcode,
    city,
    picture,
    pricing,
    callback
  ) => {
    const createdAt = new Date();
    const formatCreatedAt = `${createdAt.getFullYear()}-${(`0${(createdAt.getMonth() + 1)}`).slice(-2)}-${(`0${createdAt.getDate()}`).slice(-2)} ${createdAt.getHours()}:${createdAt.getMinutes()}:${createdAt.getSeconds()}`;
    this.db.query(`INSERT INTO member (
      email,
      password,
      firstname,
      lastname,
      address,
      zipcode,
      city,
      picture,
      created_at,
      id_pricing
      ) VALUES (
      "${email}",
      "${password}",
      "${firstname}",
      "${lastname}",
      "${address}",
      "${zipcode}",
      "${city}",
      "${picture}",
      "${formatCreatedAt}",
      "${pricing}"
      )`, (err, result) => ((result) ? callback('Member added successfully !') : callback(err)));
  };

  dbQueryUpdateMember = (
    id,
    email,
    password,
    firstname,
    lastname,
    address,
    zipcode,
    city,
    picture,
    pricing,
    callback
  ) => {
    const updatedAt = new Date();
    const formatUpdatedAt = `${updatedAt.getFullYear()}-${(`0${(updatedAt.getMonth() + 1)}`).slice(-2)}-${(`0${updatedAt.getDate()}`).slice(-2)} ${updatedAt.getHours()}:${updatedAt.getMinutes()}:${updatedAt.getSeconds()}`;
    this.db.query(`UPDATE member SET
      email = "${email}",
      password = "${password}",
      firstname = "${firstname}",
      lastname = "${lastname}",
      address = "${address}",
      zipcode = "${zipcode}",
      city = "${city}",
      picture = "${picture}",
      updated_at = "${formatUpdatedAt}",
      id_pricing = "${pricing}"
      WHERE id_member = ${id}`, (err, result) => ((result) ? callback('Member updated successfully !') : callback(err)));
  };

  dbQueryDeleteMember = (id, callback) => {
    this.db.query(`DELETE FROM member WHERE id_member = ${id}`, (err, result) => ((result) ? callback('Member deleted successfully !') : callback(err)));
  };
  // #endregion
};

export default MemberQueries;
