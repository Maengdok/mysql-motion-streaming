const FavoriteQueries = class {
  constructor(db) {
    this.db = db;
  }

  // #region CRUD FAVORITE
  dbQuerySelectFavorites = (callback) => {
    this.db.query('SELECT * FROM favorite ORDER BY id_member, id_movie ASC', (err, result) => ((result) ? callback(result) : callback(err)));
  };

  dbQuerySelectFavorite = (member, callback) => {
    this.db.query(`SELECT * FROM favorite LEFT JOIN (SELECT * FROM movie) movie USING (id_movie) WHERE id_member = ${member}`, (err, result) => ((result) ? callback(result) : callback(err)));
  };

  dbQueryAddFavorite = (member, movie, callback) => {
    this.db.query(`INSERT INTO favorite (id_member, id_movie) VALUES (${member}, ${movie})`, (err, result) => ((result) ? callback('Favorite Movie added successfully !') : callback(err)));
  };

  dbQueryDeleteFavorite = (member, movie, callback) => {
    this.db.query(`DELETE FROM favorite WHERE id_member = ${member} AND id_movie = ${movie}`, (err, result) => ((result) ? callback('Movie deleted from Favorite successfully !') : callback(err)));
  };
  // #endregion
};

export default FavoriteQueries;
