const ActorQueries = class {
  constructor(db, usedDb) {
    this.db = db;
    this.usedDb = usedDb;
  }

  // #region CRUD ACTOR
  dbQuerySelectActors = async (callback) => {
    this.db.query('SELECT * FROM actor ORDER BY firstname, lastname ASC', (err, result) => ((result) ? callback(result) : callback(err)));
  };

  dbQuerySelectActor = (id, callback) => {
    this.db.query(`SELECT * FROM actor WHERE id_actor = ${id}`, (err, result) => ((result) ? callback(result) : callback(err)));
  };

  dbQueryAddActor = (firstname, lastname, picture, callback) => {
    this.db.query(`INSERT INTO actor (firstname, lastname, picture) VALUES ("${firstname}", "${lastname}", "${picture}")`, (err, result) => ((result) ? callback('Actor added successfully !') : callback(err)));
  };

  dbQueryUpdateActor = (id, firstname, lastname, picture, callback) => {
    this.db.query(`UPDATE actor SET firstname = "${firstname}", lastname = "${lastname}", picture = "${picture}" WHERE id_actor = ${id}`, (err, result) => ((result) ? callback('Actor updated successfully !') : callback(err)));
  };

  dbQueryDeleteActor = (id, callback) => {
    this.db.query(`DELETE FROM actor WHERE id_actor = ${id}`, (err, result) => ((result) ? callback('Actor deleted successfully !') : callback(err)));
  };
  // #endregion
};

export default ActorQueries;
