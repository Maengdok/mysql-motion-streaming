const StatisticsQueries = class {
  constructor(db) {
    this.db = db;
  }

  dbQuery5MostWatchedMovies = (callback) => {
    this.db.query('SELECT * FROM movie LEFT JOIN (SELECT id_category, title AS category_title FROM category) T2 ON movie.id_category = T2.id_category LEFT JOIN (SELECT id_classification, title AS classification_title FROM classification) T3 ON movie.id_classification = T3.id_classification ORDER BY movie.view DESC LIMIT 5', (err, result) => ((result) ? callback(result) : callback(err)));
  };

  dbQuery10LatestMovies = (callback) => {
    this.db.query('SELECT * FROM movie LEFT JOIN (SELECT id_category, title AS category_title FROM category) T2 ON movie.id_category = T2.id_category LEFT JOIN (SELECT id_classification, title AS classification_title FROM classification) T3 ON movie.id_classification = T3.id_classification ORDER BY created_at DESC LIMIT 10', (err, result) => ((result) ? callback(result) : callback(err)));
  };

  dbQuery3RandomAdvertises = (callback) => {
    this.db.query('SELECT * FROM marketting ORDER BY RAND() LIMIT 3', (err, result) => ((result) ? callback(result) : callback(err)));
  };

  // TODO : REWORK MOVIE TO ADD THE NUMBER OF TIME A MEMBER HAS SEEN THE MOVIE
  // dbQueryCountWatchedMovie = (member, movie, callback) => {
  // eslint-disable-next-line max-len
  // this.db.query(`SELECT COUNT(*) FROM member_movie WHERE id_member = ${member} AND id_movie = ${movie}`, (err, result) => ((result) ? callback(result) : callback(err)));
  // };

  dbQueryCountmemberFavorites = (member, callback) => {
    this.db.query(`SELECT COUNT(*) AS count FROM favorite WHERE id_member = ${member}`, (err, result) => ((result) ? callback(result) : callback(err)));
  };
};

export default StatisticsQueries;
