const DatabaseQueries = class {
  constructor(db) {
    this.db = db;
  }

  dbConnect = () => {
    this.db.connect((err) => (err));
  };

  dbCreate = () => {
    this.db.query('CREATE DATABASE IF NOT EXISTS motion_streaming', (err, result) => ((result) ?? err));
  };

  dbUse = () => {
    this.db.query('USE motion_streaming', (err, result) => ((result) ?? err));
  };

  dbCreateMovieTable = () => {
    this.db.query('CREATE TABLE IF NOT EXISTS movie (id_movie INT NOT NULL PRIMARY KEY AUTO_INCREMENT,title VARCHAR(255) NOT NULL,description VARCHAR(255) NOT NULL,image_max TEXT,image_min TEXT,image_portrait TEXT,date DATE NOT NULL,length INT(5),ranking INT(2),vote INT,view INT,video TEXT NOT NULL,created_at DATETIME NOT NULL,updated_at DATETIME,id_category INT,id_classification INT)ENGINE=InnoDB CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci', (err, result) => ((result) ?? err));
  };

  dbCreateMarkettingTable = () => {
    this.db.query('CREATE TABLE IF NOT EXISTS marketting (id_marketting INT NOT NULL PRIMARY KEY AUTO_INCREMENT,title VARCHAR(255) NOT NULL,content VARCHAR(255) NOT NULL,link TEXT NOT NULL,picture TEXT,video TEXT)ENGINE=InnoDB CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci', (err, result) => ((result) ?? err));
  };

  dbCreatePricingTable = () => {
    this.db.query('CREATE TABLE IF NOT EXISTS pricing (id_pricing INT NOT NULL PRIMARY KEY AUTO_INCREMENT,title VARCHAR(255) NOT NULL,price INT(3) NOT NULL)ENGINE=InnoDB CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci', (err, result) => ((result) ?? err));
  };

  dbCreateMemberTable = () => {
    this.db.query('CREATE TABLE IF NOT EXISTS member (id_member INT NOT NULL PRIMARY KEY AUTO_INCREMENT,email VARCHAR(255) NOT NULL,password VARCHAR(255) NOT NULL,firstname VARCHAR(255) NOT NULL,lastname VARCHAR(255) NOT NULL,address VARCHAR(255) NOT NULL,zipcode INT(5) NOT NULL,city VARCHAR(255) NOT NULL,picture TEXT,created_at DATETIME NOT NULL,updated_at DATETIME,id_pricing INT)ENGINE=InnoDB CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci', (err, result) => ((result) ?? err));
  };

  dbCreateCategoryTable = () => {
    this.db.query('CREATE TABLE IF NOT EXISTS category (id_category INT NOT NULL PRIMARY KEY AUTO_INCREMENT,title VARCHAR(255) NOT NULL, picture TEXT)ENGINE=InnoDB CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci', (err, result) => ((result) ?? err));
  };

  dbCreateActorTable = () => {
    this.db.query('CREATE TABLE IF NOT EXISTS actor (id_actor INT NOT NULL PRIMARY KEY AUTO_INCREMENT,firstname VARCHAR(255) NOT NULL,lastname VARCHAR(255) NOT NULL,picture TEXT)ENGINE=InnoDB CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci', (err, result) => ((result) ?? err));
  };

  dbCreateClassificationTable = () => {
    this.db.query('CREATE TABLE IF NOT EXISTS classification (id_classification INT NOT NULL PRIMARY KEY AUTO_INCREMENT,title VARCHAR(30) NOT NULL)ENGINE=InnoDB CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci', (err, result) => ((result) ?? err));
  };

  dbCreateMemberFavoriteTable = () => {
    this.db.query('CREATE TABLE IF NOT EXISTS favorite (id_member INT NOT NULL,id_movie INT NOT NULL,FOREIGN KEY (id_member) REFERENCES member (id_member),FOREIGN KEY (id_movie) REFERENCES movie (id_movie))ENGINE=InnoDB CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci', (err, result) => ((result) ?? err));
  };

  dbCreateMovieActorTable = () => {
    this.db.query('CREATE TABLE IF NOT EXISTS movie_actor (id_movie INT NOT NULL,id_actor INT NOT NULL,FOREIGN KEY (id_movie) REFERENCES movie (id_movie),FOREIGN KEY (id_actor) REFERENCES actor (id_actor))ENGINE=InnoDB CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci', (err, result) => ((result) ?? err));
  };

  dbCreateMemberMovieTable = () => {
    this.db.query('CREATE TABLE IF NOT EXISTS member_movie (id_member INT NOT NULL,id_movie INT NOT NULL,FOREIGN KEY (id_member) REFERENCES member (id_member),FOREIGN KEY (id_movie) REFERENCES movie (id_movie))ENGINE=InnoDB CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci', (err, result) => ((result) ?? err));
  };

  dbConstraintMovieFK = () => {
    this.db.query('ALTER TABLE movieADD FOREIGN KEY (id_category) REFERENCES category (id_category),ADD FOREIGN KEY (id_classification) REFERENCES classification (id_classification)', (err, result) => ((result) ?? err));
  };

  dbConstraintMemberFK = () => {
    this.db.query('ALTER TABLE memberADD FOREIGN KEY (id_pricing) REFERENCES pricing (id_pricing)', (err, result) => ((result) ?? err));
  };

  run = async () => {
    this.dbConnect();
    this.dbCreate();
    this.dbUse();
    this.dbCreateMovieTable();
    this.dbCreateMemberTable();
    this.dbCreateMarkettingTable();
    this.dbCreatePricingTable();
    this.dbCreateCategoryTable();
    this.dbCreateActorTable();
    this.dbCreateClassificationTable();
    this.dbCreateMemberFavoriteTable();
    this.dbCreateMovieActorTable();
    this.dbCreateMemberMovieTable();
    this.dbConstraintMovieFK();
    this.dbConstraintMemberFK();
  };
};

export default DatabaseQueries;
