const MarkettingQueries = class {
  constructor(db) {
    this.db = db;
  }

  // #region CRUD MARKETTING
  dbQuerySelectMarkettings = (callback) => {
    this.db.query('SELECT * FROM marketting ORDER BY title ASC', (err, result) => ((result) ? callback(result) : callback(err)));
  };

  dbQuerySelectMarketting = (markettingId, callback) => {
    this.db.query(`SELECT * FROM marketting WHERE id_marketting = ${markettingId}`, (err, result) => ((result) ? callback(result) : callback(err)));
  };

  dbQueryAddMarketting = (title, content, link, picture, video, callback) => {
    this.db.query(`INSERT INTO marketting (
      title, 
      content, 
      link, 
      picture, 
      video
      ) VALUES (
        "${title}", 
        "${content}", 
        "${link}", 
        "${picture}", 
        "${video}"
      )`, (err, result) => ((result) ? callback('Marketting added successfully !') : callback(err)));
  };

  dbQueryUpdateMarketting = (id, title, content, link, picture, video, callback) => {
    this.db.query(`UPDATE marketting SET
      title = "${title}",
      content = "${content}",
      link = "${link}",
      picture = "${picture}",
      video = "${video}"
      WHERE id_marketting = ${id}`, (err, result) => ((result) ? callback('Marketting updated successfully !') : callback(err)));
  };

  dbQueryDeleteMarketting = (id, callback) => {
    this.db.query(`DELETE FROM marketting WHERE id_marketting = ${id}`, (err, result) => ((result) ? callback('Marketting deleted successfully !') : callback(err)));
  };
  // #endregion
};

export default MarkettingQueries;
