const names = [
  'Jordin Villa',
  'Justice Galloway',
  'Dalton Drake',
  'Abigayle Stark',
  'Anna Wells',
  'Ayden Noble',
  'Sean Barker',
  'Yosef Whitaker',
  'Rory Lewis',
  'Marlie Garza',
  'Johnathon Anthony',
  'Wesley Savage',
  'Santiago Gibbs',
  'Matthew Rios',
  'Cora Pitts',
  'Lucille Barrett',
  'Brendon Oconnor',
  'Daniel Brennan',
  'Maia Randall',
  'Walker Knight',
  'Samson Hoover',
  'Sanaa Paul',
  'Kaleb Garrett',
  'Ariella Church',
  'Karli Mosley',
  'Nikhil Dennis',
  'Katherine Spencer',
  'Eli Bradford',
  'Ezequiel Carson',
  'Taylor Harrison',
  'Jovany Barrett',
  'Spencer Hodge',
  'Franco Montes',
  'Andy Gillespie',
  'Gavyn Compton',
  'Averi Walsh',
  'Ruben Chavez',
  'Summer Lambert',
  'Rodney Berry',
  'Jett Perkins',
  'Alma Huynh',
  'Franklin Riley',
  'Azaria Proctor',
  'Michelle Blankenship',
  'Aydan Mullins',
  'Nolan Miller',
  'Dayanara Porter',
  'Cherish Chambers',
  'Hugo Snow',
  'Cyrus Spencer'
];

const DatabasePopulate = class {
  constructor(db) {
    this.db = db;
  }

  dbInsertIntoCategory = () => {
    this.db.query('INSERT INTO category (id_category, title, picture) VALUES (1, "Horreur", "https://i.pinimg.com/564x/0d/8f/aa/0d8faa7dfecb648f923bfbc063fad41f.jpg"), (2, "Aventure", "https://i.pinimg.com/564x/4d/33/f7/4d33f7073d71ab1db419443fe696f58a.jpg"), (3, "Action", "https://i.pinimg.com/564x/69/7f/f5/697ff5d53117acd1dcaa71ed54fcf258.jpg"), (4, "Fantastique", "https://i.pinimg.com/564x/91/bc/5e/91bc5e75ffcbf2d728a251938b0511d2.jpg"), (5, "Science-Fiction", "https://i.pinimg.com/564x/3c/bb/28/3cbb2894fca7aa9d4d321e8fdaacbf62.jpg"), (6, "Drame", "https://i.pinimg.com/564x/07/f1/b7/07f1b78effab2d7f61cd467cfbdc2bdc.jpg"), (7, "Comédie", "https://i.pinimg.com/564x/7a/69/12/7a6912285dfbd75cf4d4f8670f07d6cb.jpg"), (8, "Anime", "https://i.pinimg.com/564x/2b/ec/c5/2becc548754699c2c5a6bc2c63c03d21.jpg")', (err, result) => ((result) ?? err));
  };

  dbInsertIntoClassification = () => {
    this.db.query('INSERT INTO classification (id_classification, title) VALUES (1, "Tous publics"), (2, "Public averti"), (3, "Interdit aux moins de 12 ans"), (4, "Interdit aux moins de 16 ans"), (5, "Interdit aux moins de 18 ans"), (6, "Accord parental")', (err, result) => ((result) ?? err));
  };

  dbInsertIntoPricing = () => {
    this.db.query('INSERT INTO pricing (id_pricing, title, price ) VALUES (1, "Téma la taille du rat !", 0), (2, "J\'ai du mal à finir le mois", 10), (3, "J\'suis dans mon jacuzzi", 100)', (err, result) => ((result) ?? err));
  };

  dbInsertIntoMarketting = () => {
    for (let i = 1; i < 11; i += 1) {
      this.db.query(`INSERT INTO marketting (
        id_marketting, title, content, link, picture, video
        ) VALUES
        (${i}, "Pub Marketting ${i}", "Vous aussi vous voulez acheter du vent ?", "https://google.com/", null, null)
        `, (err, result) => ((result) ?? err));
    }
  };

  dbInsertIntoMovie = () => {
    for (let i = 1; i <= 100; i += 1) {
      const start = new Date(1950, 0, 1);
      const createdAt = new Date();
      const date = new Date(
        start.getTime() + Math.random() * (createdAt.getTime() - start.getTime())
      );
      const randCategory = Math.floor(Math.random() * (Math.ceil(9) - Math.ceil(1))) + 1;
      const randClassification = Math.floor(Math.random() * (Math.ceil(7) - Math.ceil(1))) + 1;
      const dateFormat = `${date.getFullYear()}-${(`0${(date.getMonth() + 1)}`).slice(-2)}-${(`0 ${date.getDate()}`).slice(-2)}`;
      const createdAtFormat = `${createdAt.getFullYear()}-${(`0${(createdAt.getMonth() + 1)}`).slice(-2)}-${(`0${createdAt.getDate()}`).slice(-2)}`;

      this.db.query(`INSERT INTO movie (
        id_movie, title, description, image_max, image_min, image_portrait, date, length, ranking, vote, view, video, created_at, updated_at, id_category, id_classification
        ) VALUES
        (${i}, "Movie ${i}", "Description movie ${i}", "https://picsum.photos/id/${i}/1780/721", "https://picsum.photos/id/${i}/400/240", "https://picsum.photos/id/${i}/214/290", "${dateFormat}", 3, 0, 0, 0, "https://www.youtube.com/watch?v=dQw4w9WgXcQ", "${createdAtFormat}", null, ${randCategory}, ${randClassification})
        `, (err, result) => ((result) ?? err));
    }
  };

  dbInsertIntoActor = () => {
    for (let i = 0; i <= names.length - 1; i += 1) {
      const fname = names[i].split(' ')[0];
      const lname = names[i].split(' ')[1];

      this.db.query(`INSERT INTO actor (
          id_actor, firstname, lastname, picture
        ) VALUES
        (${i}, "${fname}", "${lname}", "https://randomuser.me/api/?results=1")`, (err, result) => ((result) ?? err));
    }
  };

  dbInsertIntoMovieActor = () => {
    for (let i = 1; i <= 300; i += 1) {
      const randMovieId = Math.floor(Math.random() * (Math.ceil(100) - Math.ceil(1))) + 1;
      const randActorId = Math.floor(Math.random() * (Math.ceil(50) - Math.ceil(1))) + 1;

      this.db.query(`INSERT INTO movie_actor (
          id_movie, id_actor
        ) VALUES
        (${randMovieId}, ${randActorId})`, (err, result) => ((result) ?? err));
    }
  };

  run = async () => {
    this.dbInsertIntoCategory();
    this.dbInsertIntoClassification();
    this.dbInsertIntoPricing();
    this.dbInsertIntoMarketting();
    this.dbInsertIntoMovie();
    this.dbInsertIntoActor();
    this.dbInsertIntoMovieActor();
  };
};

export default DatabasePopulate;
