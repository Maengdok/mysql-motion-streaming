const MovieActorQueries = class {
  constructor(db) {
    this.db = db;
  }

  // #region CRUD MOVIE_ACTOR
  dbQuerySelectMovieActors = (callback) => {
    this.db.query('SELECT * FROM movie_actor ORDER BY id_movie, id_actor ASC', (err, result) => ((result) ? callback(result) : callback(err)));
  };

  dbQuerySelectActorsFromMovie = (movie, callback) => {
    this.db.query(`SELECT * FROM movie_actor RIGHT JOIN (SELECT * FROM actor) actor USING (id_actor) WHERE id_movie = ${movie} ORDER BY actor.firstname, actor.lastname ASC`, (err, result) => ((result) ? callback(result) : callback(err)));
  };

  dbQuerySelectMoviesFromActor = (actor, callback) => {
    this.db.query(`SELECT * FROM movie_actor RIGHT JOIN (SELECT * FROM movie) movie USING (id_movie) WHERE id_actor = ${actor} ORDER BY movie.title ASC`, (err, result) => ((result) ? callback(result) : callback(err)));
  };

  dbQueryAddMovieActor = (movie, actor, callback) => {
    this.db.query(`INSERT INTO movie_actor (id_movie, id_actor) VALUES (${movie}, ${actor})`, (err, result) => ((result) ? callback('Actor added to Movie successfully !') : callback(err)));
  };

  dbQueryDeleteMovieActor = (movie, actor, callback) => {
    this.db.query(`DELETE FROM movie_actor WHERE id_movie = ${movie} AND id_actor = ${actor}`, (err, result) => ((result) ? callback('Actor Movie deleted successfully !') : callback(err)));
  };
  // #endregion
};

export default MovieActorQueries;
