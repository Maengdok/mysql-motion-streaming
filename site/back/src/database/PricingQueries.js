const PricingQueries = class {
  constructor(db) {
    this.db = db;
  }

  // #region CRUD PRICING
  dbQuerySelectPricings = (callback) => {
    this.db.query('SELECT * FROM pricing ORDER BY price ASC', (err, result) => ((result) ? callback(result) : callback(err)));
  };

  dbQuerySelectPricing = (id, callback) => {
    this.db.query(`SELECT * FROM pricing WHERE id_pricing = ${id}`, (err, result) => ((result) ? callback(result) : callback(err)));
  };

  dbQueryAddPricing = (title, price, callback) => {
    this.db.query(`INSERT INTO pricing (title, price) VALUE ("${title}", ${price})`, (err, result) => ((result) ? callback('Pricing added successfully !') : callback(err)));
  };

  dbQueryUpdatePricing = (id, title, price, callback) => {
    this.db.query(`UPDATE pricing SET title = "${title}", price = ${price} WHERE id_pricing = ${id}`, (err, result) => ((result) ? callback('Pricing updated successfully !') : callback(err)));
  };

  dbQueryDeletePricing = (id, callback) => {
    this.db.query(`DELETE FROM pricing WHERE id_pricing = ${id}`, (err, result) => ((result) ? callback('Pricing deleted successfully !') : callback(err)));
  };
  // #endregion
};

export default PricingQueries;
