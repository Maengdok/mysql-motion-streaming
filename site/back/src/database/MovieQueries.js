const MovieQueries = class {
  constructor(db) {
    this.db = db;
  }

  // #region CRUD MOVIE
  dbQuerySelectMovies = (callback) => {
    this.db.query('SELECT * FROM movie LEFT JOIN(SELECT id_category, title AS category_title FROM category) cat ON movie.id_category = cat.id_category LEFT JOIN (SELECT id_classification, title AS classification_title FROM classification) cla ON movie.id_classification = cla.id_classification ORDER BY id_movie ASC', (err, result) => ((result) ? callback(result) : callback(err)));
  };

  dbQuerySelectMovie = (id, callback) => {
    this.db.query(`SELECT * FROM movie LEFT JOIN (SELECT id_category, title AS category_title FROM category) T2 ON movie.id_category = T2.id_category LEFT JOIN (SELECT id_classification, title AS classification_title FROM classification) T3 ON movie.id_classification = T3.id_classification WHERE id_movie = ${id}`, (err, result) => ((result) ? callback(result) : callback(err)));
  };

  dbQuerySelectMovieViews = (id, callback) => {
    this.db.query(`SELECT view FROM movie WHERE id_movie = ${id}`, (err, result) => ((result) ? callback(result) : callback(err)));
  };

  dbQuerySelectMovieRanking = (id, callback) => {
    this.db.query(`SELECT ranking, vote FROM movie WHERE id_movie = ${id}`, (err, result) => ((result) ? callback(result) : callback(err)));
  };

  dbQuerySelectMovieFromCategory = (id, callback) => {
    this.db.query(`SELECT * FROM movie LEFT JOIN (SELECT id_category, title AS category_title FROM category) T2 ON movie.id_category = T2.id_category LEFT JOIN (SELECT id_classification, title AS classification_title FROM classification) T3 ON movie.id_classification = T3.id_classification WHERE movie.id_category = ${id} ORDER BY title ASC`, (err, result) => ((result) ? callback(result) : callback(err)));
  };

  dbQueryAddMovie = (
    title,
    description,
    imageMax,
    imageMin,
    imagePortrait,
    date,
    length,
    video,
    category,
    classification,
    callback
  ) => {
    const createdAt = new Date();
    const formatCreatedAt = `${createdAt.getFullYear()}-${(`0${(createdAt.getMonth() + 1)}`).slice(-2)}-${(`0${createdAt.getDate()}`).slice(-2)} ${createdAt.getHours()}:${createdAt.getMinutes()}:${createdAt.getSeconds()}`;
    const createDate = new Date(date);
    const formatDate = `${createDate.getFullYear()}-${(`0${(createDate.getMonth() + 1)}`).slice(-2)}-${(`0${createDate.getDate()}`).slice(-2)}`;
    this.db.query(`INSERT INTO movie (
      title,
      description,
      image_max,
      image_min,
      image_portrait,
      date,
      length,
      ranking,
      vote,
      view,
      video,
      created_at,
      id_category,
      id_classification
    ) VALUE (
      "${title}",
      "${description}",
      "${imageMax}",
      "${imageMin}",
      "${imagePortrait}",
      "${formatDate}",
      ${length},
      0,
      0,
      0,
      "${video}",
      "${formatCreatedAt}",
      ${category},
      ${classification}
    )`, (err, result) => ((result) ? callback('Movie added successfully !') : callback(err)));
  };

  dbQueryUpdateMovie = (
    id,
    title,
    description,
    imageMax,
    imageMin,
    imagePortrait,
    date,
    length,
    video,
    category,
    classification,
    callback
  ) => {
    const updatedAt = new Date();
    const formatUpdatedAt = `${updatedAt.getFullYear()}-${(`0${(updatedAt.getMonth() + 1)}`).slice(-2)}-${(`0${updatedAt.getDate()}`).slice(-2)} ${updatedAt.getHours()}:${updatedAt.getMinutes()}:${updatedAt.getSeconds()}`;
    const createDate = new Date(date);
    const formatDate = `${createDate.getFullYear()}-${(`0${(createDate.getMonth() + 1)}`).slice(-2)}-${(`0${createDate.getDate()}`).slice(-2)}`;
    this.db.query(`UPDATE movie SET 
      title = "${title}", 
      description = "${description}", 
      image_max = "${imageMax}", 
      image_min = "${imageMin}", 
      image_portrait = "${imagePortrait}", 
      date = "${formatDate}", 
      length = ${length},
      video = "${video}",
      updated_at = "${formatUpdatedAt}",
      id_category = ${category},
      id_classification = ${classification}
      WHERE id_movie = ${id}
    `, (err, result) => ((result) ? callback('Movie updated successfully !') : callback(err)));
  };

  dbQueryUpdateMovieViews = (id, callback) => {
    this.db.query(`UPDATE movie SET view = view + 1 WHERE id_movie = ${id}`, (err, result) => ((result) ? callback('Movie views updated successfully !') : callback(err)));
  };

  dbQueryUpdateMovieRanking = (id, ranking, callback) => {
    this.db.query(`UPDATE movie SET vote = (vote + 1), ranking = ((ranking + ${ranking}) / vote) WHERE id_movie = ${id}`, (err, result) => ((result) ? callback('Movie ranking updated successfully !') : callback(err)));
  };

  dbQueryDeleteMovie = (id, callback) => {
    this.db.query(`DELETE FROM movie WHERE id_movie = ${id}`, (err, result) => ((result) ? callback('Movie deleted successfully !') : callback(err)));
  };

  dbQuerySelectMovieFromCategoryAndRanking = (category, callback) => {
    this.db.query(`SELECT * FROM movie LEFT JOIN (SELECT id_category, title AS category_title FROM category) T2 ON movie.id_category = T2.id_category LEFT JOIN (SELECT id_classification, title AS classification_title FROM classification) T3 ON movie.id_classification = T3.id_classification WHERE movie.id_category = ${category} ORDER BY ranking ASC LIMIT 6`, (err, result) => ((result) ? callback(result) : callback(err)));
  };

  dbQuerySearchMovieOrCategory = (title, callback) => {
    this.db.query(`
    SELECT * FROM movie
      LEFT JOIN (SELECT id_category, title AS category_title FROM category) p USING (id_category)
      WHERE (title LIKE '%${title}%' OR category_title LIKE '%${title}%')
      ORDER BY title ASC LIMIT 5
    `, (err, result) => ((result) ? callback(result) : callback(err)));
  };
  // #endregion
};

export default MovieQueries;
