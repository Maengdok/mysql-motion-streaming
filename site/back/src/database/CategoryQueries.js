const CategoryQueries = class {
  constructor(db) {
    this.db = db;
  }

  // #region CRUD GENRE
  dbQuerySelectCategories = (callback) => {
    this.db.query('SELECT * FROM category ORDER BY title ASC', (err, result) => ((result) ? callback(result) : callback(err)));
  };

  dbQuerySelectCategory = (id, callback) => {
    this.db.query(`SELECT * FROM category WHERE id_category = ${id}`, (err, result) => ((result) ? callback(result) : callback(err)));
  };

  dbQueryAddCategory = (title, picture, callback) => {
    this.db.query(`INSERT INTO category (title, picture) VALUES ("${title}", "${picture}")`, (err, result) => ((result) ? callback('Category added successfully !') : callback(err)));
  };

  dbQueryUpdateCategory = (id, title, picture, callback) => {
    this.db.query(`UPDATE category SET title = "${title}", picture = "${picture}" WHERE id_category = ${id}`, (err, result) => ((result) ? callback('Category updated successfully !') : callback(err)));
  };

  dbQueryDeleteCategory = (id, callback) => {
    this.db.query(`DELETE FROM category WHERE id_category = ${id}`, (err, result) => ((result) ? callback('Category deleted successfully !') : callback(err)));
  };
  // #endregion
};

export default CategoryQueries;
