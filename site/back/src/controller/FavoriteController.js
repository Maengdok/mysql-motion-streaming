import FavoriteQueries from '../database/FavoriteQueries.js';
import FavoriteNormalizer from '../normalizer/FavoriteNormalizer.js';

const FavoriteController = class {
  constructor(db, usedDb) {
    this.db = db;
    this.usedDb = usedDb;

    this.favoriteQueries = new FavoriteQueries(db, usedDb);
    this.favoriteNormalizer = new FavoriteNormalizer();
  }

  list = (callback) => {
    this.favoriteQueries.dbQuerySelectFavorites(
      (response) => callback(this.favoriteNormalizer.listNormalizer(response))
    );
  };

  show = (member, callback) => {
    this.favoriteQueries.dbQuerySelectFavorite(member, (response) => callback(response));
  };

  add = (member, movie, callback) => {
    if (typeof member !== 'number') throw new Error('Error : Member must be of type number');
    if (typeof movie !== 'number') throw new Error('Error : Movie must be of type number');

    this.favoriteQueries.dbQueryAddFavorite(member, movie, (response) => callback(response));
  };

  delete = (member, movie, callback) => {
    if (typeof member !== 'number') throw new Error('Error : Member must be of type number');
    if (typeof movie !== 'number') throw new Error('Error : Movie must be of type number');

    this.favoriteQueries.dbQueryDeleteFavorite(member, movie, (response) => callback(response));
  };
};

export default FavoriteController;
