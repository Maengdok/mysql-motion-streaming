import StatisticsQueries from '../database/StatisticsQueries.js';
import StatisticsNormalizer from '../normalizer/StatisticsNormalizer.js';

const StatisticsController = class {
  constructor(db, usedDb) {
    this.db = db;
    this.usedDb = usedDb;

    this.statisticsController = new StatisticsQueries(db, usedDb);
    this.statisticsNormalizer = new StatisticsNormalizer();
  }

  show5MostWatchedMovies = (callback) => {
    this.statisticsController.dbQuery5MostWatchedMovies(
      (response) => callback(this.statisticsNormalizer.showMoviesNormalizer(response))
    );
  };

  show10LatestMovies = (callback) => {
    this.statisticsController.dbQuery10LatestMovies(
      (response) => callback(this.statisticsNormalizer.showMoviesNormalizer(response))
    );
  };

  show3RandomAdvertises = (callback) => {
    this.statisticsController.dbQuery3RandomAdvertises(
      (response) => callback(this.statisticsNormalizer.showAdverstisesNormalizer(response))
    );
  };

  // TODO : REWORK MOVIE TO ADD THE NUMBER OF TIME A MEMBER HAS SEEN THE MOVIE
  // showWatchedMovies = (member, movie, callback) => {
  //   if (typeof member !== "number") throw "Error : Member must be of type number";
  //   if (typeof movie !== "number") throw "Error : Movie must be of type number";

  //   this.statisticsController.dbQueryCountWatchedMovie(
  //   member,
  // movie,
  //   (response) => callback(this.statisticsNormalizer.showWatchedMovies(response))
  // );
  // };

  showCountFavorites = (member, callback) => {
    if (typeof member !== 'number') throw new Error('Error : Member must be of type number');

    this.statisticsController.dbQueryCountmemberFavorites(
      member,
      (response) => callback(this.statisticsNormalizer.showCountFavoritesNormalizer(response))
    );
  };
};

export default StatisticsController;
