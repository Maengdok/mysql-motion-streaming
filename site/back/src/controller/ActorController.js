import ActorQueries from '../database/ActorQueries.js';
import ActorNormalizer from '../normalizer/ActorNormalizer.js';

const ActorController = class {
  constructor(db, usedDb) {
    this.db = db;
    this.usedDb = usedDb;

    this.actorQueries = new ActorQueries(db, usedDb);
    this.actorNormalizer = new ActorNormalizer();
  }

  list = (callback) => {
    this.actorQueries.dbQuerySelectActors(
      (response) => callback(this.actorNormalizer.listNormalizer(response))
    );
  };

  show = (id, callback) => {
    this.actorQueries.dbQuerySelectActor(
      id,
      (response) => callback(this.actorNormalizer.showNormalizer(response))
    );
  };

  add = (firstname, lastname, picture, callback) => {
    if (typeof firstname !== 'string') throw new Error('Error : Firstname must be of type string');
    if (typeof lastname !== 'string') throw new Error('Error : Lastname must be of type string');
    if (typeof picture !== 'string') throw new Error('Error : Picture must be of type string');

    this.actorQueries.dbQueryAddActor(
      firstname,
      lastname,
      picture,
      (response) => callback(response)
    );
  };

  update = (id, firstname, lastname, picture, callback) => {
    if (typeof id !== 'number') throw new Error('Error : Id must be of type integer');
    if (typeof firstname !== 'string') throw new Error('Error : Firstname must be of type string');
    if (typeof lastname !== 'string') throw new Error('Error : Lastname must be of type string');
    if (typeof picture !== 'string') throw new Error('Error : Picture must be of type string');

    this.actorQueries.dbQueryUpdateActor(
      id,
      firstname,
      lastname,
      picture,
      (response) => callback(response)
    );
  };

  delete = (id, callback) => {
    this.actorQueries.dbQueryDeleteActor(id, (response) => callback(response));
  };
};

export default ActorController;
