import MovieQueries from '../database/MovieQueries.js';
import MovieNormalizer from '../normalizer/MovieNormalizer.js';

const MovieController = class {
  constructor(db, usedDb) {
    this.db = db;
    this.usedDb = usedDb;

    this.movieQueries = new MovieQueries(db, usedDb);
    this.movieNormalizer = new MovieNormalizer();
  }

  list = (callback) => {
    this.movieQueries.dbQuerySelectMovies(
      (response) => callback(this.movieNormalizer.listNormalizer(response))
    );
  };

  show = (id, callback) => {
    this.movieQueries.dbQuerySelectMovie(
      id,
      (response) => callback(this.movieNormalizer.showNormalizer(response))
    );
  };

  category = (id, callback) => {
    this.movieQueries.dbQuerySelectMovieFromCategory(
      id,
      (response) => callback(this.movieNormalizer.listNormalizer(response))
    );
  };

  add = (
    title,
    description,
    imageMax,
    imageMin,
    imagePortrait,
    date,
    length,
    video,
    category,
    classification,
    callback
  ) => {
    if (typeof title !== 'string') throw new Error('Error : Title must be of type string');
    if (typeof description !== 'string') throw new Error('Error : Description must be of type string');
    if (typeof imageMax !== 'string') throw new Error('Error : ImageMax must be of type string');
    if (typeof imageMin !== 'string') throw new Error('Error : ImageMin must be of type string');
    if (typeof imagePortrait !== 'string') throw new Error('Error : ImagePortrait must be of type string');
    if (typeof date !== 'string') throw new Error('Error : Date must be of type string');
    if (typeof length !== 'number') throw new Error('Error : Length must be of type number');
    if (typeof video !== 'string') throw new Error('Error : Video must be of type string');
    if (typeof category !== 'number') throw new Error('Error : Category must be of type number');
    if (typeof classification !== 'number') throw new Error('Error : Classification must be of type number');

    this.movieQueries.dbQueryAddMovie(
      title,
      description,
      imageMax,
      imageMin,
      imagePortrait,
      date,
      length,
      video,
      category,
      classification,
      (response) => callback(response)
    );
  };

  update = (
    id,
    title,
    description,
    imageMax,
    imageMin,
    imagePortrait,
    date,
    length,
    video,
    category,
    classification,
    callback
  ) => {
    if (typeof id !== 'number') throw new Error('Error : Id must be of type number');
    if (typeof title !== 'string') throw new Error('Error : Title must be of type string');
    if (typeof description !== 'string') throw new Error('Error : Description must be of type string');
    if (typeof imageMax !== 'string') throw new Error('Error : ImageMax must be of type string');
    if (typeof imageMin !== 'string') throw new Error('Error : ImageMin must be of type string');
    if (typeof imagePortrait !== 'string') throw new Error('Error : ImagePortrait must be of type string');
    if (typeof date !== 'string') throw new Error('Error : Date must be of type string');
    if (typeof length !== 'number') throw new Error('Error : Length must be of type number');
    if (typeof video !== 'string') throw new Error('Error : Video must be of type string');
    if (typeof category !== 'number') throw new Error('Error : Category must be of type number');
    if (typeof classification !== 'number') throw new Error('Error : Classification must be of type number');

    this.movieQueries.dbQueryUpdateMovie(
      id,
      title,
      description,
      imageMax,
      imageMin,
      imagePortrait,
      date,
      length,
      video,
      category,
      classification,
      (response) => callback(response)
    );
  };

  delete = (id, callback) => {
    this.movieQueries.dbQueryDeleteMovie(id, (response) => callback(response));
  };

  showViews = (id, callback) => {
    this.movieQueries.dbQuerySelectMovieViews(
      id,
      (response) => callback(this.movieNormalizer.viewsNormalizer(response))
    );
  };

  updateViews = (id, callback) => {
    this.movieQueries.dbQueryUpdateMovieViews(id, (response) => callback(response));
  };

  showRanking = (id, callback) => {
    this.movieQueries.dbQuerySelectMovieRanking(
      id,
      (response) => callback(this.movieNormalizer.rankingNormalizer(response))
    );
  };

  updateRanking = (id, ranking, callback) => {
    this.movieQueries.dbQueryUpdateMovieRanking(id, ranking, (response) => callback(response));
  };

  show6MoviesFromCategoryAndRanking = (category, callback) => {
    this.movieQueries.dbQuerySelectMovieFromCategoryAndRanking(
      category,
      (response) => callback(this.movieNormalizer.listNormalizer(response))
    );
  };

  search = (title, callback) => {
    this.movieQueries.dbQuerySearchMovieOrCategory(
      title,
      (response) => callback(this.movieNormalizer.searchNormalizer(response))
    );
  };
};

export default MovieController;
