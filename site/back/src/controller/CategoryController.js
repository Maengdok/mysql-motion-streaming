import CategoryQueries from '../database/CategoryQueries.js';
import CategoryNormalizer from '../normalizer/CategoryNormalizer.js';

const CategoryController = class {
  constructor(db, usedDb) {
    this.db = db;
    this.usedDb = usedDb;

    this.categoryQueries = new CategoryQueries(db, usedDb);
    this.categoryNormalizer = new CategoryNormalizer();
  }

  list = (callback) => {
    this.categoryQueries.dbQuerySelectCategories(
      (response) => callback(this.categoryNormalizer.listNormalizer(response))
    );
  };

  show = (id, callback) => {
    this.categoryQueries.dbQuerySelectCategory(
      id,
      (response) => callback(this.categoryNormalizer.showNormalizer(response))
    );
  };

  add = (title, picture, callback) => {
    if (typeof title !== 'string') throw new Error('Error : Title must be of type string');
    if (typeof picture !== 'string') throw new Error('Error : Picture must be of type string');

    this.categoryQueries.dbQueryAddCategory(title, picture, (response) => callback(response));
  };

  update = (id, title, picture, callback) => {
    if (typeof id !== 'number') throw new Error('Error : Id must be of type integer');
    if (typeof title !== 'string') throw new Error('Error : Title must be of type string');
    if (typeof picture !== 'string') throw new Error('Error : Picture must be of type string');

    this.categoryQueries.dbQueryUpdateCategory(
      id,
      title,
      picture,
      (response) => callback(response)
    );
  };

  delete = (id, callback) => {
    this.categoryQueries.dbQueryDeleteCategory(id, (response) => callback(response));
  };
};

export default CategoryController;
