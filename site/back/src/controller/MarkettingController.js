import MarkettingQueries from '../database/MarkettingQueries.js';
import MarkettingNormalizer from '../normalizer/MarkettingNormalizer.js';

const MarkettingController = class {
  constructor(db, usedDb) {
    this.db = db;
    this.usedDb = usedDb;

    this.markettingQueries = new MarkettingQueries(db, usedDb);
    this.markettingNormalizer = new MarkettingNormalizer();
  }

  list = (callback) => {
    this.markettingQueries.dbQuerySelectMarkettings(
      (response) => callback(this.markettingNormalizer.listNormalizer(response))
    );
  };

  show = (id, callback) => {
    this.markettingQueries.dbQuerySelectMarketting(
      id,
      (response) => callback(this.markettingNormalizer.showNormalizer(response))
    );
  };

  add = (title, content, link, picture, video, callback) => {
    if (typeof title !== 'string') throw new Error('Error : Title must be of type string');
    if (typeof content !== 'string') throw new Error('Error : Content must be of type string');
    if (typeof link !== 'string') throw new Error('Error : Link must be of type string');
    if (typeof picture !== 'string') throw new Error('Error : Picture must be of type string');
    if (typeof video !== 'string') throw new Error('Error : Video must be of type string');

    this.markettingQueries.dbQueryAddMarketting(
      title,
      content,
      link,
      picture,
      video,
      (response) => callback(response)
    );
  };

  update = (id, title, content, link, picture, video, callback) => {
    if (typeof id !== 'number') throw new Error('Error : Id must be of type integer');
    if (typeof title !== 'string') throw new Error('Error : Title must be of type string');
    if (typeof content !== 'string') throw new Error('Error : Content must be of type string');
    if (typeof link !== 'string') throw new Error('Error : Link must be of type string');
    if (typeof picture !== 'string') throw new Error('Error : Picture must be of type string');
    if (typeof video !== 'string') throw new Error('Error : Video must be of type string');

    this.markettingQueries.dbQueryUpdateMarketting(
      id,
      title,
      content,
      link,
      picture,
      video,
      (response) => callback(response)
    );
  };

  delete = (id, callback) => {
    this.markettingQueries.dbQueryDeleteMarketting(id, (response) => callback(response));
  };
};

export default MarkettingController;
