import MovieActorQueries from '../database/MovieActorQueries.js';
import MovieActorNormalizer from '../normalizer/MovieActorNormalizer.js';

const MovieActorController = class {
  constructor(db, usedDb) {
    this.db = db;
    this.usedDb = usedDb;

    this.movieActorQueries = new MovieActorQueries(db, usedDb);
    this.movieActorNormalizer = new MovieActorNormalizer();
  }

  list = (callback) => {
    this.movieActorQueries.dbQuerySelectMovieActors(
      (response) => callback(this.movieActorNormalizer.listNormalizer(response))
    );
  };

  add = (movie, actor, callback) => {
    if (typeof movie !== 'number') throw new Error('Error : Movie must be of type number');
    if (typeof actor !== 'number') throw new Error('Error : Actor must be of type number');

    this.movieActorQueries.dbQueryAddMovieActor(movie, actor, (response) => callback(response));
  };

  showActorsFromMovie = (movie, callback) => {
    if (typeof movie !== 'number') throw new Error('Error : Movie must be of type number');

    this.movieActorQueries.dbQuerySelectActorsFromMovie(
      movie,
      (response) => callback(this.movieActorNormalizer.showActorsFromMovieNormalizer(response))
    );
  };

  showMoviesFromActor = (actor, callback) => {
    if (typeof actor !== 'number') throw new Error('Error : Actor must be of type number');

    this.movieActorQueries.dbQuerySelectMoviesFromActor(
      actor,
      (response) => callback(this.movieActorNormalizer.showMoviesFromActorNormalizer(response))
    );
  };

  delete = (movie, actor, callback) => {
    if (typeof movie !== 'number') throw new Error('Error : Movie must be of type number');
    if (typeof actor !== 'number') throw new Error('Error : Actor must be of type number');

    this.movieActorQueries.dbQueryDeleteMovieActor(movie, actor, (response) => callback(response));
  };
};

export default MovieActorController;
