import PricingQueries from '../database/PricingQueries.js';
import PricingNormalizer from '../normalizer/PricingNormalizer.js';

const PricingController = class {
  constructor(db, usedDb) {
    this.db = db;
    this.usedDb = usedDb;

    this.pricingQueries = new PricingQueries(db, usedDb);
    this.pricingNormalizer = new PricingNormalizer();
  }

  list = (callback) => {
    this.pricingQueries.dbQuerySelectPricings(
      (response) => callback(this.pricingNormalizer.listNormalizer(response))
    );
  };

  show = (id, callback) => {
    this.pricingQueries.dbQuerySelectPricing(
      id,
      (response) => callback(this.pricingNormalizer.showNormalizer(response))
    );
  };

  add = (title, price, callback) => {
    if (typeof title !== 'string') throw new Error('Error : Title must be of type string');
    if (typeof price !== 'number') throw new Error('Error : Price must be of type number');

    this.pricingQueries.dbQueryAddPricing(title, price, (response) => callback(response));
  };

  update = (id, title, price, callback) => {
    if (typeof id !== 'number') throw new Error('Error : Id must be of type integer');
    if (typeof title !== 'string') throw new Error('Error : Title must be of type string');
    if (typeof price !== 'number') throw new Error('Error : Price must be of type number');

    this.pricingQueries.dbQueryUpdatePricing(id, title, price, (response) => callback(response));
  };

  delete = (id, callback) => {
    this.pricingQueries.dbQueryDeletePricing(id, (response) => callback(response));
  };
};

export default PricingController;
