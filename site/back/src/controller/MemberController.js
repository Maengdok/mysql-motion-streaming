import MemberQueries from '../database/MemberQueries.js';
import MemberNormalizer from '../normalizer/MemberNormalizer.js';

const MemberController = class {
  constructor(db, usedDb) {
    this.db = db;
    this.usedDb = usedDb;

    this.memberQueries = new MemberQueries(db, usedDb);
    this.memberNormalizer = new MemberNormalizer();
  }

  list = (callback) => {
    this.memberQueries.dbQuerySelectMembers(
      (response) => callback(this.memberNormalizer.listNormalizer(response))
    );
  };

  show = (id, callback) => {
    this.memberQueries.dbQuerySelectMember(
      id,
      (response) => callback(this.memberNormalizer.showNormalizer(response))
    );
  };

  add = (
    email,
    password,
    firstname,
    lastname,
    address,
    zipcode,
    city,
    picture,
    pricing,
    callback
  ) => {
    if (typeof email !== 'string') throw new Error('Error : Title must be of type string');
    if (typeof password !== 'string') throw new Error('Error : Password must be of type string');
    if (typeof firstname !== 'string') throw new Error('Error : Firstname must be of type string');
    if (typeof lastname !== 'string') throw new Error('Error : Lastname must be of type string');
    if (typeof address !== 'string') throw new Error('Error : Address must be of type string');
    if (typeof zipcode !== 'string') throw new Error('Error : Zipcode must be of type string');
    if (typeof city !== 'string') throw new Error('Error : City must be of type string');
    if (typeof picture !== 'string') throw new Error('Error : Picture must be of type string');
    if (typeof pricing !== 'number') throw new Error('Error : Pricing must be of type number');

    this.memberQueries.dbQueryAddMember(
      email,
      password,
      firstname,
      lastname,
      address,
      zipcode,
      city,
      picture,
      pricing,
      (response) => callback(response)
    );
  };

  update = (
    id,
    email,
    password,
    firstname,
    lastname,
    address,
    zipcode,
    city,
    picture,
    pricing,
    callback
  ) => {
    if (typeof id !== 'number') throw new Error('Error : Id must be of type number');
    if (typeof email !== 'string') throw new Error('Error : Title must be of type string');
    if (typeof password !== 'string') throw new Error('Error : Password must be of type string');
    if (typeof firstname !== 'string') throw new Error('Error : Firstname must be of type string');
    if (typeof lastname !== 'string') throw new Error('Error : Lastname must be of type string');
    if (typeof address !== 'string') throw new Error('Error : Address must be of type string');
    if (typeof zipcode !== 'string') throw new Error('Error : Zipcode must be of type string');
    if (typeof city !== 'string') throw new Error('Error : City must be of type string');
    if (typeof picture !== 'string') throw new Error('Error : Picture must be of type string');
    if (typeof pricing !== 'number') throw new Error('Error : Pricing must be of type number');

    this.memberQueries.dbQueryUpdateMember(
      id,
      email,
      password,
      firstname,
      lastname,
      address,
      zipcode,
      city,
      picture,
      pricing,
      (response) => callback(response)
    );
  };

  delete = (id, callback) => {
    this.memberQueries.dbQueryDeleteMember(id, (response) => callback(response));
  };
};

export default MemberController;
