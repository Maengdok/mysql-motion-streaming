import ClassificationQueries from '../database/ClassificationQueries.js';
import ClassificationNormalizer from '../normalizer/ClassificationNormalizer.js';

const ClassificationController = class {
  constructor(db, usedDb) {
    this.db = db;
    this.usedDb = usedDb;

    this.classificationQueries = new ClassificationQueries(db, usedDb);
    this.classificationNormalizer = new ClassificationNormalizer();
  }

  list = (callback) => {
    this.classificationQueries.dbQuerySelectClassificatons(
      (response) => callback(this.classificationNormalizer.listNormalizer(response))
    );
  };

  show = (id, callback) => {
    this.classificationQueries.dbQuerySelectClassification(
      id,
      (response) => callback(this.classificationNormalizer.showNormalizer(response))
    );
  };

  add = (title, callback) => {
    if (typeof title !== 'string') throw new Error('Error : Title must be of type string');

    this.classificationQueries.dbQueryAddClassification(title, (response) => callback(response));
  };

  update = (id, title, callback) => {
    if (typeof id !== 'number') throw new Error('Error : Id must be of type integer');
    if (typeof title !== 'string') throw new Error('Error : Title must be of type string');

    this.classificationQueries.dbQueryUpdateClassification(
      id,
      title,
      (response) => callback(response)
    );
  };

  delete = (id, callback) => {
    this.classificationQueries.dbQueryDeleteClassification(id, (response) => callback(response));
  };
};

export default ClassificationController;
