const request = require('supertest');

const baseUrl = 'http://127.0.0.1:8080';

describe(`${baseUrl}/api/classification/list`, () => {
  describe('GET list classification =>', () => {
    test('Should get the response method with status code 200', async () => {
      const res = await request(baseUrl)
        .get('/api/classification/list')
        .set('Accept', 'application/json');

      expect(res.statusCode).toBe(200);
    });
  });
});

describe(`${baseUrl}/api/classification/show`, () => {
  describe('GET classification =>', () => {
    test('Should get the response method with status code 200', async () => {
      const res = await request(baseUrl)
        .get('/api/classification/show/6')
        .set('Accept', 'application/json');

      expect(res.statusCode).toBe(200);
    });
  });
});

describe(`${baseUrl}/api/classification/add`, () => {
  describe('POST classification =>', () => {
    test('Should get the response method with status code 200', async () => {
      const classification = {
        title: 'UnitTestclassification'
      };
      const res = await request(baseUrl)
        .post('/api/classification/add')
        .set('Accept', 'application/json')
        .send(classification);

      expect(res.statusCode).toBe(200);
      expect(res.body).toBe('Classification added successfully !');
    });
  });
});

describe(`${baseUrl}/api/classification/update`, () => {
  describe('PATCH classification =>', () => {
    test('Should get the response method with status code 200', async () => {
      const classification = {
        id: 7,
        title: 'UnitTestUpdateclassification'
      };
      const res = await request(baseUrl)
        .patch('/api/classification/update')
        .set('Accept', 'application/json')
        .send(classification);

      expect(res.statusCode).toBe(200);
      expect(res.body).toBe('Classification updated successfully !');
    });
  });
});

describe(`${baseUrl}/api/classification/delete`, () => {
  describe('DELETE classification =>', () => {
    test('Should get the response method with status code 200', async () => {
      const res = await request(baseUrl)
        .delete('/api/classification/delete/7')
        .set('Accept', 'application/json');

      expect(res.statusCode).toBe(200);
      expect(res.body).toBe('Classification deleted successfully !');
    });
  });
});

describe(`${baseUrl}/api/classification/add`, () => {
  describe('POST classification with wrong type on title =>', () => {
    test('Should get the response method with status code 500', async () => {
      const classification = {
        title: 1
      };
      const res = await request(baseUrl)
        .post('/api/classification/add')
        .set('Accept', 'application/json')
        .send(classification);

      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/classification/update`, () => {
  describe('PATCH classification with wrong type on title =>', () => {
    test('Should get the response method with status code 200', async () => {
      const classification = {
        id: 7,
        title: 1
      };
      const res = await request(baseUrl)
        .patch('/api/classification/update')
        .set('Accept', 'application/json')
        .send(classification);

      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/classification/add`, () => {
  describe('POST classification with empty data on title =>', () => {
    test('Should get the response method with status code 200', async () => {
      const classification = {};
      const res = await request(baseUrl)
        .post('/api/classification/add')
        .set('Accept', 'application/json')
        .send(classification);

      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/classification/update`, () => {
  describe('PATCH classification with empty data on title =>', () => {
    test('Should get the response method with status code 200', async () => {
      const classification = {
        id: 7
      };
      const res = await request(baseUrl)
        .patch('/api/classification/update')
        .set('Accept', 'application/json')
        .send(classification);

      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});
