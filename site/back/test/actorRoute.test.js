const request = require('supertest');

const baseUrl = 'http://127.0.0.1:8080';

describe(`${baseUrl}/api/actor/list`, () => {
  describe('GET list actor =>', () => {
    test('Should get the response method with status code 200', async () => {
      const res = await request(baseUrl)
        .get('/api/actor/list')
        .set('Accept', 'application/json');
      expect(res.statusCode).toBe(200);
    });
  });
});

describe(`${baseUrl}/api/actor/show`, () => {
  describe('GET actor =>', () => {
    test('Should get the response method with status code 200', async () => {
      const res = await request(baseUrl)
        .get('/api/actor/show/6')
        .set('Accept', 'application/json');
      expect(res.statusCode).toBe(200);
    });
  });
});

describe(`${baseUrl}/api/actor/add`, () => {
  describe('POST actor =>', () => {
    test('Should get the response method with status code 200', async () => {
      const actor = {
        firstname: 'Unit',
        lastname: 'Test',
        picture: ''
      };
      const res = await request(baseUrl)
        .post('/api/actor/add')
        .set('Accept', 'application/json')
        .send(actor);
      expect(res.statusCode).toBe(200);
      expect(res.body).toBe('Actor added successfully !');
    });
  });
});

describe(`${baseUrl}/api/actor/update`, () => {
  describe('PATCH actor =>', () => {
    test('Should get the response method with status code 200', async () => {
      const actor = {
        id: 51,
        firstname: 'Unit',
        lastname: 'TestUpdate',
        picture: ''
      };
      const res = await request(baseUrl)
        .patch('/api/actor/update')
        .set('Accept', 'application/json')
        .send(actor);
      expect(res.statusCode).toBe(200);
      expect(res.body).toBe('Actor updated successfully !');
    });
  });
});

describe(`${baseUrl}/api/actor/delete/51`, () => {
  describe('DELETE actor =>', () => {
    test('Should get the response method with status code 200', async () => {
      const res = await request(baseUrl)
        .delete('/api/actor/delete/50')
        .set('Accept', 'application/json');
      expect(res.statusCode).toBe(200);
      expect(res.body).toBe('Actor deleted successfully !');
    });
  });
});

describe(`${baseUrl}/api/actor/add`, () => {
  describe('POST actor with wrong type on firstname =>', () => {
    test('Should get the response method with status code 500', async () => {
      const actor = {
        firstname: 1,
        lastname: 'Test',
        picture: ''
      };
      const res = await request(baseUrl)
        .post('/api/actor/add')
        .set('Accept', 'application/json')
        .send(actor);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/actor/add`, () => {
  describe('POST actor with wrong type on lastname =>', () => {
    test('Should get the response method with status code 500', async () => {
      const actor = {
        firstname: 'Unit',
        lastname: 1,
        picture: ''
      };
      const res = await request(baseUrl)
        .post('/api/actor/add')
        .set('Accept', 'application/json')
        .send(actor);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/actor/add`, () => {
  describe('POST actor with wrong type on picture =>', () => {
    test('Should get the response method with status code 500', async () => {
      const actor = {
        firstname: 'Unit',
        lastname: 'Test',
        picture: 1
      };
      const res = await request(baseUrl)
        .post('/api/actor/add')
        .set('Accept', 'application/json')
        .send(actor);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/actor/update`, () => {
  describe('PATCH actor with wrong type on firstname =>', () => {
    test('Should get the response method with status code 500', async () => {
      const actor = {
        id: 51,
        firstname: 1,
        lastname: 'TestUpdate',
        picture: ''
      };
      const res = await request(baseUrl)
        .patch('/api/actor/update')
        .set('Accept', 'application/json')
        .send(actor);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/actor/update`, () => {
  describe('PATCH actor with wrong type on lastname =>', () => {
    test('Should get the response method with status code 500', async () => {
      const actor = {
        id: 51,
        firstname: 'Unit',
        lastname: 1,
        picture: ''
      };
      const res = await request(baseUrl)
        .patch('/api/actor/update')
        .set('Accept', 'application/json')
        .send(actor);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/actor/update`, () => {
  describe('PATCH actor with wrong type on picture =>', () => {
    test('Should get the response method with status code 500', async () => {
      const actor = {
        id: 51,
        firstname: 'Unit',
        lastname: 'TestUpdate',
        picture: 1
      };
      const res = await request(baseUrl)
        .patch('/api/actor/update')
        .set('Accept', 'application/json')
        .send(actor);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/actor/add`, () => {
  describe('POST actor with empty data on firstname =>', () => {
    test('Should get the response method with status code 500', async () => {
      const actor = {
        lastname: 'Test',
        picture: ''
      };
      const res = await request(baseUrl)
        .post('/api/actor/add')
        .set('Accept', 'application/json')
        .send(actor);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/actor/add`, () => {
  describe('POST actor with empty data on lastname =>', () => {
    test('Should get the response method with status code 500', async () => {
      const actor = {
        firstname: 'Unit',
        picture: ''
      };
      const res = await request(baseUrl)
        .post('/api/actor/add')
        .set('Accept', 'application/json')
        .send(actor);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/actor/add`, () => {
  describe('POST actor with empty data on picture =>', () => {
    test('Should get the response method with status code 500', async () => {
      const actor = {
        firstname: 'Unit',
        lastname: 'Test'
      };
      const res = await request(baseUrl)
        .post('/api/actor/add')
        .set('Accept', 'application/json')
        .send(actor);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/actor/update`, () => {
  describe('PATCH actor with empty data on firstname =>', () => {
    test('Should get the response method with status code 500', async () => {
      const actor = {
        id: 51,
        lastname: 'TestUpdate',
        picture: ''
      };
      const res = await request(baseUrl)
        .patch('/api/actor/update')
        .set('Accept', 'application/json')
        .send(actor);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/actor/update`, () => {
  describe('PATCH actor with empty data on lastname =>', () => {
    test('Should get the response method with status code 500', async () => {
      const actor = {
        id: 51,
        firstname: 'Unit',
        picture: ''
      };
      const res = await request(baseUrl)
        .patch('/api/actor/update')
        .set('Accept', 'application/json')
        .send(actor);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/actor/update`, () => {
  describe('PATCH actor with empty data on picture =>', () => {
    test('Should get the response method with status code 500', async () => {
      const actor = {
        id: 51,
        firstname: 'Unit',
        lastname: 'Test'
      };
      const res = await request(baseUrl)
        .patch('/api/actor/update')
        .set('Accept', 'application/json')
        .send(actor);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});
