const request = require('supertest');

const baseUrl = 'http://127.0.0.1:8080';

describe(`${baseUrl}/api/pricing/list`, () => {
  describe('GET list pricing =>', () => {
    test('Should get the response method with status code 200', async () => {
      const res = await request(baseUrl)
        .get('/api/pricing/list')
        .set('Accept', 'application/json');
      expect(res.statusCode).toBe(200);
    });
  });
});

describe(`${baseUrl}/api/pricing/show`, () => {
  describe('GET pricing =>', () => {
    test('Should get the response method with status code 200', async () => {
      const res = await request(baseUrl)
        .get('/api/pricing/show/1')
        .set('Accept', 'application/json');
      expect(res.statusCode).toBe(200);
    });
  });
});

describe(`${baseUrl}/api/pricing/add`, () => {
  describe('POST pricing =>', () => {
    test('Should get the response method with status code 200', async () => {
      const pricing = {
        title: 'UnitTestPricing',
        price: 20000
      };
      const res = await request(baseUrl)
        .post('/api/pricing/add')
        .set('Accept', 'application/json')
        .send(pricing);
      expect(res.statusCode).toBe(200);
      expect(res.body).toBe('Pricing added successfully !');
    });
  });
});

describe(`${baseUrl}/api/pricing/update`, () => {
  describe('PATCH pricing =>', () => {
    test('Should get the response method with status code 200', async () => {
      const pricing = {
        id: 5,
        title: 'UnitTestPricingUpdate',
        price: 20000
      };
      const res = await request(baseUrl)
        .patch('/api/pricing/update')
        .set('Accept', 'application/json')
        .send(pricing);
      expect(res.statusCode).toBe(200);
      expect(res.body).toBe('Pricing updated successfully !');
    });
  });
});

describe(`${baseUrl}/api/pricing/delete/13`, () => {
  describe('DELETE pricing =>', () => {
    test('Should get the response method with status code 200', async () => {
      const res = await request(baseUrl)
        .delete('/api/pricing/delete/5')
        .set('Accept', 'application/json');
      expect(res.statusCode).toBe(200);
      expect(res.body).toBe('Pricing deleted successfully !');
    });
  });
});

describe(`${baseUrl}/api/pricing/add`, () => {
  describe('POST pricing with wrong type on title =>', () => {
    test('Should get the response method with status code 500', async () => {
      const pricing = {
        title: 1,
        price: 20000
      };
      const res = await request(baseUrl)
        .post('/api/pricing/add')
        .set('Accept', 'application/json')
        .send(pricing);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/pricing/add`, () => {
  describe('POST pricing with wrong type on price =>', () => {
    test('Should get the response method with status code 500', async () => {
      const pricing = {
        title: 'UnitTestPricing',
        price: '20000'
      };
      const res = await request(baseUrl)
        .post('/api/pricing/add')
        .set('Accept', 'application/json')
        .send(pricing);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/pricing/update`, () => {
  describe('PATCH pricing with wrong type on title =>', () => {
    test('Should get the response method with status code 500', async () => {
      const pricing = {
        id: 5,
        title: 1,
        price: 20000
      };
      const res = await request(baseUrl)
        .patch('/api/pricing/update')
        .set('Accept', 'application/json')
        .send(pricing);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/pricing/update`, () => {
  describe('PATCH pricing with wrong type on price =>', () => {
    test('Should get the response method with status code 500', async () => {
      const pricing = {
        id: 5,
        title: 'UnitTestPricing',
        price: '20000'
      };
      const res = await request(baseUrl)
        .patch('/api/pricing/update')
        .set('Accept', 'application/json')
        .send(pricing);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/pricing/add`, () => {
  describe('POST pricing with empty data on title =>', () => {
    test('Should get the response method with status code 500', async () => {
      const pricing = {
        price: 20000
      };
      const res = await request(baseUrl)
        .post('/api/pricing/add')
        .set('Accept', 'application/json')
        .send(pricing);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/pricing/add`, () => {
  describe('POST pricing with empty data on price =>', () => {
    test('Should get the response method with status code 500', async () => {
      const pricing = {
        title: 'UnitTestPricing'
      };
      const res = await request(baseUrl)
        .post('/api/pricing/add')
        .set('Accept', 'application/json')
        .send(pricing);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/pricing/update`, () => {
  describe('PATCH pricing with empty data on title =>', () => {
    test('Should get the response method with status code 500', async () => {
      const pricing = {
        id: 5,
        price: 20000
      };
      const res = await request(baseUrl)
        .patch('/api/pricing/update')
        .set('Accept', 'application/json')
        .send(pricing);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/pricing/update`, () => {
  describe('PATCH pricing with empty data on price =>', () => {
    test('Should get the response method with status code 500', async () => {
      const pricing = {
        id: 5,
        title: 'UnitTestPricing'
      };
      const res = await request(baseUrl)
        .patch('/api/pricing/update')
        .set('Accept', 'application/json')
        .send(pricing);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});
