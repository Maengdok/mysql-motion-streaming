const request = require('supertest');

const baseUrl = 'http://127.0.0.1:8080';

describe(`${baseUrl}/api/member/list`, () => {
  describe('GET list member =>', () => {
    test('Should get the response method with status code 200', async () => {
      const res = await request(baseUrl)
        .get('/api/member/list')
        .set('Accept', 'application/json');
      expect(res.statusCode).toBe(200);
    });
  });
});

describe(`${baseUrl}/api/member/show`, () => {
  describe('GET member =>', () => {
    test('Should get the response method with status code 200', async () => {
      const res = await request(baseUrl)
        .get('/api/member/show/1')
        .set('Accept', 'application/json');
      expect(res.statusCode).toBe(200);
    });
  });
});

describe(`${baseUrl}/api/member/add`, () => {
  describe('POST member =>', () => {
    test('Should get the response method with status code 200', async () => {
      const member = {
        email: 'unit.test@test.com',
        password: '',
        firstname: 'Unit',
        lastname: 'Test',
        address: '5 rd of Jest',
        zipcode: '99999',
        city: 'JestVille',
        picture: '',
        pricing: 1
      };
      const res = await request(baseUrl)
        .post('/api/member/add')
        .set('Accept', 'application/json')
        .send(member);
      expect(res.statusCode).toBe(200);
      expect(res.body).toBe('Member added successfully !');
    });
  });
});

describe(`${baseUrl}/api/member/update`, () => {
  describe('PATCH member =>', () => {
    test('Should get the response method with status code 200', async () => {
      const member = {
        id: 2,
        email: 'unit.test@test.com',
        password: '',
        firstname: 'UnitUpdate',
        lastname: 'TestUpdate',
        address: '5 rd of Jest',
        zipcode: '99999',
        city: 'JestVille',
        picture: '',
        pricing: 1
      };
      const res = await request(baseUrl)
        .patch('/api/member/update')
        .set('Accept', 'application/json')
        .send(member);
      expect(res.statusCode).toBe(200);
      expect(res.body).toBe('Member updated successfully !');
    });
  });
});

describe(`${baseUrl}/api/member/delete/`, () => {
  describe('DELETE member =>', () => {
    test('Should get the response method with status code 200', async () => {
      const res = await request(baseUrl)
        .delete('/api/member/delete/3')
        .set('Accept', 'application/json');
      expect(res.statusCode).toBe(200);
      expect(res.body).toBe('Member deleted successfully !');
    });
  });
});

describe(`${baseUrl}/api/member/add`, () => {
  describe('POST member with wrong type on email =>', () => {
    test('Should get the response method with status code 500', async () => {
      const member = {
        email: 1,
        password: '',
        firstname: 'Unit',
        lastname: 'Test',
        address: '5 rd of Jest',
        zipcode: '99999',
        city: 'JestVille',
        picture: '',
        pricing: 1
      };
      const res = await request(baseUrl)
        .post('/api/member/add')
        .set('Accept', 'application/json')
        .send(member);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/member/add`, () => {
  describe('POST member with wrong type on password =>', () => {
    test('Should get the response method with status code 500', async () => {
      const member = {
        email: 'unit.test@test.com',
        password: 1234,
        firstname: 'Unit',
        lastname: 'Test',
        address: '5 rd of Jest',
        zipcode: '99999',
        city: 'JestVille',
        picture: '',
        pricing: 1
      };
      const res = await request(baseUrl)
        .post('/api/member/add')
        .set('Accept', 'application/json')
        .send(member);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/member/add`, () => {
  describe('POST member with wrong type on firstname =>', () => {
    test('Should get the response method with status code 500', async () => {
      const member = {
        email: 'unit.test@test.com',
        password: '',
        firstname: 1,
        lastname: 'Test',
        address: '5 rd of Jest',
        zipcode: '99999',
        city: 'JestVille',
        picture: '',
        pricing: 1
      };
      const res = await request(baseUrl)
        .post('/api/member/add')
        .set('Accept', 'application/json')
        .send(member);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/member/add`, () => {
  describe('POST member with wrong type on lastname =>', () => {
    test('Should get the response method with status code 500', async () => {
      const member = {
        email: 'unit.test@test.com',
        password: '',
        firstname: 'Unit',
        lastname: 1,
        address: '5 rd of Jest',
        zipcode: '99999',
        city: 'JestVille',
        picture: '',
        pricing: 1
      };
      const res = await request(baseUrl)
        .post('/api/member/add')
        .set('Accept', 'application/json')
        .send(member);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/member/add`, () => {
  describe('POST member with wrong type on address =>', () => {
    test('Should get the response method with status code 500', async () => {
      const member = {
        email: 'unit.test@test.com',
        password: '',
        firstname: 'Unit',
        lastname: 'Test',
        address: 5,
        zipcode: '99999',
        city: 'JestVille',
        picture: '',
        pricing: 1
      };
      const res = await request(baseUrl)
        .post('/api/member/add')
        .set('Accept', 'application/json')
        .send(member);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/member/add`, () => {
  describe('POST member with wrong type on zipcode =>', () => {
    test('Should get the response method with status code 500', async () => {
      const member = {
        email: 'unit.test@test.com',
        password: '',
        firstname: 'Unit',
        lastname: 'Test',
        address: '5 rd of Jest',
        zipcode: 4,
        city: 'JestVille',
        picture: '',
        pricing: 1
      };
      const res = await request(baseUrl)
        .post('/api/member/add')
        .set('Accept', 'application/json')
        .send(member);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/member/add`, () => {
  describe('POST member with wrong type on city =>', () => {
    test('Should get the response method with status code 500', async () => {
      const member = {
        email: 'unit.test@test.com',
        password: '',
        firstname: 'Unit',
        lastname: 'Test',
        address: '5 rd of Jest',
        zipcode: '99999',
        city: 1,
        picture: '',
        pricing: 1
      };
      const res = await request(baseUrl)
        .post('/api/member/add')
        .set('Accept', 'application/json')
        .send(member);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/member/add`, () => {
  describe('POST member with wrong type on picture =>', () => {
    test('Should get the response method with status code 500', async () => {
      const member = {
        email: 'unit.test@test.com',
        password: '',
        firstname: 'Unit',
        lastname: 'Test',
        address: '5 rd of Jest',
        zipcode: '99999',
        city: 'JestVille',
        picture: 1,
        pricing: 1
      };
      const res = await request(baseUrl)
        .post('/api/member/add')
        .set('Accept', 'application/json')
        .send(member);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/member/add`, () => {
  describe('POST member with wrong type on pricing =>', () => {
    test('Should get the response method with status code 500', async () => {
      const member = {
        email: 'unit.test@test.com',
        password: '',
        firstname: 'Unit',
        lastname: 'Test',
        address: '5 rd of Jest',
        zipcode: '99999',
        city: 'JestVille',
        picture: '',
        pricing: '1'
      };
      const res = await request(baseUrl)
        .post('/api/member/add')
        .set('Accept', 'application/json')
        .send(member);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/member/update`, () => {
  describe('PATCH member with empty data on email =>', () => {
    test('Should get the response method with status code 500', async () => {
      const member = {
        password: '',
        firstname: 'Unit',
        lastname: 'Test',
        address: '5 rd of Jest',
        zipcode: '99999',
        city: 'JestVille',
        picture: '',
        pricing: 1
      };
      const res = await request(baseUrl)
        .patch('/api/member/update')
        .set('Accept', 'application/json')
        .send(member);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/member/update`, () => {
  describe('PATCH member with empty data on password =>', () => {
    test('Should get the response method with status code 500', async () => {
      const member = {
        email: 'unit.test@test.com',
        firstname: 'Unit',
        lastname: 'Test',
        address: '5 rd of Jest',
        zipcode: '99999',
        city: 'JestVille',
        picture: '',
        pricing: 1
      };
      const res = await request(baseUrl)
        .patch('/api/member/update')
        .set('Accept', 'application/json')
        .send(member);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/member/update`, () => {
  describe('PATCH member with empty data on firstname =>', () => {
    test('Should get the response method with status code 500', async () => {
      const member = {
        email: 'unit.test@test.com',
        password: '',
        lastname: 'Test',
        address: '5 rd of Jest',
        zipcode: '99999',
        city: 'JestVille',
        picture: '',
        pricing: 1
      };
      const res = await request(baseUrl)
        .patch('/api/member/update')
        .set('Accept', 'application/json')
        .send(member);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/member/update`, () => {
  describe('PATCH member with empty data on lastname =>', () => {
    test('Should get the response method with status code 500', async () => {
      const member = {
        email: 'unit.test@test.com',
        password: '',
        firstname: 'Unit',
        address: '5 rd of Jest',
        zipcode: '99999',
        city: 'JestVille',
        picture: '',
        pricing: 1
      };
      const res = await request(baseUrl)
        .patch('/api/member/update')
        .set('Accept', 'application/json')
        .send(member);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/member/update`, () => {
  describe('PATCH member with empty data on address =>', () => {
    test('Should get the response method with status code 500', async () => {
      const member = {
        email: 'unit.test@test.com',
        password: '',
        firstname: 'Unit',
        lastname: 'Test',
        zipcode: '99999',
        city: 'JestVille',
        picture: '',
        pricing: 1
      };
      const res = await request(baseUrl)
        .patch('/api/member/update')
        .set('Accept', 'application/json')
        .send(member);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/member/update`, () => {
  describe('PATCH member with empty data on zipcode =>', () => {
    test('Should get the response method with status code 500', async () => {
      const member = {
        email: 'unit.test@test.com',
        password: '',
        firstname: 'Unit',
        lastname: 'Test',
        address: '5 rd of Jest',
        city: 'JestVille',
        picture: '',
        pricing: 1
      };
      const res = await request(baseUrl)
        .patch('/api/member/update')
        .set('Accept', 'application/json')
        .send(member);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/member/update`, () => {
  describe('PATCH member with empty type on city =>', () => {
    test('Should get the response method with status code 500', async () => {
      const member = {
        email: 'unit.test@test.com',
        password: '',
        firstname: 'Unit',
        lastname: 'Test',
        address: '5 rd of Jest',
        zipcode: '99999',
        picture: '',
        pricing: 1
      };
      const res = await request(baseUrl)
        .patch('/api/member/update')
        .set('Accept', 'application/json')
        .send(member);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/member/update`, () => {
  describe('PATCH member with empty data on picture =>', () => {
    test('Should get the response method with status code 500', async () => {
      const member = {
        email: 'unit.test@test.com',
        password: '',
        firstname: 'Unit',
        lastname: 'Test',
        address: '5 rd of Jest',
        zipcode: '99999',
        city: 'JestVille',
        pricing: 1
      };
      const res = await request(baseUrl)
        .patch('/api/member/update')
        .set('Accept', 'application/json')
        .send(member);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/member/update`, () => {
  describe('PATCH member with empty data on pricing =>', () => {
    test('Should get the response method with status code 500', async () => {
      const member = {
        email: 'unit.test@test.com',
        password: '',
        firstname: 'Unit',
        lastname: 'Test',
        address: '5 rd of Jest',
        zipcode: '99999',
        city: 'JestVille',
        picture: ''
      };
      const res = await request(baseUrl)
        .patch('/api/member/update')
        .set('Accept', 'application/json')
        .send(member);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});
