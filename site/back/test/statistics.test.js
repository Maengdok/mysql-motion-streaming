const request = require('supertest');

const baseUrl = 'http://127.0.0.1:8080';

describe(`${baseUrl}/api/statistics/showmostwatched`, () => {
  describe('GET statistics =>', () => {
    test('Should get the response method with status code 200', async () => {
      const res = await request(baseUrl)
        .get('/api/statistics/showmostwatched/')
        .set('Accept', 'application/json');
      expect(res.statusCode).toBe(200);
    });
  });
});

describe(`${baseUrl}/api/statistics/showlatest`, () => {
  describe('GET statistics =>', () => {
    test('Should get the response method with status code 200', async () => {
      const res = await request(baseUrl)
        .get('/api/statistics/showlatest/')
        .set('Accept', 'application/json');
      expect(res.statusCode).toBe(200);
    });
  });
});

describe(`${baseUrl}/api/statistics/showads`, () => {
  describe('GET statistics =>', () => {
    test('Should get the response method with status code 200', async () => {
      const res = await request(baseUrl)
        .get('/api/statistics/showads/')
        .set('Accept', 'application/json');
      expect(res.statusCode).toBe(200);
    });
  });
});
