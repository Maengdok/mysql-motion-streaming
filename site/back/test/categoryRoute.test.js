const request = require('supertest');

const baseUrl = 'http://127.0.0.1:8080';

describe(`${baseUrl}/api/category/list`, () => {
  describe('GET list category =>', () => {
    test('Should get the response method with status code 200', async () => {
      const res = await request(baseUrl)
        .get('/api/category/list')
        .set('Accept', 'application/json');

      expect(res.statusCode).toBe(200);
    });
  });
});

describe(`${baseUrl}/api/category/show`, () => {
  describe('GET category =>', () => {
    test('Should get the response method with status code 200', async () => {
      const res = await request(baseUrl)
        .get('/api/category/show/6')
        .set('Accept', 'application/json');

      expect(res.statusCode).toBe(200);
    });
  });
});

describe(`${baseUrl}/api/category/add`, () => {
  describe('POST category =>', () => {
    test('Should get the response method with status code 200', async () => {
      const category = {
        title: 'UnitTestCategory',
        picture: ''
      };
      const res = await request(baseUrl)
        .post('/api/category/add')
        .set('Accept', 'application/json')
        .send(category);

      expect(res.statusCode).toBe(200);
      expect(res.body).toBe('Category added successfully !');
    });
  });
});

describe(`${baseUrl}/api/category/update`, () => {
  describe('PATCH category =>', () => {
    test('Should get the response method with status code 200', async () => {
      const category = {
        id: 9,
        title: 'UnitTestUpdateCategory',
        picture: ''
      };
      const res = await request(baseUrl)
        .patch('/api/category/update')
        .set('Accept', 'application/json')
        .send(category);

      expect(res.statusCode).toBe(200);
      expect(res.body).toBe('Category updated successfully !');
    });
  });
});

describe(`${baseUrl}/api/category/delete`, () => {
  describe('DELETE category =>', () => {
    test('Should get the response method with status code 200', async () => {
      const res = await request(baseUrl)
        .delete('/api/category/delete/9')
        .set('Accept', 'application/json');

      expect(res.statusCode).toBe(200);
      expect(res.body).toBe('Category deleted successfully !');
    });
  });
});

describe(`${baseUrl}/api/category/add`, () => {
  describe('POST category with wrong type on title =>', () => {
    test('Should get the response method with status code 500', async () => {
      const category = {
        title: 1,
        picture: ''
      };
      const res = await request(baseUrl)
        .post('/api/category/add')
        .set('Accept', 'application/json')
        .send(category);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/category/add`, () => {
  describe('POST category with wrong type on picture =>', () => {
    test('Should get the response method with status code 500', async () => {
      const category = {
        title: 'UnitTestCategory',
        picture: 1
      };
      const res = await request(baseUrl)
        .post('/api/category/add')
        .set('Accept', 'application/json')
        .send(category);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/category/update`, () => {
  describe('PATCH category with wrong type on title =>', () => {
    test('Should get the response method with status code 500', async () => {
      const category = {
        id: 9,
        title: 1,
        picture: ''
      };
      const res = await request(baseUrl)
        .patch('/api/category/update')
        .set('Accept', 'application/json')
        .send(category);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/category/update`, () => {
  describe('PATCH category with wrong type on picture =>', () => {
    test('Should get the response method with status code 500', async () => {
      const category = {
        id: 9,
        title: 'UnitTestUpdateCategory',
        picture: 1
      };
      const res = await request(baseUrl)
        .patch('/api/category/update')
        .set('Accept', 'application/json')
        .send(category);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/category/add`, () => {
  describe('POST category with empty data on title =>', () => {
    test('Should get the response method with status code 500', async () => {
      const category = {
        title: 1,
      };
      const res = await request(baseUrl)
        .post('/api/category/add')
        .set('Accept', 'application/json')
        .send(category);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/category/add`, () => {
  describe('POST category with empty data on picture =>', () => {
    test('Should get the response method with status code 500', async () => {
      const category = {
        title: 'UnitTestCategory',
      };
      const res = await request(baseUrl)
        .post('/api/category/add')
        .set('Accept', 'application/json')
        .send(category);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/category/update`, () => {
  describe('PATCH category with empty data on title =>', () => {
    test('Should get the response method with status code 500', async () => {
      const category = {
        id: 9,
        picture: ''
      };
      const res = await request(baseUrl)
        .patch('/api/category/update')
        .set('Accept', 'application/json')
        .send(category);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/category/update`, () => {
  describe('PATCH category with empty data on picture =>', () => {
    test('Should get the response method with status code 500', async () => {
      const category = {
        id: 9,
        title: 'UnitTestUpdateCategory',
      };
      const res = await request(baseUrl)
        .patch('/api/category/update')
        .set('Accept', 'application/json')
        .send(category);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});