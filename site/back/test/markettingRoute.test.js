const request = require('supertest');

const baseUrl = 'http://127.0.0.1:8080';

describe(`${baseUrl}/api/marketting/list`, () => {
  describe('GET list marketting =>', () => {
    test('Should get the response method with status code 200', async () => {
      const res = await request(baseUrl)
        .get('/api/marketting/list')
        .set('Accept', 'application/json');
      expect(res.statusCode).toBe(200);
    });
  });
});

describe(`${baseUrl}/api/marketting/show`, () => {
  describe('GET marketting =>', () => {
    test('Should get the response method with status code 200', async () => {
      const res = await request(baseUrl)
        .get('/api/marketting/show/6')
        .set('Accept', 'application/json');
      expect(res.statusCode).toBe(200);
    });
  });
});

describe(`${baseUrl}/api/marketting/add`, () => {
  describe('POST marketting =>', () => {
    test('Should get the response method with status code 200', async () => {
      const marketting = {
        title: 'UnitTestmarketting',
        content: 'UnitTestMarkettingContent',
        link: 'UnitTestMarkettingLink',
        picture: '',
        video: ''
      };
      const res = await request(baseUrl)
        .post('/api/marketting/add')
        .set('Accept', 'application/json')
        .send(marketting);
      expect(res.statusCode).toBe(200);
      expect(res.body).toBe('Marketting added successfully !');
    });
  });
});

describe(`${baseUrl}/api/marketting/update`, () => {
  describe('PATCH marketting =>', () => {
    test('Should get the response method with status code 200', async () => {
      const marketting = {
        id: 11,
        title: 'UnitTestmarkettingUpdate',
        content: 'UnitTestMarkettingContentUpdate',
        link: 'UnitTestMarkettingLinkUpdate',
        picture: '',
        video: ''
      };
      const res = await request(baseUrl)
        .patch('/api/marketting/update')
        .set('Accept', 'application/json')
        .send(marketting);
      expect(res.statusCode).toBe(200);
      expect(res.body).toBe('Marketting updated successfully !');
    });
  });
});

describe(`${baseUrl}/api/marketting/delete`, () => {
  describe('DELETE marketting =>', () => {
    test('Should get the response method with status code 200', async () => {
      const res = await request(baseUrl)
        .delete('/api/marketting/delete/11')
        .set('Accept', 'application/json');
      expect(res.statusCode).toBe(200);
      expect(res.body).toBe('Marketting deleted successfully !');
    });
  });
});

describe(`${baseUrl}/api/marketting/add`, () => {
  describe('POST marketting with wrong type on title =>', () => {
    test('Should get the response method with status code 500', async () => {
      const marketting = {
        title: 1,
        content: 'UnitTestMarkettingContent',
        link: 'UnitTestMarkettingLink',
        picture: '',
        video: ''
      };
      const res = await request(baseUrl)
        .post('/api/marketting/add')
        .set('Accept', 'application/json')
        .send(marketting);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/marketting/add`, () => {
  describe('POST marketting with wrong type on content =>', () => {
    test('Should get the response method with status code 500', async () => {
      const marketting = {
        title: 'UnitTestMarkettingTitle',
        content: 1,
        link: 'UnitTestMarkettingLink',
        picture: '',
        video: ''
      };
      const res = await request(baseUrl)
        .post('/api/marketting/add')
        .set('Accept', 'application/json')
        .send(marketting);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});
describe(`${baseUrl}/api/marketting/add`, () => {
  describe('POST marketting with wrong type on link =>', () => {
    test('Should get the response method with status code 500', async () => {
      const marketting = {
        title: 'UnitTestMarkettingTitle',
        content: 'UnitTestMarkettingContent',
        link: 1,
        picture: '',
        video: ''
      };
      const res = await request(baseUrl)
        .post('/api/marketting/add')
        .set('Accept', 'application/json')
        .send(marketting);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});
describe(`${baseUrl}/api/marketting/add`, () => {
  describe('POST marketting with wrong type on picture =>', () => {
    test('Should get the response method with status code 500', async () => {
      const marketting = {
        title: 'UnitTestMarkettingTitle',
        content: 'UnitTestMarkettingContent',
        link: 'UnitTestMarkettingLink',
        picture: 1,
        video: ''
      };
      const res = await request(baseUrl)
        .post('/api/marketting/add')
        .set('Accept', 'application/json')
        .send(marketting);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});
describe(`${baseUrl}/api/marketting/add`, () => {
  describe('POST marketting with wrong type on video =>', () => {
    test('Should get the response method with status code 500', async () => {
      const marketting = {
        title: 'UnitTestMarkettingTitle',
        content: 'UnitTestMarkettingContent',
        link: 'UnitTestMarkettingLink',
        picture: '',
        video: 1
      };
      const res = await request(baseUrl)
        .post('/api/marketting/add')
        .set('Accept', 'application/json')
        .send(marketting);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/marketting/update`, () => {
  describe('PATCH marketting with empty data on title =>', () => {
    test('Should get the response method with status code 500', async () => {
      const marketting = {
        id: 11,
        content: 'UnitTestMarkettingContent',
        link: 'UnitTestMarkettingLink',
        picture: '',
        video: ''
      };
      const res = await request(baseUrl)
        .patch('/api/marketting/update')
        .set('Accept', 'application/json')
        .send(marketting);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/marketting/update`, () => {
  describe('PATCH marketting with empty data on content =>', () => {
    test('Should get the response method with status code 500', async () => {
      const marketting = {
        id: 11,
        title: 'UnitTestMarkettingTitle',
        link: 'UnitTestMarkettingLink',
        picture: '',
        video: ''
      };
      const res = await request(baseUrl)
        .patch('/api/marketting/update')
        .set('Accept', 'application/json')
        .send(marketting);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});
describe(`${baseUrl}/api/marketting/update`, () => {
  describe('PATCH marketting with empty data on link =>', () => {
    test('Should get the response method with status code 500', async () => {
      const marketting = {
        id: 11,
        title: 'UnitTestMarkettingTitle',
        content: 'UnitTestMarkettingContent',
        picture: '',
        video: ''
      };
      const res = await request(baseUrl)
        .patch('/api/marketting/update')
        .set('Accept', 'application/json')
        .send(marketting);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});
describe(`${baseUrl}/api/marketting/update`, () => {
  describe('PATCH marketting with empty data on picture =>', () => {
    test('Should get the response method with status code 500', async () => {
      const marketting = {
        id: 11,
        title: 'UnitTestMarkettingTitle',
        content: 'UnitTestMarkettingContent',
        link: 'UnitTestMarkettingLink',
        video: ''
      };
      const res = await request(baseUrl)
        .patch('/api/marketting/update')
        .set('Accept', 'application/json')
        .send(marketting);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});
describe(`${baseUrl}/api/marketting/update`, () => {
  describe('PATCH marketting with empty data on video =>', () => {
    test('Should get the response method with status code 500', async () => {
      const marketting = {
        title: 'UnitTestMarkettingTitle',
        content: 'UnitTestMarkettingContent',
        link: 'UnitTestMarkettingLink',
        picture: ''
      };
      const res = await request(baseUrl)
        .patch('/api/marketting/update')
        .set('Accept', 'application/json')
        .send(marketting);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});
