const request = require('supertest');

const baseUrl = 'http://127.0.0.1:8080';

describe(`${baseUrl}/api/unknown/list`, () => {
  describe('GET unknown actor =>', () => {
    test('Should get the response method with status code 404', async () => {
      const res = await request(baseUrl)
        .get('/api/unknown/list')
        .set('Accept', 'application/json');
      expect(res.statusCode).toBe(404);
    });
  });
});