const request = require('supertest');

const baseUrl = 'http://127.0.0.1:8080';

describe(`${baseUrl}/api/movie/list`, () => {
  describe('GET list movie =>', () => {
    test('Should get the response method with status code 200', async () => {
      const res = await request(baseUrl)
        .get('/api/movie/list')
        .set('Accept', 'application/json');
      expect(res.statusCode).toBe(200);
    });
  });
});

describe(`${baseUrl}/api/movie/show`, () => {
  describe('GET movie =>', () => {
    test('Should get the response method with status code 200', async () => {
      const res = await request(baseUrl)
        .get('/api/movie/show/99')
        .set('Accept', 'application/json');
      expect(res.statusCode).toBe(200);
    });
  });
});

describe(`${baseUrl}/api/movie/views`, () => {
  describe('GET views =>', () => {
    test('Should get the response method with status code 200', async () => {
      const res = await request(baseUrl)
        .get('/api/movie/views/99')
        .set('Accept', 'application/json');
      expect(res.statusCode).toBe(200);
    });
  });
});

describe(`${baseUrl}/api/movie/category`, () => {
  describe('GET category =>', () => {
    test('Should get the response method with status code 200', async () => {
      const res = await request(baseUrl)
        .get('/api/movie/category/1')
        .set('Accept', 'application/json');
      expect(res.statusCode).toBe(200);
    });
  });
});

describe(`${baseUrl}/api/movie/ranking`, () => {
  describe('GET ranking =>', () => {
    test('Should get the response method with status code 200', async () => {
      const res = await request(baseUrl)
        .get('/api/movie/ranking/99')
        .set('Accept', 'application/json');
      expect(res.statusCode).toBe(200);
    });
  });
});

describe(`${baseUrl}/api/movie/search`, () => {
  describe('GET movie =>', () => {
    test('Should get the response method with status code 200', async () => {
      const res = await request(baseUrl)
        .get('/api/movie/search/movie 99')
        .set('Accept', 'application/json');
      expect(res.statusCode).toBe(200);
    });
  });
});

describe(`${baseUrl}/api/movie/add`, () => {
  describe('POST movie =>', () => {
    test('Should get the response method with status code 200', async () => {
      const movie = {
        title: 'MovieUnitTest',
        description: 'MovieUnitTestDescription',
        imageMin: '',
        imageMax: '',
        imagePortrait: '',
        date: '2022-06-11',
        length: 320,
        video: '',
        category: 1,
        classification: 1
      };
      const res = await request(baseUrl)
        .post('/api/movie/add')
        .set('Accept', 'application/json')
        .send(movie);
      expect(res.statusCode).toBe(200);
      expect(res.body).toBe('Movie added successfully !');
    });
  });
});

describe(`${baseUrl}/api/movie/update`, () => {
  describe('PATCH movie =>', () => {
    test('Should get the response method with status code 200', async () => {
      const movie = {
        id: 102,
        title: 'MovieUnitTestUpdate',
        description: 'MovieUnitTestDescriptionUpdate',
        imageMin: '',
        imageMax: '',
        imagePortrait: '',
        date: '2022-06-11',
        length: 320,
        video: '',
        category: 1,
        classification: 1
      };
      const res = await request(baseUrl)
        .patch('/api/movie/update')
        .set('Accept', 'application/json')
        .send(movie);
      expect(res.statusCode).toBe(200);
      expect(res.body).toBe('Movie updated successfully !');
    });
  });
});

describe(`${baseUrl}/api/movie/updateviews`, () => {
  describe('PATCH views =>', () => {
    test('Should get the response method with status code 200', async () => {
      const res = await request(baseUrl)
        .patch('/api/movie/updateviews/1')
        .set('Accept', 'application/json');
      expect(res.statusCode).toBe(200);
      expect(res.body).toBe('Movie views updated successfully !');
    });
  });
});

describe(`${baseUrl}/api/movie/updateranking`, () => {
  describe('PATCH movie =>', () => {
    test('Should get the response method with status code 200', async () => {
      const movie = {
        id: 102,
        ranking: 5
      };
      const res = await request(baseUrl)
        .patch('/api/movie/updateranking')
        .set('Accept', 'application/json')
        .send(movie);
      expect(res.statusCode).toBe(200);
      expect(res.body).toBe('Movie ranking updated successfully !');
    });
  });
});

describe(`${baseUrl}/api/movie/delete/`, () => {
  describe('DELETE movie =>', () => {
    test('Should get the response method with status code 200', async () => {
      const res = await request(baseUrl)
        .delete('/api/movie/delete/102')
        .set('Accept', 'application/json');
      expect(res.statusCode).toBe(200);
      expect(res.body).toBe('Movie deleted successfully !');
    });
  });
});

describe(`${baseUrl}/api/movie/add`, () => {
  describe('POST movie with wrong type on title =>', () => {
    test('Should get the response method with status code 500', async () => {
      const movie = {
        title: 1,
        description: 'MovieUnitTestDescription',
        imageMin: '',
        imageMax: '',
        imagePortrait: '',
        date: '2022-06-11',
        length: 320,
        video: '',
        category: 1,
        classification: 1
      };
      const res = await request(baseUrl)
        .post('/api/movie/add')
        .set('Accept', 'application/json')
        .send(movie);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/movie/add`, () => {
  describe('POST movie with wrong type on description =>', () => {
    test('Should get the response method with status code 500', async () => {
      const movie = {
        title: 'MovieUnitTest',
        description: 1,
        imageMin: '',
        imageMax: '',
        imagePortrait: '',
        date: '2022-06-11',
        length: 320,
        video: '',
        category: 1,
        classification: 1
      };
      const res = await request(baseUrl)
        .post('/api/movie/add')
        .set('Accept', 'application/json')
        .send(movie);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/movie/add`, () => {
  describe('POST movie with wrong type on imageMin =>', () => {
    test('Should get the response method with status code 500', async () => {
      const movie = {
        title: 'MovieUnitTest',
        description: 'MovieUnitTestDescription',
        imageMin: 1,
        imageMax: '',
        imagePortrait: '',
        date: '2022-06-11',
        length: 320,
        video: '',
        category: 1,
        classification: 1
      };
      const res = await request(baseUrl)
        .post('/api/movie/add')
        .set('Accept', 'application/json')
        .send(movie);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/movie/add`, () => {
  describe('POST movie with wrong type on imageMax =>', () => {
    test('Should get the response method with status code 500', async () => {
      const movie = {
        title: 'MovieUnitTest',
        description: 'MovieUnitTestDescription',
        imageMin: '',
        imageMax: 1,
        imagePortrait: '',
        date: '2022-06-11',
        length: 320,
        video: '',
        category: 1,
        classification: 1
      };
      const res = await request(baseUrl)
        .post('/api/movie/add')
        .set('Accept', 'application/json')
        .send(movie);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/movie/add`, () => {
  describe('POST movie with wrong type on imagePortrait =>', () => {
    test('Should get the response method with status code 500', async () => {
      const movie = {
        title: 'MovieUnitTest',
        description: 'MovieUnitTestDescription',
        imageMin: '',
        imageMax: '',
        imagePortrait: 1,
        date: '2022-06-11',
        length: 320,
        video: '',
        category: 1,
        classification: 1
      };
      const res = await request(baseUrl)
        .post('/api/movie/add')
        .set('Accept', 'application/json')
        .send(movie);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/movie/add`, () => {
  describe('POST movie with wrong type on date =>', () => {
    test('Should get the response method with status code 500', async () => {
      const movie = {
        title: 'MovieUnitTest',
        description: 'MovieUnitTestDescription',
        imageMin: '',
        imageMax: '',
        imagePortrait: '',
        date: 1,
        length: 320,
        video: '',
        category: 1,
        classification: 1
      };
      const res = await request(baseUrl)
        .post('/api/movie/add')
        .set('Accept', 'application/json')
        .send(movie);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/movie/add`, () => {
  describe('POST movie with wrong type on length =>', () => {
    test('Should get the response method with status code 500', async () => {
      const movie = {
        title: 'MovieUnitTest',
        description: 'MovieUnitTestDescription',
        imageMin: '',
        imageMax: '',
        imagePortrait: '',
        date: '2022-06-11',
        length: '320',
        video: '',
        category: 1,
        classification: 1
      };
      const res = await request(baseUrl)
        .post('/api/movie/add')
        .set('Accept', 'application/json')
        .send(movie);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/movie/add`, () => {
  describe('POST movie with wrong type on video =>', () => {
    test('Should get the response method with status code 500', async () => {
      const movie = {
        title: 'MovieUnitTest',
        description: 'MovieUnitTestDescription',
        imageMin: '',
        imageMax: '',
        imagePortrait: '',
        date: '2022-06-11',
        length: 320,
        video: 1,
        category: 1,
        classification: 1
      };
      const res = await request(baseUrl)
        .post('/api/movie/add')
        .set('Accept', 'application/json')
        .send(movie);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/movie/add`, () => {
  describe('POST movie with wrong type on category =>', () => {
    test('Should get the response method with status code 500', async () => {
      const movie = {
        title: 'MovieUnitTest',
        description: 'MovieUnitTestDescription',
        imageMin: '',
        imageMax: '',
        imagePortrait: '',
        date: '2022-06-11',
        length: 320,
        video: '',
        category: '1',
        classification: 1
      };
      const res = await request(baseUrl)
        .post('/api/movie/add')
        .set('Accept', 'application/json')
        .send(movie);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/movie/add`, () => {
  describe('POST movie with wrong type on classification =>', () => {
    test('Should get the response method with status code 500', async () => {
      const movie = {
        title: 'MovieUnitTest',
        description: 'MovieUnitTestDescription',
        imageMin: '',
        imageMax: '',
        imagePortrait: '',
        date: '2022-06-11',
        length: 320,
        video: '',
        category: 1,
        classification: '1'
      };
      const res = await request(baseUrl)
        .post('/api/movie/add')
        .set('Accept', 'application/json')
        .send(movie);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/movie/add`, () => {
  describe('POST movie with empty data on title =>', () => {
    test('Should get the response method with status code 500', async () => {
      const movie = {
        description: 'MovieUnitTestDescription',
        imageMin: '',
        imageMax: '',
        imagePortrait: '',
        date: '2022-06-11',
        length: 320,
        video: '',
        category: 1,
        classification: 1
      };
      const res = await request(baseUrl)
        .post('/api/movie/add')
        .set('Accept', 'application/json')
        .send(movie);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/movie/add`, () => {
  describe('POST movie with empty data on description =>', () => {
    test('Should get the response method with status code 500', async () => {
      const movie = {
        title: 'MovieUnitTest',
        imageMin: '',
        imageMax: '',
        imagePortrait: '',
        date: '2022-06-11',
        length: 320,
        video: '',
        category: 1,
        classification: 1
      };
      const res = await request(baseUrl)
        .post('/api/movie/add')
        .set('Accept', 'application/json')
        .send(movie);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/movie/add`, () => {
  describe('POST movie with empty data on imageMin =>', () => {
    test('Should get the response method with status code 500', async () => {
      const movie = {
        title: 'MovieUnitTest',
        description: 'MovieUnitTestDescription',

        imageMax: '',
        imagePortrait: '',
        date: '2022-06-11',
        length: 320,
        video: '',
        category: 1,
        classification: 1
      };
      const res = await request(baseUrl)
        .post('/api/movie/add')
        .set('Accept', 'application/json')
        .send(movie);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/movie/add`, () => {
  describe('POST movie with empty data on imageMax =>', () => {
    test('Should get the response method with status code 500', async () => {
      const movie = {
        title: 'MovieUnitTest',
        description: 'MovieUnitTestDescription',
        imageMin: '',
        imagePortrait: '',
        date: '2022-06-11',
        length: 320,
        video: '',
        category: 1,
        classification: 1
      };
      const res = await request(baseUrl)
        .post('/api/movie/add')
        .set('Accept', 'application/json')
        .send(movie);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/movie/add`, () => {
  describe('POST movie with empty data on imagePortrait =>', () => {
    test('Should get the response method with status code 500', async () => {
      const movie = {
        title: 'MovieUnitTest',
        description: 'MovieUnitTestDescription',
        imageMin: '',
        imageMax: '',
        date: '2022-06-11',
        length: 320,
        video: '',
        category: 1,
        classification: 1
      };
      const res = await request(baseUrl)
        .post('/api/movie/add')
        .set('Accept', 'application/json')
        .send(movie);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/movie/add`, () => {
  describe('POST movie with empty data on date =>', () => {
    test('Should get the response method with status code 500', async () => {
      const movie = {
        title: 'MovieUnitTest',
        description: 'MovieUnitTestDescription',
        imageMin: '',
        imageMax: '',
        imagePortrait: '',
        length: 320,
        video: '',
        category: 1,
        classification: 1
      };
      const res = await request(baseUrl)
        .post('/api/movie/add')
        .set('Accept', 'application/json')
        .send(movie);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/movie/add`, () => {
  describe('POST movie with empty data on length =>', () => {
    test('Should get the response method with status code 500', async () => {
      const movie = {
        title: 'MovieUnitTest',
        description: 'MovieUnitTestDescription',
        imageMin: '',
        imageMax: '',
        imagePortrait: '',
        date: '2022-06-11',
        video: '',
        category: 1,
        classification: 1
      };
      const res = await request(baseUrl)
        .post('/api/movie/add')
        .set('Accept', 'application/json')
        .send(movie);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/movie/add`, () => {
  describe('POST movie with empty data on video =>', () => {
    test('Should get the response method with status code 500', async () => {
      const movie = {
        title: 'MovieUnitTest',
        description: 'MovieUnitTestDescription',
        imageMin: '',
        imageMax: '',
        imagePortrait: '',
        date: '2022-06-11',
        length: 320,
        category: 1,
        classification: 1
      };
      const res = await request(baseUrl)
        .post('/api/movie/add')
        .set('Accept', 'application/json')
        .send(movie);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/movie/add`, () => {
  describe('POST movie with empty data on category =>', () => {
    test('Should get the response method with status code 500', async () => {
      const movie = {
        title: 'MovieUnitTest',
        description: 'MovieUnitTestDescription',
        imageMin: '',
        imageMax: '',
        imagePortrait: '',
        date: '2022-06-11',
        length: 320,
        video: '',
        classification: 1
      };
      const res = await request(baseUrl)
        .post('/api/movie/add')
        .set('Accept', 'application/json')
        .send(movie);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/movie/add`, () => {
  describe('POST movie with empty data on classification =>', () => {
    test('Should get the response method with status code 500', async () => {
      const movie = {
        title: 'MovieUnitTest',
        description: 'MovieUnitTestDescription',
        imageMin: '',
        imageMax: '',
        imagePortrait: '',
        date: '2022-06-11',
        length: 320,
        video: '',
        category: 1,
      };
      const res = await request(baseUrl)
        .post('/api/movie/add')
        .set('Accept', 'application/json')
        .send(movie);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/movie/update`, () => {
  describe('PATCH movie with wrong type on title =>', () => {
    test('Should get the response method with status code 500', async () => {
      const movie = {
        title: 1,
        description: 'MovieUnitTestDescription',
        imageMin: '',
        imageMax: '',
        imagePortrait: '',
        date: '2022-06-11',
        length: 320,
        video: '',
        category: 1,
        classification: 1
      };
      const res = await request(baseUrl)
        .patch('/api/movie/update')
        .set('Accept', 'application/json')
        .send(movie);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/movie/update`, () => {
  describe('PATCH movie with wrong type on description =>', () => {
    test('Should get the response method with status code 500', async () => {
      const movie = {
        title: 'MovieUnitTest',
        description: 1,
        imageMin: '',
        imageMax: '',
        imagePortrait: '',
        date: '2022-06-11',
        length: 320,
        video: '',
        category: 1,
        classification: 1
      };
      const res = await request(baseUrl)
        .patch('/api/movie/update')
        .set('Accept', 'application/json')
        .send(movie);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/movie/update`, () => {
  describe('PATCH movie with wrong type on imageMin =>', () => {
    test('Should get the response method with status code 500', async () => {
      const movie = {
        title: 'MovieUnitTest',
        description: 'MovieUnitTestDescription',
        imageMin: 1,
        imageMax: '',
        imagePortrait: '',
        date: '2022-06-11',
        length: 320,
        video: '',
        category: 1,
        classification: 1
      };
      const res = await request(baseUrl)
        .patch('/api/movie/update')
        .set('Accept', 'application/json')
        .send(movie);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/movie/update`, () => {
  describe('PATCH movie with wrong type on imageMax =>', () => {
    test('Should get the response method with status code 500', async () => {
      const movie = {
        title: 'MovieUnitTest',
        description: 'MovieUnitTestDescription',
        imageMin: '',
        imageMax: 1,
        imagePortrait: '',
        date: '2022-06-11',
        length: 320,
        video: '',
        category: 1,
        classification: 1
      };
      const res = await request(baseUrl)
        .patch('/api/movie/update')
        .set('Accept', 'application/json')
        .send(movie);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/movie/update`, () => {
  describe('PATCH movie with wrong type on imagePortrait =>', () => {
    test('Should get the response method with status code 500', async () => {
      const movie = {
        title: 'MovieUnitTest',
        description: 'MovieUnitTestDescription',
        imageMin: '',
        imageMax: '',
        imagePortrait: 1,
        date: '2022-06-11',
        length: 320,
        video: '',
        category: 1,
        classification: 1
      };
      const res = await request(baseUrl)
        .patch('/api/movie/update')
        .set('Accept', 'application/json')
        .send(movie);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/movie/update`, () => {
  describe('PATCH movie with wrong type on date =>', () => {
    test('Should get the response method with status code 500', async () => {
      const movie = {
        title: 'MovieUnitTest',
        description: 'MovieUnitTestDescription',
        imageMin: '',
        imageMax: '',
        imagePortrait: '',
        date: 1,
        length: 320,
        video: '',
        category: 1,
        classification: 1
      };
      const res = await request(baseUrl)
        .patch('/api/movie/update')
        .set('Accept', 'application/json')
        .send(movie);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/movie/update`, () => {
  describe('PATCH movie with wrong type on length =>', () => {
    test('Should get the response method with status code 500', async () => {
      const movie = {
        title: 'MovieUnitTest',
        description: 'MovieUnitTestDescription',
        imageMin: '',
        imageMax: '',
        imagePortrait: '',
        date: '2022-06-11',
        length: '320',
        video: '',
        category: 1,
        classification: 1
      };
      const res = await request(baseUrl)
        .patch('/api/movie/update')
        .set('Accept', 'application/json')
        .send(movie);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/movie/update`, () => {
  describe('PATCH movie with wrong type on video =>', () => {
    test('Should get the response method with status code 500', async () => {
      const movie = {
        title: 'MovieUnitTest',
        description: 'MovieUnitTestDescription',
        imageMin: '',
        imageMax: '',
        imagePortrait: '',
        date: '2022-06-11',
        length: 320,
        video: 1,
        category: 1,
        classification: 1
      };
      const res = await request(baseUrl)
        .patch('/api/movie/update')
        .set('Accept', 'application/json')
        .send(movie);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/movie/update`, () => {
  describe('PATCH movie with wrong type on category =>', () => {
    test('Should get the response method with status code 500', async () => {
      const movie = {
        title: 'MovieUnitTest',
        description: 'MovieUnitTestDescription',
        imageMin: '',
        imageMax: '',
        imagePortrait: '',
        date: '2022-06-11',
        length: 320,
        video: '',
        category: '1',
        classification: 1
      };
      const res = await request(baseUrl)
        .patch('/api/movie/update')
        .set('Accept', 'application/json')
        .send(movie);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/movie/update`, () => {
  describe('PATCH movie with wrong type on classification =>', () => {
    test('Should get the response method with status code 500', async () => {
      const movie = {
        title: 'MovieUnitTest',
        description: 'MovieUnitTestDescription',
        imageMin: '',
        imageMax: '',
        imagePortrait: '',
        date: '2022-06-11',
        length: 320,
        video: '',
        category: 1,
        classification: '1'
      };
      const res = await request(baseUrl)
        .patch('/api/movie/update')
        .set('Accept', 'application/json')
        .send(movie);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/movie/update`, () => {
  describe('PATCH movie with empty data on title =>', () => {
    test('Should get the response method with status code 500', async () => {
      const movie = {
        description: 'MovieUnitTestDescription',
        imageMin: '',
        imageMax: '',
        imagePortrait: '',
        date: '2022-06-11',
        length: 320,
        video: '',
        category: 1,
        classification: 1
      };
      const res = await request(baseUrl)
        .patch('/api/movie/update')
        .set('Accept', 'application/json')
        .send(movie);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/movie/update`, () => {
  describe('PATCH movie with empty data on description =>', () => {
    test('Should get the response method with status code 500', async () => {
      const movie = {
        title: 'MovieUnitTest',
        imageMin: '',
        imageMax: '',
        imagePortrait: '',
        date: '2022-06-11',
        length: 320,
        video: '',
        category: 1,
        classification: 1
      };
      const res = await request(baseUrl)
        .patch('/api/movie/update')
        .set('Accept', 'application/json')
        .send(movie);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/movie/update`, () => {
  describe('PATCH movie with empty data on imageMin =>', () => {
    test('Should get the response method with status code 500', async () => {
      const movie = {
        title: 'MovieUnitTest',
        description: 'MovieUnitTestDescription',

        imageMax: '',
        imagePortrait: '',
        date: '2022-06-11',
        length: 320,
        video: '',
        category: 1,
        classification: 1
      };
      const res = await request(baseUrl)
        .patch('/api/movie/update')
        .set('Accept', 'application/json')
        .send(movie);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/movie/update`, () => {
  describe('PATCH movie with empty data on imageMax =>', () => {
    test('Should get the response method with status code 500', async () => {
      const movie = {
        title: 'MovieUnitTest',
        description: 'MovieUnitTestDescription',
        imageMin: '',
        imagePortrait: '',
        date: '2022-06-11',
        length: 320,
        video: '',
        category: 1,
        classification: 1
      };
      const res = await request(baseUrl)
        .patch('/api/movie/update')
        .set('Accept', 'application/json')
        .send(movie);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/movie/update`, () => {
  describe('PATCH movie with empty data on imagePortrait =>', () => {
    test('Should get the response method with status code 500', async () => {
      const movie = {
        title: 'MovieUnitTest',
        description: 'MovieUnitTestDescription',
        imageMin: '',
        imageMax: '',
        date: '2022-06-11',
        length: 320,
        video: '',
        category: 1,
        classification: 1
      };
      const res = await request(baseUrl)
        .patch('/api/movie/update')
        .set('Accept', 'application/json')
        .send(movie);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/movie/update`, () => {
  describe('PATCH movie with empty data on date =>', () => {
    test('Should get the response method with status code 500', async () => {
      const movie = {
        title: 'MovieUnitTest',
        description: 'MovieUnitTestDescription',
        imageMin: '',
        imageMax: '',
        imagePortrait: '',
        length: 320,
        video: '',
        category: 1,
        classification: 1
      };
      const res = await request(baseUrl)
        .patch('/api/movie/update')
        .set('Accept', 'application/json')
        .send(movie);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/movie/update`, () => {
  describe('PATCH movie with empty data on length =>', () => {
    test('Should get the response method with status code 500', async () => {
      const movie = {
        title: 'MovieUnitTest',
        description: 'MovieUnitTestDescription',
        imageMin: '',
        imageMax: '',
        imagePortrait: '',
        date: '2022-06-11',
        video: '',
        category: 1,
        classification: 1
      };
      const res = await request(baseUrl)
        .patch('/api/movie/update')
        .set('Accept', 'application/json')
        .send(movie);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/movie/update`, () => {
  describe('PATCH movie with empty data on video =>', () => {
    test('Should get the response method with status code 500', async () => {
      const movie = {
        title: 'MovieUnitTest',
        description: 'MovieUnitTestDescription',
        imageMin: '',
        imageMax: '',
        imagePortrait: '',
        date: '2022-06-11',
        length: 320,
        category: 1,
        classification: 1
      };
      const res = await request(baseUrl)
        .patch('/api/movie/update')
        .set('Accept', 'application/json')
        .send(movie);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/movie/update`, () => {
  describe('PATCH movie with empty data on category =>', () => {
    test('Should get the response method with status code 500', async () => {
      const movie = {
        title: 'MovieUnitTest',
        description: 'MovieUnitTestDescription',
        imageMin: '',
        imageMax: '',
        imagePortrait: '',
        date: '2022-06-11',
        length: 320,
        video: '',
        classification: 1
      };
      const res = await request(baseUrl)
        .patch('/api/movie/update')
        .set('Accept', 'application/json')
        .send(movie);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/movie/update`, () => {
  describe('PATCH movie with empty data on classification =>', () => {
    test('Should get the response method with status code 500', async () => {
      const movie = {
        title: 'MovieUnitTest',
        description: 'MovieUnitTestDescription',
        imageMin: '',
        imageMax: '',
        imagePortrait: '',
        date: '2022-06-11',
        length: 320,
        video: '',
        category: 1
      };
      const res = await request(baseUrl)
        .patch('/api/movie/update')
        .set('Accept', 'application/json')
        .send(movie);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});
