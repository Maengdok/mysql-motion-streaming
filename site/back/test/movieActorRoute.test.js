const request = require('supertest');

const baseUrl = 'http://127.0.0.1:8080';

describe(`${baseUrl}/api/movieactor/list`, () => {
  describe('GET list movieactor =>', () => {
    test('Should get the response method with status code 200', async () => {
      const res = await request(baseUrl)
        .get('/api/movieactor/list')
        .set('Accept', 'application/json');
      expect(res.statusCode).toBe(200);
    });
  });
});

describe(`${baseUrl}/api/movieactor/showmovies`, () => {
  describe('GET movieactor =>', () => {
    test('Should get the response method with status code 200', async () => {
      const res = await request(baseUrl)
        .get('/api/movieactor/showmovies/10')
        .set('Accept', 'application/json');
      expect(res.statusCode).toBe(200);
    });
  });
});

describe(`${baseUrl}/api/movieactor/showactors`, () => {
  describe('GET movieactor =>', () => {
    test('Should get the response method with status code 200', async () => {
      const res = await request(baseUrl)
        .get('/api/movieactor/showactors/10')
        .set('Accept', 'application/json');
      expect(res.statusCode).toBe(200);
    });
  });
});

describe(`${baseUrl}/api/movieactor/add`, () => {
  describe('POST movieactor =>', () => {
    test('Should get the response method with status code 200', async () => {
      const movieactor = {
        movie: 10,
        actor: 21
      };
      const res = await request(baseUrl)
        .post('/api/movieactor/add')
        .set('Accept', 'application/json')
        .send(movieactor);
      expect(res.statusCode).toBe(200);
      expect(res.body).toBe('Actor added to Movie successfully !');
    });
  });
});

describe(`${baseUrl}/api/movieactor/delete/`, () => {
  describe('DELETE movieactor =>', () => {
    test('Should get the response method with status code 200', async () => {
      const movieactor = {
        movie: 10,
        actor: 21
      };
      const res = await request(baseUrl)
        .delete('/api/movieactor/delete')
        .set('Accept', 'application/json')
        .send(movieactor);
      expect(res.statusCode).toBe(200);
      expect(res.body).toBe('Actor Movie deleted successfully !');
    });
  });
});

describe(`${baseUrl}/api/movieactor/add`, () => {
  describe('POST movieactor with wrong type on movie =>', () => {
    test('Should get the response method with status code 500', async () => {
      const movieactor = {
        movie: '10',
        actor: 21
      };
      const res = await request(baseUrl)
        .post('/api/movieactor/add')
        .set('Accept', 'application/json')
        .send(movieactor);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/movieactor/add`, () => {
  describe('POST movieactor with wrong type on actor =>', () => {
    test('Should get the response method with status code 500', async () => {
      const movieactor = {
        movie: 10,
        actor: '21'
      };
      const res = await request(baseUrl)
        .post('/api/movieactor/add')
        .set('Accept', 'application/json')
        .send(movieactor);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/movieactor/add`, () => {
  describe('POST movieactor with empty data on movie =>', () => {
    test('Should get the response method with status code 500', async () => {
      const movieactor = {
        actor: 21
      };
      const res = await request(baseUrl)
        .post('/api/movieactor/add')
        .set('Accept', 'application/json')
        .send(movieactor);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/movieactor/add`, () => {
  describe('POST movieactor with empty data on actor =>', () => {
    test('Should get the response method with status code 500', async () => {
      const movieactor = {
        movie: 10
      };
      const res = await request(baseUrl)
        .post('/api/movieactor/add')
        .set('Accept', 'application/json')
        .send(movieactor);
      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});
