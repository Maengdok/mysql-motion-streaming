const request = require('supertest');

const baseUrl = 'http://127.0.0.1:8080';

describe(`${baseUrl}/api/favorite/list`, () => {
  describe('GET list favorite =>', () => {
    test('Should get the response method with status code 200', async () => {
      const res = await request(baseUrl)
        .get('/api/favorite/list')
        .set('Accept', 'application/json');

      expect(res.statusCode).toBe(200);
    });
  });
});

describe(`${baseUrl}/api/favorite/show`, () => {
  describe('GET favorite =>', () => {
    test('Should get the response method with status code 200', async () => {
      const res = await request(baseUrl)
        .get('/api/favorite/show/6')
        .set('Accept', 'application/json');

      expect(res.statusCode).toBe(200);
    });
  });
});

describe(`${baseUrl}/api/favorite/add`, () => {
  describe('POST favorite =>', () => {
    test('Should get the response method with status code 200', async () => {
      const favorite = {
        member: 1,
        movie: 99
      };
      const res = await request(baseUrl)
        .post('/api/favorite/add')
        .set('Accept', 'application/json')
        .send(favorite);

      expect(res.statusCode).toBe(200);
      expect(res.body).toBe('Favorite Movie added successfully !');
    });
  });
});

describe(`${baseUrl}/api/favorite/delete`, () => {
  describe('DELETE favorite =>', () => {
    test('Should get the response method with status code 200', async () => {
      const favorite = {
        member: 1,
        movie: 99
      };
      const res = await request(baseUrl)
        .delete('/api/favorite/delete')
        .set('Accept', 'application/json')
        .send(favorite)

      expect(res.statusCode).toBe(200);
      expect(res.body).toBe('Movie deleted from Favorite successfully !');
    });
  });
});

describe(`${baseUrl}/api/favorite/add`, () => {
  describe('POST favorite with wrong type on member =>', () => {
    test('Should get the response method with status code 500', async () => {
      const favorite = {
        member: '1',
        movie: 99
      };
      const res = await request(baseUrl)
        .post('/api/favorite/add')
        .set('Accept', 'application/json')
        .send(favorite);

      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/favorite/add`, () => {
  describe('POST favorite with wrong type on movie =>', () => {
    test('Should get the response method with status code 500', async () => {
      const favorite = {
        member: 1,
        movie: '99'
      };
      const res = await request(baseUrl)
        .post('/api/favorite/add')
        .set('Accept', 'application/json')
        .send(favorite);

      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/favorite/add`, () => {
  describe('POST favorite with empty data on member =>', () => {
    test('Should get the response method with status code 200', async () => {
      const favorite = {
        movie: 99
      };
      const res = await request(baseUrl)
        .post('/api/favorite/add')
        .set('Accept', 'application/json')
        .send(favorite);

      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});

describe(`${baseUrl}/api/favorite/add`, () => {
  describe('POST favorite with empty data on movie =>', () => {
    test('Should get the response method with status code 200', async () => {
      const favorite = {
        member: 1
      };
      const res = await request(baseUrl)
        .post('/api/favorite/add')
        .set('Accept', 'application/json')
        .send(favorite);

      expect(res.statusCode).toBe(500);
      expect(res.body).toStrictEqual({});
    });
  });
});
