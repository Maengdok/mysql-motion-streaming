module.exports = {
  apps: [{
    name: 'app',
    script: './src/index.js',
    exec_mode: 'fork',
    instances: '1',
    watch: true,
    env: {
      NODE_ENV: 'development'
    },
    env_production: {
      NODE_ENV: 'production'
    }
  }]
};
